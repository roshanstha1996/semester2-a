﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAnalysisSoftware
{
    //selected chunk data class
    public partial class SelectedChunkData : Form
    {
        //declared variables
        int[] chunk1HeartRateArray = new int[2000];
        int[] chunk2HeartRateArray = new int[2000];
        int[] chunk1SpeedArray = new int[2000];
        int[] chunk2SpeedArray = new int[2000];
        int[] chunk1CadenceArray = new int[2000];
        int[] chunk2CadenceArray = new int[2000];
        int[] chunk1AltitudeArray = new int[2000];
        int[] chunk2AltitudeArray = new int[2000];
        int[] chunk1PowerInWattsArray = new int[2000];
        int[] chunk2PowerInWattsArray = new int[2000];
        int[] chunk3HeartRateArray = new int[2000];
        int[] chunk4HeartRateArray = new int[2000];
        int[] chunk3SpeedArray = new int[2000];
        int[] chunk4SpeedArray = new int[2000];
        int[] chunk3CadenceArray = new int[2000];
        int[] chunk4CadenceArray = new int[2000];
        int[] chunk3AltitudeArray = new int[2000];
        int[] chunk4AltitudeArray = new int[2000];
        int[] chunk3PowerInWattsArray = new int[2000];
        int[] chunk4PowerInWattsArray = new int[2000];
        int totalItems;
        double avgSpeed;
        double avgPower;
        double totalSeconds;

        /// <summary>
        /// constructor of selected chunk data with parameters
        /// </summary>
        /// <param name="chunkNo"></param>
        /// <param name="totalSeconds"></param>
        public SelectedChunkData(int chunkNo, double totalSeconds)
        {
            this.totalSeconds = totalSeconds;
            InitializeComponent();

            //storing value on array from another class
            if (chunkNo == 2)
            {
                grpchunk3Summ.Visible = false;
                grpchunk4Summ.Visible = false;
                chunk1HeartRateArray = SelectableInformation.chunk1ArrayHR;
                chunk2HeartRateArray = SelectableInformation.chunk2ArrayHR;
                chunk1SpeedArray = SelectableInformation.chunk1ArraySpeed;
                chunk2SpeedArray = SelectableInformation.chunk2ArraySpeed;
                chunk1CadenceArray = SelectableInformation.chunk1ArrayCadence;
                chunk2CadenceArray = SelectableInformation.chunk2ArrayCadence;
                chunk1AltitudeArray = SelectableInformation.chunk1ArrayAltitude;
                chunk2AltitudeArray = SelectableInformation.chunk2ArrayAltitude;
                chunk1PowerInWattsArray = SelectableInformation.chunk1ArrayPower;
                chunk2PowerInWattsArray = SelectableInformation.chunk2ArrayPower;
                
                //calling necessary methods
                calculateChunk1Summary();
                calculateChunk2Summary();
            }
            else if (chunkNo == 3)
            {
                grpchunk4Summ.Visible = false;
                chunk1HeartRateArray = SelectableInformation.chunk1ArrayHR;
                chunk2HeartRateArray = SelectableInformation.chunk2ArrayHR;
                chunk3HeartRateArray = SelectableInformation.chunk3ArrayHR;
                chunk1SpeedArray = SelectableInformation.chunk1ArraySpeed;
                chunk2SpeedArray = SelectableInformation.chunk2ArraySpeed;
                chunk3SpeedArray = SelectableInformation.chunk3ArraySpeed;
                chunk1CadenceArray = SelectableInformation.chunk1ArrayCadence;
                chunk2CadenceArray = SelectableInformation.chunk2ArrayCadence;
                chunk3CadenceArray = SelectableInformation.chunk3ArrayCadence;
                chunk1AltitudeArray = SelectableInformation.chunk1ArrayAltitude;
                chunk2AltitudeArray = SelectableInformation.chunk2ArrayAltitude;
                chunk3AltitudeArray = SelectableInformation.chunk3ArrayAltitude;
                chunk1PowerInWattsArray = SelectableInformation.chunk1ArrayPower;
                chunk2PowerInWattsArray = SelectableInformation.chunk2ArrayPower;
                chunk3PowerInWattsArray = SelectableInformation.chunk3ArrayPower;
                
                //calling necessary methods
                calculateChunk1Summary();
                calculateChunk2Summary();
                calculateChunk3Summary();
            }
            else
            {
                chunk1HeartRateArray = SelectableInformation.chunk1ArrayHR;
                chunk2HeartRateArray = SelectableInformation.chunk2ArrayHR;
                chunk3HeartRateArray = SelectableInformation.chunk3ArrayHR;
                chunk4HeartRateArray = SelectableInformation.chunk4ArrayHR;
                chunk1SpeedArray = SelectableInformation.chunk1ArraySpeed;
                chunk2SpeedArray = SelectableInformation.chunk2ArraySpeed;
                chunk3SpeedArray = SelectableInformation.chunk3ArraySpeed;
                chunk4SpeedArray = SelectableInformation.chunk4ArraySpeed;
                chunk1CadenceArray = SelectableInformation.chunk1ArrayCadence;
                chunk2CadenceArray = SelectableInformation.chunk2ArrayCadence;
                chunk3CadenceArray = SelectableInformation.chunk3ArrayCadence;
                chunk4CadenceArray = SelectableInformation.chunk4ArrayCadence;
                chunk1AltitudeArray = SelectableInformation.chunk1ArrayAltitude;
                chunk2AltitudeArray = SelectableInformation.chunk1ArrayAltitude;
                chunk3AltitudeArray = SelectableInformation.chunk1ArrayAltitude;
                chunk4AltitudeArray = SelectableInformation.chunk1ArrayAltitude;
                chunk1PowerInWattsArray = SelectableInformation.chunk1ArrayPower;
                chunk2PowerInWattsArray = SelectableInformation.chunk2ArrayPower;
                chunk3PowerInWattsArray = SelectableInformation.chunk3ArrayPower;
                chunk4PowerInWattsArray = SelectableInformation.chunk4ArrayPower;
                
                //calling necessary methods
                calculateChunk1Summary();
                calculateChunk2Summary();
                calculateChunk3Summary();
                calculateChunk4Summary();
            }
        }

        /// <summary>
        /// method to calculate the chunk1 for summary data
        /// </summary>
        public void calculateChunk1Summary()
        {
            //to calculate speed for chunk 1
            totalItems = chunk1SpeedArray.Length;
            int totalSpeed = 0;
            for (int i=0; i<totalItems; i++)
            {
                totalSpeed = totalSpeed + chunk1SpeedArray[i];
            }
            //average speed
            avgSpeed = Math.Round((double)totalSpeed / totalItems, 2);
            lblchunk1AvgSpeed.Text = avgSpeed.ToString() + " " + "km/h";
            //maximum speed
            int maxSpeed = chunk1SpeedArray.Max();
            lblchunk1MaxSpeed.Text = maxSpeed.ToString() + " " + "km/h";

            //calculating heart rate for chunk 1 
            totalItems = chunk1HeartRateArray.Length;
            int totalHeartRate = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total heart rate
                totalHeartRate = totalHeartRate + chunk1HeartRateArray[i];
            }
            //average heart rate
            int avgHeartRate = totalHeartRate / totalItems;
            lblchunk1AvgHeartRate.Text = avgHeartRate.ToString() + " " + "bpm";
            //maximum heart rate
            int maxHeartRate = chunk1HeartRateArray.Max();
            lblchunk1MaxHeartRate.Text = maxHeartRate.ToString() + " " + "bpm";
            //minimum heart rate
            int minHeartRate = chunk1HeartRateArray.Min();
            lblchunk1MinHeartRate.Text = minHeartRate.ToString() + " " + "bpm";

            //calculating power for chunk 1
            totalItems = chunk1PowerInWattsArray.Length;
            int totalPower = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total power
                totalPower = totalPower + chunk1PowerInWattsArray[i];
            }
            //average power
            avgPower = totalPower / totalItems;
            lblchunk1AvgPower.Text = avgPower.ToString() + " " + "watts";
            //maximum power
            int maxPower = chunk1PowerInWattsArray.Max();
            lblchunk1MaxPower.Text = maxPower.ToString() + " " + "watts";

            //calculating altitude for chunk 1
            totalItems = chunk1AltitudeArray.Length;
            int totalAltitude = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total altitude
                totalAltitude = totalAltitude + chunk1AltitudeArray[i];
            }
            //average altitude
            double avgAltitude = Math.Round((double)totalAltitude / totalItems, 2);
            lblchunk1AvgAltitude.Text = avgAltitude.ToString() + " " + "m/ft";
            //maximum altitude
            int maxAltitude = chunk1AltitudeArray.Max();
            lblchunk1MaxAltitude.Text = maxAltitude.ToString() + " " + "m/ft";

            //calculating chunk 1 distance covered
            double totalDistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalSeconds), 2);
            lblchunk1TotalDistance.Text = totalDistance.ToString() + " " + "km/h";
        }

        /// <summary>
        /// method to calculate chunk 2 for summary data
        /// </summary>
        public void calculateChunk2Summary()
        {
            //calculating speed for chunk 2
            totalItems = chunk2SpeedArray.Length;
            int totalSpeed = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total speed
                totalSpeed = totalSpeed + chunk2SpeedArray[i];
            }
            //average speed
            avgSpeed = Math.Round((double)totalSpeed / totalItems, 2);
            lblchunk2AvgSpeed.Text = avgSpeed.ToString() + " " + "km/h";
            //maximum speed
            int maxSpeed = chunk2SpeedArray.Max();
            lblchunk2MaxSpeed.Text = maxSpeed.ToString() + " " + "km/h";

            //calculating heart rate for chunk 2 
            totalItems = chunk2HeartRateArray.Length;
            int totalHeartRate = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total heart rate
                totalHeartRate = totalHeartRate + chunk2HeartRateArray[i];
            }
            //average heart rate
            int avgHeartRate = totalHeartRate / totalItems;
            lblchunk2AvgHeartRate.Text = avgHeartRate.ToString() + " " + "bpm";
            //maximum heart rate
            int maxHeartRate = chunk2HeartRateArray.Max();
            lblchunk2MaxHeartRate.Text = maxHeartRate.ToString() + " " + "bpm";
            //minimum heart rate
            int minHeartRate = chunk2HeartRateArray.Min();
            lblchunk2MinHeartRate.Text = minHeartRate.ToString() + " " + "bpm";

            //calculating power for chunk 2
            totalItems = chunk2PowerInWattsArray.Length;
            int totalPower = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total power
                totalPower = totalPower + chunk2PowerInWattsArray[i];
            }
            //average power
            avgPower = totalPower / totalItems;
            lblchunk2AvgPower.Text = avgPower.ToString() + " " + "watts";
            //maximum power
            int maxPower = chunk2PowerInWattsArray.Max();
            lblchunk2MaxPower.Text = maxPower.ToString() + " " + "watts";

            //calculating altitude for chunk 2
            totalItems = chunk2AltitudeArray.Length;
            int totalAltitude = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total altitude
                totalAltitude = totalAltitude + chunk2AltitudeArray[i];
            }
            //average altitude
            double avgAltitude = Math.Round((double)totalAltitude / totalItems, 2);
            lblchunk2AvgAltitude.Text = avgAltitude.ToString() + " " + "m/ft";
            //maximum altitude
            int maxAltitude = chunk2AltitudeArray.Max();
            lblchunk2MaxAltitude.Text = maxAltitude.ToString() + " " + "m/ft";

            //calculating chunk 2 distance covered
            double totalDistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalSeconds), 2);
            lblchunk2TotalDistance.Text = totalDistance.ToString() + " " + "km/h";
        }

        /// <summary>
        /// method to calculate chunk 3 summary data
        /// </summary>
        public void calculateChunk3Summary()
        {
            //calculating speed for chunk 3
            totalItems = chunk3SpeedArray.Length;
            int totalSpeed = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total speed
                totalSpeed = totalSpeed + chunk3SpeedArray[i];
            }
            //average speed
            avgSpeed = Math.Round((double)totalSpeed / totalItems, 2);
            lblchunk3AvgSpeed.Text = avgSpeed.ToString() + " " + "km/h";
            //maximum speed
            int maxSpeed = chunk3SpeedArray.Max();
            lblchunk3MaxSpeed.Text = maxSpeed.ToString() + " " + "km/h";

            //calculating heart rate for chunk 3 
            totalItems = chunk3HeartRateArray.Length;
            int totalHeartRate = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total heart rate
                totalHeartRate = totalHeartRate + chunk3HeartRateArray[i];
            }
            //average heart rate
            int avgHeartRate = totalHeartRate / totalItems;
            lblchunk3AvgHeartRate.Text = avgHeartRate.ToString() + " " + "bpm";
            //maximum heart rate
            int maxHeartRate = chunk3HeartRateArray.Max();
            lblchunk3MaxHeartRate.Text = maxHeartRate.ToString() + " " + "bpm";
            //minimum heart rate
            int minHeartRate = chunk3HeartRateArray.Min();
            lblchunk3MinHeartRate.Text = minHeartRate.ToString() + " " + "bpm";

            //calculating power for chunk 3
            totalItems = chunk3PowerInWattsArray.Length;
            int totalPower = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total power
                totalPower = totalPower + chunk3PowerInWattsArray[i];
            }
            //average power
            avgPower = totalPower / totalItems;
            lblchunk3AvgPower.Text = avgPower.ToString() + " " + "watts";
            //maximum power
            int maxPower = chunk3PowerInWattsArray.Max();
            lblchunk3MaxPower.Text = maxPower.ToString() + " " + "watts";

            //calculating altitude for chunk 3
            totalItems = chunk3AltitudeArray.Length;
            int totalAltitude = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total altitude
                totalAltitude = totalAltitude + chunk3AltitudeArray[i];
            }
            //average altitude
            double avgAltitude = Math.Round((double)totalAltitude / totalItems, 2);
            lblchunk3AvgAltitude.Text = avgAltitude.ToString() + " " + "m/ft";
            //maximum altitude
            int maxAltitude = chunk3AltitudeArray.Max();
            lblchunk3MaxAltitude.Text = maxAltitude.ToString() + " " + "m/ft";

            //calculating chunk 3 distance covered
            double totalDistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalSeconds), 2);
            lblchunk3TotalDistance.Text = totalDistance.ToString() + " " + "km/h";
        }

        /// <summary>
        /// calculate summary data for chunk 4
        /// </summary>
        public void calculateChunk4Summary()
        {
            //calculating speed for chunk 4
            totalItems = chunk4SpeedArray.Length;
            int totalSpeed = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total speed
                totalSpeed = totalSpeed + chunk4SpeedArray[i];
            }
            //average speed
            avgSpeed = Math.Round((double)totalSpeed / totalItems, 2);
            lblchunk4AvgSpeed.Text = avgSpeed.ToString() + " " + "km/h";
            //maximum speed
            int maxSpeed = chunk4SpeedArray.Max();
            lblchunk4MaxSpeed.Text = maxSpeed.ToString() + " " + "km/h";

            //calculating heart rate for chunk 4
            totalItems = chunk4HeartRateArray.Length;
            int totalHeartRate = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total heart rate
                totalHeartRate = totalHeartRate + chunk4HeartRateArray[i];
            }
            //average heart rate
            int avgHeartRate = totalHeartRate / totalItems;
            lblchunk4AvgHeartRate.Text = avgHeartRate.ToString() + " " + "bpm";
            //maximum heart rate
            int maxHeartRate = chunk4HeartRateArray.Max();
            lblchunk4MaxHeartRate.Text = maxHeartRate.ToString() + " " + "bpm";
            //minimum heart rate
            int minHeartRate = chunk4HeartRateArray.Min();
            lblchunk4MinHeartRate.Text = minHeartRate.ToString() + " " + "bpm";

            //calculating power for chunk 4
            totalItems = chunk4PowerInWattsArray.Length;
            int totalPower = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total power
                totalPower = totalPower + chunk4PowerInWattsArray[i];
            }
            //average power
            avgPower = totalPower / totalItems;
            lblchunk4AvgPower.Text = avgPower.ToString() + " " + "watts";
            //maximum power
            int maxPower = chunk4PowerInWattsArray.Max();
            lblchunk4MaxPower.Text = maxPower.ToString() + " " + "watts";

            //calculating altitude for chunk 4
            totalItems = chunk4AltitudeArray.Length;
            int totalAltitude = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total altitude
                totalAltitude = totalAltitude + chunk4AltitudeArray[i];
            }
            //average altitude
            double avgAltitude = Math.Round((double)totalAltitude / totalItems, 2);
            lblchunk4AvgAltitude.Text = avgAltitude.ToString() + " " + "m/ft";
            //maximum altitude
            int maxAltitude = chunk4AltitudeArray.Max();
            lblchunk4MaxAltitude.Text = maxAltitude.ToString() + " " + "m/ft";

            //calculating chunk 4 distance covered
            double totalDistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalSeconds), 2);
            lblchunk4TotalDistance.Text = totalDistance.ToString() + " " + "km/h";
        }
    }
}
