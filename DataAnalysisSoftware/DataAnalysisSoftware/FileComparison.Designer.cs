﻿namespace DataAnalysisSoftware
{
    partial class FileComparison
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeStamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox_file1Summary = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblmaxAltitudePM = new System.Windows.Forms.Label();
            this.lblavgAltitudePM = new System.Windows.Forms.Label();
            this.lblmaxPowerPM = new System.Windows.Forms.Label();
            this.lblavgPowerPM = new System.Windows.Forms.Label();
            this.lblminHeartRatePM = new System.Windows.Forms.Label();
            this.lblmaxHeartRatePM = new System.Windows.Forms.Label();
            this.lblavgHeartRatePM = new System.Windows.Forms.Label();
            this.lblmaxSpeedPM = new System.Windows.Forms.Label();
            this.lblavgSpeedPM = new System.Windows.Forms.Label();
            this.lbltotalDistancePM = new System.Windows.Forms.Label();
            this.lbltotalDistance = new System.Windows.Forms.Label();
            this.lbl_totalDistance = new System.Windows.Forms.Label();
            this.lblmaxAltitude = new System.Windows.Forms.Label();
            this.lblavgAltitude = new System.Windows.Forms.Label();
            this.lblmaxPower = new System.Windows.Forms.Label();
            this.lblavgPower = new System.Windows.Forms.Label();
            this.lblminHeartRate = new System.Windows.Forms.Label();
            this.lblmaxHeartRate = new System.Windows.Forms.Label();
            this.lblavgHeartRate = new System.Windows.Forms.Label();
            this.lblmaxSpeed = new System.Windows.Forms.Label();
            this.lblavgSpeed = new System.Windows.Forms.Label();
            this.lbl_maxAltitude = new System.Windows.Forms.Label();
            this.lbl_avgAltitude = new System.Windows.Forms.Label();
            this.lbl_maxPower = new System.Windows.Forms.Label();
            this.lbl_avgPower = new System.Windows.Forms.Label();
            this.lbl_minHeartRate = new System.Windows.Forms.Label();
            this.lbl_maxHeartRate = new System.Windows.Forms.Label();
            this.lbl_avgHeartRate = new System.Windows.Forms.Label();
            this.lbl_maxSpeed = new System.Windows.Forms.Label();
            this.lbl_avgSpeed = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.groupBox_file2Summary = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblmaxAltitude2PM = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblavgAltitude2PM = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblmaxPower2PM = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblavgPower2PM = new System.Windows.Forms.Label();
            this.lblminHeartRate2PM = new System.Windows.Forms.Label();
            this.lblmaxHeartRate2PM = new System.Windows.Forms.Label();
            this.lbltotalDistance2 = new System.Windows.Forms.Label();
            this.lblavgHeartRate2PM = new System.Windows.Forms.Label();
            this.lbl_totalDistance2 = new System.Windows.Forms.Label();
            this.lblmaxSpeed2PM = new System.Windows.Forms.Label();
            this.lblmaxAltitude2 = new System.Windows.Forms.Label();
            this.lblavgSpeed2PM = new System.Windows.Forms.Label();
            this.lblavgAltitude2 = new System.Windows.Forms.Label();
            this.lbltotalDistance2PM = new System.Windows.Forms.Label();
            this.lblmaxPower2 = new System.Windows.Forms.Label();
            this.lblavgPower2 = new System.Windows.Forms.Label();
            this.lblminHeartRate2 = new System.Windows.Forms.Label();
            this.lblmaxHeartRate2 = new System.Windows.Forms.Label();
            this.lblavgHeartRate2 = new System.Windows.Forms.Label();
            this.lblmaxSpeed2 = new System.Windows.Forms.Label();
            this.lblavgSpeed2 = new System.Windows.Forms.Label();
            this.lbl_maxAltitude2 = new System.Windows.Forms.Label();
            this.lbl_avgAltitude2 = new System.Windows.Forms.Label();
            this.lbl_maxPower2 = new System.Windows.Forms.Label();
            this.lbl_avgPower2 = new System.Windows.Forms.Label();
            this.lbl_minHeartRate2 = new System.Windows.Forms.Label();
            this.lbl_maxHeartRate2 = new System.Windows.Forms.Label();
            this.lbl_avgHeartRate2 = new System.Windows.Forms.Label();
            this.lbl_maxSpeed2 = new System.Windows.Forms.Label();
            this.lbl_avgSpeed2 = new System.Windows.Forms.Label();
            this.File2HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.File2Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.File2Cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.File2Altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.File2Power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.File2PowerIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.File2TimeStamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox_file1Summary.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox_file2Summary.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(286, 32);
            this.label1.TabIndex = 7;
            this.label1.Text = "Multiple File Comparison";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HeartRate,
            this.Speed,
            this.Cadence,
            this.Altitude,
            this.Power,
            this.PowerIndex,
            this.TimeStamp});
            this.dataGridView1.Location = new System.Drawing.Point(0, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(593, 320);
            this.dataGridView1.TabIndex = 8;
            this.dataGridView1.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseUp);
            // 
            // HeartRate
            // 
            this.HeartRate.HeaderText = "Heart Rate";
            this.HeartRate.Name = "HeartRate";
            this.HeartRate.ReadOnly = true;
            // 
            // Speed
            // 
            this.Speed.HeaderText = "Speed";
            this.Speed.Name = "Speed";
            this.Speed.ReadOnly = true;
            // 
            // Cadence
            // 
            this.Cadence.HeaderText = "Cadence";
            this.Cadence.Name = "Cadence";
            this.Cadence.ReadOnly = true;
            // 
            // Altitude
            // 
            this.Altitude.HeaderText = "Altitude";
            this.Altitude.Name = "Altitude";
            this.Altitude.ReadOnly = true;
            // 
            // Power
            // 
            this.Power.HeaderText = "Power";
            this.Power.Name = "Power";
            this.Power.ReadOnly = true;
            // 
            // PowerIndex
            // 
            this.PowerIndex.HeaderText = "Power Index";
            this.PowerIndex.Name = "PowerIndex";
            this.PowerIndex.ReadOnly = true;
            // 
            // TimeStamp
            // 
            this.TimeStamp.HeaderText = "Time Stamp";
            this.TimeStamp.Name = "TimeStamp";
            this.TimeStamp.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox_file1Summary);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(1, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(596, 582);
            this.panel1.TabIndex = 9;
            // 
            // groupBox_file1Summary
            // 
            this.groupBox_file1Summary.BackColor = System.Drawing.Color.White;
            this.groupBox_file1Summary.Controls.Add(this.label6);
            this.groupBox_file1Summary.Controls.Add(this.label7);
            this.groupBox_file1Summary.Controls.Add(this.label8);
            this.groupBox_file1Summary.Controls.Add(this.label9);
            this.groupBox_file1Summary.Controls.Add(this.label10);
            this.groupBox_file1Summary.Controls.Add(this.label2);
            this.groupBox_file1Summary.Controls.Add(this.label3);
            this.groupBox_file1Summary.Controls.Add(this.label4);
            this.groupBox_file1Summary.Controls.Add(this.label5);
            this.groupBox_file1Summary.Controls.Add(this.label11);
            this.groupBox_file1Summary.Controls.Add(this.lblmaxAltitudePM);
            this.groupBox_file1Summary.Controls.Add(this.lblavgAltitudePM);
            this.groupBox_file1Summary.Controls.Add(this.lblmaxPowerPM);
            this.groupBox_file1Summary.Controls.Add(this.lblavgPowerPM);
            this.groupBox_file1Summary.Controls.Add(this.lblminHeartRatePM);
            this.groupBox_file1Summary.Controls.Add(this.lblmaxHeartRatePM);
            this.groupBox_file1Summary.Controls.Add(this.lblavgHeartRatePM);
            this.groupBox_file1Summary.Controls.Add(this.lblmaxSpeedPM);
            this.groupBox_file1Summary.Controls.Add(this.lblavgSpeedPM);
            this.groupBox_file1Summary.Controls.Add(this.lbltotalDistancePM);
            this.groupBox_file1Summary.Controls.Add(this.lbltotalDistance);
            this.groupBox_file1Summary.Controls.Add(this.lbl_totalDistance);
            this.groupBox_file1Summary.Controls.Add(this.lblmaxAltitude);
            this.groupBox_file1Summary.Controls.Add(this.lblavgAltitude);
            this.groupBox_file1Summary.Controls.Add(this.lblmaxPower);
            this.groupBox_file1Summary.Controls.Add(this.lblavgPower);
            this.groupBox_file1Summary.Controls.Add(this.lblminHeartRate);
            this.groupBox_file1Summary.Controls.Add(this.lblmaxHeartRate);
            this.groupBox_file1Summary.Controls.Add(this.lblavgHeartRate);
            this.groupBox_file1Summary.Controls.Add(this.lblmaxSpeed);
            this.groupBox_file1Summary.Controls.Add(this.lblavgSpeed);
            this.groupBox_file1Summary.Controls.Add(this.lbl_maxAltitude);
            this.groupBox_file1Summary.Controls.Add(this.lbl_avgAltitude);
            this.groupBox_file1Summary.Controls.Add(this.lbl_maxPower);
            this.groupBox_file1Summary.Controls.Add(this.lbl_avgPower);
            this.groupBox_file1Summary.Controls.Add(this.lbl_minHeartRate);
            this.groupBox_file1Summary.Controls.Add(this.lbl_maxHeartRate);
            this.groupBox_file1Summary.Controls.Add(this.lbl_avgHeartRate);
            this.groupBox_file1Summary.Controls.Add(this.lbl_maxSpeed);
            this.groupBox_file1Summary.Controls.Add(this.lbl_avgSpeed);
            this.groupBox_file1Summary.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_file1Summary.Location = new System.Drawing.Point(0, 329);
            this.groupBox_file1Summary.Name = "groupBox_file1Summary";
            this.groupBox_file1Summary.Size = new System.Drawing.Size(593, 253);
            this.groupBox_file1Summary.TabIndex = 34;
            this.groupBox_file1Summary.TabStop = false;
            this.groupBox_file1Summary.Text = "File 1 Summary Data";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(486, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 15);
            this.label6.TabIndex = 51;
            this.label6.Text = "bpm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(486, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 15);
            this.label7.TabIndex = 50;
            this.label7.Text = "m/ft";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(486, 162);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 15);
            this.label8.TabIndex = 49;
            this.label8.Text = "m/ft";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(486, 124);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 15);
            this.label9.TabIndex = 48;
            this.label9.Text = "watts";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(486, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 15);
            this.label10.TabIndex = 47;
            this.label10.Text = "watts";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(200, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 15);
            this.label2.TabIndex = 46;
            this.label2.Text = "km/h";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(200, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 15);
            this.label3.TabIndex = 45;
            this.label3.Text = "bpm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(200, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 15);
            this.label4.TabIndex = 44;
            this.label4.Text = "bpm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(200, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 15);
            this.label5.TabIndex = 43;
            this.label5.Text = "km/h";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(200, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 15);
            this.label11.TabIndex = 42;
            this.label11.Text = "km/h";
            // 
            // lblmaxAltitudePM
            // 
            this.lblmaxAltitudePM.AutoSize = true;
            this.lblmaxAltitudePM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxAltitudePM.ForeColor = System.Drawing.Color.Black;
            this.lblmaxAltitudePM.Location = new System.Drawing.Point(529, 203);
            this.lblmaxAltitudePM.Name = "lblmaxAltitudePM";
            this.lblmaxAltitudePM.Size = new System.Drawing.Size(12, 15);
            this.lblmaxAltitudePM.TabIndex = 41;
            this.lblmaxAltitudePM.Text = "-";
            // 
            // lblavgAltitudePM
            // 
            this.lblavgAltitudePM.AutoSize = true;
            this.lblavgAltitudePM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgAltitudePM.ForeColor = System.Drawing.Color.Black;
            this.lblavgAltitudePM.Location = new System.Drawing.Point(529, 163);
            this.lblavgAltitudePM.Name = "lblavgAltitudePM";
            this.lblavgAltitudePM.Size = new System.Drawing.Size(12, 15);
            this.lblavgAltitudePM.TabIndex = 40;
            this.lblavgAltitudePM.Text = "-";
            // 
            // lblmaxPowerPM
            // 
            this.lblmaxPowerPM.AutoSize = true;
            this.lblmaxPowerPM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxPowerPM.ForeColor = System.Drawing.Color.Black;
            this.lblmaxPowerPM.Location = new System.Drawing.Point(529, 125);
            this.lblmaxPowerPM.Name = "lblmaxPowerPM";
            this.lblmaxPowerPM.Size = new System.Drawing.Size(12, 15);
            this.lblmaxPowerPM.TabIndex = 39;
            this.lblmaxPowerPM.Text = "-";
            // 
            // lblavgPowerPM
            // 
            this.lblavgPowerPM.AutoSize = true;
            this.lblavgPowerPM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgPowerPM.ForeColor = System.Drawing.Color.Black;
            this.lblavgPowerPM.Location = new System.Drawing.Point(529, 86);
            this.lblavgPowerPM.Name = "lblavgPowerPM";
            this.lblavgPowerPM.Size = new System.Drawing.Size(12, 15);
            this.lblavgPowerPM.TabIndex = 38;
            this.lblavgPowerPM.Text = "-";
            // 
            // lblminHeartRatePM
            // 
            this.lblminHeartRatePM.AutoSize = true;
            this.lblminHeartRatePM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblminHeartRatePM.ForeColor = System.Drawing.Color.Black;
            this.lblminHeartRatePM.Location = new System.Drawing.Point(529, 50);
            this.lblminHeartRatePM.Name = "lblminHeartRatePM";
            this.lblminHeartRatePM.Size = new System.Drawing.Size(12, 15);
            this.lblminHeartRatePM.TabIndex = 37;
            this.lblminHeartRatePM.Text = "-";
            // 
            // lblmaxHeartRatePM
            // 
            this.lblmaxHeartRatePM.AutoSize = true;
            this.lblmaxHeartRatePM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxHeartRatePM.ForeColor = System.Drawing.Color.Black;
            this.lblmaxHeartRatePM.Location = new System.Drawing.Point(259, 203);
            this.lblmaxHeartRatePM.Name = "lblmaxHeartRatePM";
            this.lblmaxHeartRatePM.Size = new System.Drawing.Size(12, 15);
            this.lblmaxHeartRatePM.TabIndex = 36;
            this.lblmaxHeartRatePM.Text = "-";
            // 
            // lblavgHeartRatePM
            // 
            this.lblavgHeartRatePM.AutoSize = true;
            this.lblavgHeartRatePM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgHeartRatePM.ForeColor = System.Drawing.Color.Black;
            this.lblavgHeartRatePM.Location = new System.Drawing.Point(259, 163);
            this.lblavgHeartRatePM.Name = "lblavgHeartRatePM";
            this.lblavgHeartRatePM.Size = new System.Drawing.Size(12, 15);
            this.lblavgHeartRatePM.TabIndex = 35;
            this.lblavgHeartRatePM.Text = "-";
            // 
            // lblmaxSpeedPM
            // 
            this.lblmaxSpeedPM.AutoSize = true;
            this.lblmaxSpeedPM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxSpeedPM.ForeColor = System.Drawing.Color.Black;
            this.lblmaxSpeedPM.Location = new System.Drawing.Point(259, 125);
            this.lblmaxSpeedPM.Name = "lblmaxSpeedPM";
            this.lblmaxSpeedPM.Size = new System.Drawing.Size(12, 15);
            this.lblmaxSpeedPM.TabIndex = 34;
            this.lblmaxSpeedPM.Text = "-";
            // 
            // lblavgSpeedPM
            // 
            this.lblavgSpeedPM.AutoSize = true;
            this.lblavgSpeedPM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgSpeedPM.ForeColor = System.Drawing.Color.Black;
            this.lblavgSpeedPM.Location = new System.Drawing.Point(259, 86);
            this.lblavgSpeedPM.Name = "lblavgSpeedPM";
            this.lblavgSpeedPM.Size = new System.Drawing.Size(12, 15);
            this.lblavgSpeedPM.TabIndex = 33;
            this.lblavgSpeedPM.Text = "-";
            // 
            // lbltotalDistancePM
            // 
            this.lbltotalDistancePM.AutoSize = true;
            this.lbltotalDistancePM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotalDistancePM.ForeColor = System.Drawing.Color.Black;
            this.lbltotalDistancePM.Location = new System.Drawing.Point(259, 50);
            this.lbltotalDistancePM.Name = "lbltotalDistancePM";
            this.lbltotalDistancePM.Size = new System.Drawing.Size(12, 15);
            this.lbltotalDistancePM.TabIndex = 32;
            this.lbltotalDistancePM.Text = "-";
            // 
            // lbltotalDistance
            // 
            this.lbltotalDistance.AutoSize = true;
            this.lbltotalDistance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotalDistance.ForeColor = System.Drawing.Color.Black;
            this.lbltotalDistance.Location = new System.Drawing.Point(153, 51);
            this.lbltotalDistance.Name = "lbltotalDistance";
            this.lbltotalDistance.Size = new System.Drawing.Size(13, 15);
            this.lbltotalDistance.TabIndex = 31;
            this.lbltotalDistance.Text = "0";
            // 
            // lbl_totalDistance
            // 
            this.lbl_totalDistance.AutoSize = true;
            this.lbl_totalDistance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalDistance.ForeColor = System.Drawing.Color.Blue;
            this.lbl_totalDistance.Location = new System.Drawing.Point(6, 52);
            this.lbl_totalDistance.Name = "lbl_totalDistance";
            this.lbl_totalDistance.Size = new System.Drawing.Size(134, 15);
            this.lbl_totalDistance.TabIndex = 30;
            this.lbl_totalDistance.Text = "Total Distance Covered :";
            // 
            // lblmaxAltitude
            // 
            this.lblmaxAltitude.AutoSize = true;
            this.lblmaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblmaxAltitude.Location = new System.Drawing.Point(435, 204);
            this.lblmaxAltitude.Name = "lblmaxAltitude";
            this.lblmaxAltitude.Size = new System.Drawing.Size(13, 15);
            this.lblmaxAltitude.TabIndex = 29;
            this.lblmaxAltitude.Text = "0";
            // 
            // lblavgAltitude
            // 
            this.lblavgAltitude.AutoSize = true;
            this.lblavgAltitude.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblavgAltitude.Location = new System.Drawing.Point(435, 166);
            this.lblavgAltitude.Name = "lblavgAltitude";
            this.lblavgAltitude.Size = new System.Drawing.Size(13, 15);
            this.lblavgAltitude.TabIndex = 28;
            this.lblavgAltitude.Text = "0";
            // 
            // lblmaxPower
            // 
            this.lblmaxPower.AutoSize = true;
            this.lblmaxPower.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxPower.ForeColor = System.Drawing.Color.Black;
            this.lblmaxPower.Location = new System.Drawing.Point(435, 127);
            this.lblmaxPower.Name = "lblmaxPower";
            this.lblmaxPower.Size = new System.Drawing.Size(13, 15);
            this.lblmaxPower.TabIndex = 27;
            this.lblmaxPower.Text = "0";
            // 
            // lblavgPower
            // 
            this.lblavgPower.AutoSize = true;
            this.lblavgPower.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgPower.ForeColor = System.Drawing.Color.Black;
            this.lblavgPower.Location = new System.Drawing.Point(435, 89);
            this.lblavgPower.Name = "lblavgPower";
            this.lblavgPower.Size = new System.Drawing.Size(13, 15);
            this.lblavgPower.TabIndex = 26;
            this.lblavgPower.Text = "0";
            // 
            // lblminHeartRate
            // 
            this.lblminHeartRate.AutoSize = true;
            this.lblminHeartRate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblminHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblminHeartRate.Location = new System.Drawing.Point(435, 50);
            this.lblminHeartRate.Name = "lblminHeartRate";
            this.lblminHeartRate.Size = new System.Drawing.Size(13, 15);
            this.lblminHeartRate.TabIndex = 25;
            this.lblminHeartRate.Text = "0";
            // 
            // lblmaxHeartRate
            // 
            this.lblmaxHeartRate.AutoSize = true;
            this.lblmaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblmaxHeartRate.Location = new System.Drawing.Point(153, 203);
            this.lblmaxHeartRate.Name = "lblmaxHeartRate";
            this.lblmaxHeartRate.Size = new System.Drawing.Size(13, 15);
            this.lblmaxHeartRate.TabIndex = 24;
            this.lblmaxHeartRate.Text = "0";
            // 
            // lblavgHeartRate
            // 
            this.lblavgHeartRate.AutoSize = true;
            this.lblavgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblavgHeartRate.Location = new System.Drawing.Point(153, 162);
            this.lblavgHeartRate.Name = "lblavgHeartRate";
            this.lblavgHeartRate.Size = new System.Drawing.Size(13, 15);
            this.lblavgHeartRate.TabIndex = 23;
            this.lblavgHeartRate.Text = "0";
            // 
            // lblmaxSpeed
            // 
            this.lblmaxSpeed.AutoSize = true;
            this.lblmaxSpeed.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblmaxSpeed.Location = new System.Drawing.Point(153, 124);
            this.lblmaxSpeed.Name = "lblmaxSpeed";
            this.lblmaxSpeed.Size = new System.Drawing.Size(13, 15);
            this.lblmaxSpeed.TabIndex = 22;
            this.lblmaxSpeed.Text = "0";
            // 
            // lblavgSpeed
            // 
            this.lblavgSpeed.AutoSize = true;
            this.lblavgSpeed.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblavgSpeed.Location = new System.Drawing.Point(153, 86);
            this.lblavgSpeed.Name = "lblavgSpeed";
            this.lblavgSpeed.Size = new System.Drawing.Size(13, 15);
            this.lblavgSpeed.TabIndex = 21;
            this.lblavgSpeed.Text = "0";
            // 
            // lbl_maxAltitude
            // 
            this.lbl_maxAltitude.AutoSize = true;
            this.lbl_maxAltitude.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxAltitude.Location = new System.Drawing.Point(295, 205);
            this.lbl_maxAltitude.Name = "lbl_maxAltitude";
            this.lbl_maxAltitude.Size = new System.Drawing.Size(112, 15);
            this.lbl_maxAltitude.TabIndex = 20;
            this.lbl_maxAltitude.Text = "Maximum Altitude :";
            // 
            // lbl_avgAltitude
            // 
            this.lbl_avgAltitude.AutoSize = true;
            this.lbl_avgAltitude.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgAltitude.Location = new System.Drawing.Point(295, 167);
            this.lbl_avgAltitude.Name = "lbl_avgAltitude";
            this.lbl_avgAltitude.Size = new System.Drawing.Size(101, 15);
            this.lbl_avgAltitude.TabIndex = 19;
            this.lbl_avgAltitude.Text = "Average Altitude :";
            // 
            // lbl_maxPower
            // 
            this.lbl_maxPower.AutoSize = true;
            this.lbl_maxPower.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxPower.Location = new System.Drawing.Point(295, 128);
            this.lbl_maxPower.Name = "lbl_maxPower";
            this.lbl_maxPower.Size = new System.Drawing.Size(103, 15);
            this.lbl_maxPower.TabIndex = 18;
            this.lbl_maxPower.Text = "Maximum Power :";
            // 
            // lbl_avgPower
            // 
            this.lbl_avgPower.AutoSize = true;
            this.lbl_avgPower.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgPower.Location = new System.Drawing.Point(295, 90);
            this.lbl_avgPower.Name = "lbl_avgPower";
            this.lbl_avgPower.Size = new System.Drawing.Size(92, 15);
            this.lbl_avgPower.TabIndex = 17;
            this.lbl_avgPower.Text = "Average Power :";
            // 
            // lbl_minHeartRate
            // 
            this.lbl_minHeartRate.AutoSize = true;
            this.lbl_minHeartRate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_minHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_minHeartRate.Location = new System.Drawing.Point(295, 51);
            this.lbl_minHeartRate.Name = "lbl_minHeartRate";
            this.lbl_minHeartRate.Size = new System.Drawing.Size(124, 15);
            this.lbl_minHeartRate.TabIndex = 16;
            this.lbl_minHeartRate.Text = "Minimum Heart Rate :";
            // 
            // lbl_maxHeartRate
            // 
            this.lbl_maxHeartRate.AutoSize = true;
            this.lbl_maxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxHeartRate.Location = new System.Drawing.Point(6, 204);
            this.lbl_maxHeartRate.Name = "lbl_maxHeartRate";
            this.lbl_maxHeartRate.Size = new System.Drawing.Size(125, 15);
            this.lbl_maxHeartRate.TabIndex = 15;
            this.lbl_maxHeartRate.Text = "Maximum Heart Rate :";
            // 
            // lbl_avgHeartRate
            // 
            this.lbl_avgHeartRate.AutoSize = true;
            this.lbl_avgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgHeartRate.Location = new System.Drawing.Point(6, 163);
            this.lbl_avgHeartRate.Name = "lbl_avgHeartRate";
            this.lbl_avgHeartRate.Size = new System.Drawing.Size(114, 15);
            this.lbl_avgHeartRate.TabIndex = 14;
            this.lbl_avgHeartRate.Text = "Average Heart Rate :";
            // 
            // lbl_maxSpeed
            // 
            this.lbl_maxSpeed.AutoSize = true;
            this.lbl_maxSpeed.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxSpeed.Location = new System.Drawing.Point(6, 125);
            this.lbl_maxSpeed.Name = "lbl_maxSpeed";
            this.lbl_maxSpeed.Size = new System.Drawing.Size(102, 15);
            this.lbl_maxSpeed.TabIndex = 13;
            this.lbl_maxSpeed.Text = "Maximum Speed :";
            // 
            // lbl_avgSpeed
            // 
            this.lbl_avgSpeed.AutoSize = true;
            this.lbl_avgSpeed.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgSpeed.Location = new System.Drawing.Point(6, 87);
            this.lbl_avgSpeed.Name = "lbl_avgSpeed";
            this.lbl_avgSpeed.Size = new System.Drawing.Size(91, 15);
            this.lbl_avgSpeed.TabIndex = 12;
            this.lbl_avgSpeed.Text = "Average Speed :";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox_file2Summary);
            this.panel2.Controls.Add(this.dataGridView2);
            this.panel2.Location = new System.Drawing.Point(600, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(596, 582);
            this.panel2.TabIndex = 10;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.File2HeartRate,
            this.File2Speed,
            this.File2Cadence,
            this.File2Altitude,
            this.File2Power,
            this.File2PowerIndex,
            this.File2TimeStamp});
            this.dataGridView2.Location = new System.Drawing.Point(0, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(593, 320);
            this.dataGridView2.TabIndex = 8;
            this.dataGridView2.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView2_CellMouseUp);
            // 
            // groupBox_file2Summary
            // 
            this.groupBox_file2Summary.BackColor = System.Drawing.Color.White;
            this.groupBox_file2Summary.Controls.Add(this.label16);
            this.groupBox_file2Summary.Controls.Add(this.label17);
            this.groupBox_file2Summary.Controls.Add(this.label12);
            this.groupBox_file2Summary.Controls.Add(this.label18);
            this.groupBox_file2Summary.Controls.Add(this.label13);
            this.groupBox_file2Summary.Controls.Add(this.label19);
            this.groupBox_file2Summary.Controls.Add(this.lblmaxAltitude2PM);
            this.groupBox_file2Summary.Controls.Add(this.label20);
            this.groupBox_file2Summary.Controls.Add(this.label14);
            this.groupBox_file2Summary.Controls.Add(this.lblavgAltitude2PM);
            this.groupBox_file2Summary.Controls.Add(this.label15);
            this.groupBox_file2Summary.Controls.Add(this.lblmaxPower2PM);
            this.groupBox_file2Summary.Controls.Add(this.label21);
            this.groupBox_file2Summary.Controls.Add(this.lblavgPower2PM);
            this.groupBox_file2Summary.Controls.Add(this.lblminHeartRate2PM);
            this.groupBox_file2Summary.Controls.Add(this.lblmaxHeartRate2PM);
            this.groupBox_file2Summary.Controls.Add(this.lbltotalDistance2);
            this.groupBox_file2Summary.Controls.Add(this.lblavgHeartRate2PM);
            this.groupBox_file2Summary.Controls.Add(this.lbl_totalDistance2);
            this.groupBox_file2Summary.Controls.Add(this.lblmaxSpeed2PM);
            this.groupBox_file2Summary.Controls.Add(this.lblmaxAltitude2);
            this.groupBox_file2Summary.Controls.Add(this.lblavgSpeed2PM);
            this.groupBox_file2Summary.Controls.Add(this.lblavgAltitude2);
            this.groupBox_file2Summary.Controls.Add(this.lbltotalDistance2PM);
            this.groupBox_file2Summary.Controls.Add(this.lblmaxPower2);
            this.groupBox_file2Summary.Controls.Add(this.lblavgPower2);
            this.groupBox_file2Summary.Controls.Add(this.lblminHeartRate2);
            this.groupBox_file2Summary.Controls.Add(this.lblmaxHeartRate2);
            this.groupBox_file2Summary.Controls.Add(this.lblavgHeartRate2);
            this.groupBox_file2Summary.Controls.Add(this.lblmaxSpeed2);
            this.groupBox_file2Summary.Controls.Add(this.lblavgSpeed2);
            this.groupBox_file2Summary.Controls.Add(this.lbl_maxAltitude2);
            this.groupBox_file2Summary.Controls.Add(this.lbl_avgAltitude2);
            this.groupBox_file2Summary.Controls.Add(this.lbl_maxPower2);
            this.groupBox_file2Summary.Controls.Add(this.lbl_avgPower2);
            this.groupBox_file2Summary.Controls.Add(this.lbl_minHeartRate2);
            this.groupBox_file2Summary.Controls.Add(this.lbl_maxHeartRate2);
            this.groupBox_file2Summary.Controls.Add(this.lbl_avgHeartRate2);
            this.groupBox_file2Summary.Controls.Add(this.lbl_maxSpeed2);
            this.groupBox_file2Summary.Controls.Add(this.lbl_avgSpeed2);
            this.groupBox_file2Summary.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_file2Summary.Location = new System.Drawing.Point(2, 329);
            this.groupBox_file2Summary.Name = "groupBox_file2Summary";
            this.groupBox_file2Summary.Size = new System.Drawing.Size(594, 253);
            this.groupBox_file2Summary.TabIndex = 35;
            this.groupBox_file2Summary.TabStop = false;
            this.groupBox_file2Summary.Text = "File 2 Summary Data";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(491, 52);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 15);
            this.label16.TabIndex = 56;
            this.label16.Text = "bpm";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(491, 204);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 15);
            this.label17.TabIndex = 55;
            this.label17.Text = "m/ft";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(196, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 15);
            this.label12.TabIndex = 56;
            this.label12.Text = "km/h";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(491, 163);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 15);
            this.label18.TabIndex = 54;
            this.label18.Text = "m/ft";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(196, 204);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 15);
            this.label13.TabIndex = 55;
            this.label13.Text = "bpm";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(491, 125);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 15);
            this.label19.TabIndex = 53;
            this.label19.Text = "watts";
            // 
            // lblmaxAltitude2PM
            // 
            this.lblmaxAltitude2PM.AutoSize = true;
            this.lblmaxAltitude2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxAltitude2PM.ForeColor = System.Drawing.Color.Black;
            this.lblmaxAltitude2PM.Location = new System.Drawing.Point(541, 204);
            this.lblmaxAltitude2PM.Name = "lblmaxAltitude2PM";
            this.lblmaxAltitude2PM.Size = new System.Drawing.Size(12, 15);
            this.lblmaxAltitude2PM.TabIndex = 51;
            this.lblmaxAltitude2PM.Text = "-";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(491, 87);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 15);
            this.label20.TabIndex = 52;
            this.label20.Text = "watts";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(196, 163);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 15);
            this.label14.TabIndex = 54;
            this.label14.Text = "bpm";
            // 
            // lblavgAltitude2PM
            // 
            this.lblavgAltitude2PM.AutoSize = true;
            this.lblavgAltitude2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgAltitude2PM.ForeColor = System.Drawing.Color.Black;
            this.lblavgAltitude2PM.Location = new System.Drawing.Point(541, 164);
            this.lblavgAltitude2PM.Name = "lblavgAltitude2PM";
            this.lblavgAltitude2PM.Size = new System.Drawing.Size(12, 15);
            this.lblavgAltitude2PM.TabIndex = 50;
            this.lblavgAltitude2PM.Text = "-";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(196, 125);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 15);
            this.label15.TabIndex = 53;
            this.label15.Text = "km/h";
            // 
            // lblmaxPower2PM
            // 
            this.lblmaxPower2PM.AutoSize = true;
            this.lblmaxPower2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxPower2PM.ForeColor = System.Drawing.Color.Black;
            this.lblmaxPower2PM.Location = new System.Drawing.Point(541, 126);
            this.lblmaxPower2PM.Name = "lblmaxPower2PM";
            this.lblmaxPower2PM.Size = new System.Drawing.Size(12, 15);
            this.lblmaxPower2PM.TabIndex = 49;
            this.lblmaxPower2PM.Text = "-";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(196, 87);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 15);
            this.label21.TabIndex = 52;
            this.label21.Text = "km/h";
            // 
            // lblavgPower2PM
            // 
            this.lblavgPower2PM.AutoSize = true;
            this.lblavgPower2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgPower2PM.ForeColor = System.Drawing.Color.Black;
            this.lblavgPower2PM.Location = new System.Drawing.Point(541, 87);
            this.lblavgPower2PM.Name = "lblavgPower2PM";
            this.lblavgPower2PM.Size = new System.Drawing.Size(12, 15);
            this.lblavgPower2PM.TabIndex = 48;
            this.lblavgPower2PM.Text = "-";
            // 
            // lblminHeartRate2PM
            // 
            this.lblminHeartRate2PM.AutoSize = true;
            this.lblminHeartRate2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblminHeartRate2PM.ForeColor = System.Drawing.Color.Black;
            this.lblminHeartRate2PM.Location = new System.Drawing.Point(541, 51);
            this.lblminHeartRate2PM.Name = "lblminHeartRate2PM";
            this.lblminHeartRate2PM.Size = new System.Drawing.Size(12, 15);
            this.lblminHeartRate2PM.TabIndex = 47;
            this.lblminHeartRate2PM.Text = "-";
            // 
            // lblmaxHeartRate2PM
            // 
            this.lblmaxHeartRate2PM.AutoSize = true;
            this.lblmaxHeartRate2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxHeartRate2PM.ForeColor = System.Drawing.Color.Black;
            this.lblmaxHeartRate2PM.Location = new System.Drawing.Point(246, 205);
            this.lblmaxHeartRate2PM.Name = "lblmaxHeartRate2PM";
            this.lblmaxHeartRate2PM.Size = new System.Drawing.Size(12, 15);
            this.lblmaxHeartRate2PM.TabIndex = 46;
            this.lblmaxHeartRate2PM.Text = "-";
            // 
            // lbltotalDistance2
            // 
            this.lbltotalDistance2.AutoSize = true;
            this.lbltotalDistance2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotalDistance2.ForeColor = System.Drawing.Color.Black;
            this.lbltotalDistance2.Location = new System.Drawing.Point(145, 51);
            this.lbltotalDistance2.Name = "lbltotalDistance2";
            this.lbltotalDistance2.Size = new System.Drawing.Size(13, 15);
            this.lbltotalDistance2.TabIndex = 31;
            this.lbltotalDistance2.Text = "0";
            // 
            // lblavgHeartRate2PM
            // 
            this.lblavgHeartRate2PM.AutoSize = true;
            this.lblavgHeartRate2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgHeartRate2PM.ForeColor = System.Drawing.Color.Black;
            this.lblavgHeartRate2PM.Location = new System.Drawing.Point(246, 165);
            this.lblavgHeartRate2PM.Name = "lblavgHeartRate2PM";
            this.lblavgHeartRate2PM.Size = new System.Drawing.Size(12, 15);
            this.lblavgHeartRate2PM.TabIndex = 45;
            this.lblavgHeartRate2PM.Text = "-";
            // 
            // lbl_totalDistance2
            // 
            this.lbl_totalDistance2.AutoSize = true;
            this.lbl_totalDistance2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalDistance2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_totalDistance2.Location = new System.Drawing.Point(6, 52);
            this.lbl_totalDistance2.Name = "lbl_totalDistance2";
            this.lbl_totalDistance2.Size = new System.Drawing.Size(134, 15);
            this.lbl_totalDistance2.TabIndex = 30;
            this.lbl_totalDistance2.Text = "Total Distance Covered :";
            // 
            // lblmaxSpeed2PM
            // 
            this.lblmaxSpeed2PM.AutoSize = true;
            this.lblmaxSpeed2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxSpeed2PM.ForeColor = System.Drawing.Color.Black;
            this.lblmaxSpeed2PM.Location = new System.Drawing.Point(246, 127);
            this.lblmaxSpeed2PM.Name = "lblmaxSpeed2PM";
            this.lblmaxSpeed2PM.Size = new System.Drawing.Size(12, 15);
            this.lblmaxSpeed2PM.TabIndex = 44;
            this.lblmaxSpeed2PM.Text = "-";
            // 
            // lblmaxAltitude2
            // 
            this.lblmaxAltitude2.AutoSize = true;
            this.lblmaxAltitude2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxAltitude2.ForeColor = System.Drawing.Color.Black;
            this.lblmaxAltitude2.Location = new System.Drawing.Point(440, 204);
            this.lblmaxAltitude2.Name = "lblmaxAltitude2";
            this.lblmaxAltitude2.Size = new System.Drawing.Size(13, 15);
            this.lblmaxAltitude2.TabIndex = 29;
            this.lblmaxAltitude2.Text = "0";
            // 
            // lblavgSpeed2PM
            // 
            this.lblavgSpeed2PM.AutoSize = true;
            this.lblavgSpeed2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgSpeed2PM.ForeColor = System.Drawing.Color.Black;
            this.lblavgSpeed2PM.Location = new System.Drawing.Point(246, 88);
            this.lblavgSpeed2PM.Name = "lblavgSpeed2PM";
            this.lblavgSpeed2PM.Size = new System.Drawing.Size(12, 15);
            this.lblavgSpeed2PM.TabIndex = 43;
            this.lblavgSpeed2PM.Text = "-";
            // 
            // lblavgAltitude2
            // 
            this.lblavgAltitude2.AutoSize = true;
            this.lblavgAltitude2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgAltitude2.ForeColor = System.Drawing.Color.Black;
            this.lblavgAltitude2.Location = new System.Drawing.Point(440, 166);
            this.lblavgAltitude2.Name = "lblavgAltitude2";
            this.lblavgAltitude2.Size = new System.Drawing.Size(13, 15);
            this.lblavgAltitude2.TabIndex = 28;
            this.lblavgAltitude2.Text = "0";
            // 
            // lbltotalDistance2PM
            // 
            this.lbltotalDistance2PM.AutoSize = true;
            this.lbltotalDistance2PM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotalDistance2PM.ForeColor = System.Drawing.Color.Black;
            this.lbltotalDistance2PM.Location = new System.Drawing.Point(246, 52);
            this.lbltotalDistance2PM.Name = "lbltotalDistance2PM";
            this.lbltotalDistance2PM.Size = new System.Drawing.Size(12, 15);
            this.lbltotalDistance2PM.TabIndex = 42;
            this.lbltotalDistance2PM.Text = "-";
            // 
            // lblmaxPower2
            // 
            this.lblmaxPower2.AutoSize = true;
            this.lblmaxPower2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxPower2.ForeColor = System.Drawing.Color.Black;
            this.lblmaxPower2.Location = new System.Drawing.Point(440, 127);
            this.lblmaxPower2.Name = "lblmaxPower2";
            this.lblmaxPower2.Size = new System.Drawing.Size(13, 15);
            this.lblmaxPower2.TabIndex = 27;
            this.lblmaxPower2.Text = "0";
            // 
            // lblavgPower2
            // 
            this.lblavgPower2.AutoSize = true;
            this.lblavgPower2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgPower2.ForeColor = System.Drawing.Color.Black;
            this.lblavgPower2.Location = new System.Drawing.Point(440, 89);
            this.lblavgPower2.Name = "lblavgPower2";
            this.lblavgPower2.Size = new System.Drawing.Size(13, 15);
            this.lblavgPower2.TabIndex = 26;
            this.lblavgPower2.Text = "0";
            // 
            // lblminHeartRate2
            // 
            this.lblminHeartRate2.AutoSize = true;
            this.lblminHeartRate2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblminHeartRate2.ForeColor = System.Drawing.Color.Black;
            this.lblminHeartRate2.Location = new System.Drawing.Point(440, 50);
            this.lblminHeartRate2.Name = "lblminHeartRate2";
            this.lblminHeartRate2.Size = new System.Drawing.Size(13, 15);
            this.lblminHeartRate2.TabIndex = 25;
            this.lblminHeartRate2.Text = "0";
            // 
            // lblmaxHeartRate2
            // 
            this.lblmaxHeartRate2.AutoSize = true;
            this.lblmaxHeartRate2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxHeartRate2.ForeColor = System.Drawing.Color.Black;
            this.lblmaxHeartRate2.Location = new System.Drawing.Point(145, 203);
            this.lblmaxHeartRate2.Name = "lblmaxHeartRate2";
            this.lblmaxHeartRate2.Size = new System.Drawing.Size(13, 15);
            this.lblmaxHeartRate2.TabIndex = 24;
            this.lblmaxHeartRate2.Text = "0";
            // 
            // lblavgHeartRate2
            // 
            this.lblavgHeartRate2.AutoSize = true;
            this.lblavgHeartRate2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgHeartRate2.ForeColor = System.Drawing.Color.Black;
            this.lblavgHeartRate2.Location = new System.Drawing.Point(145, 162);
            this.lblavgHeartRate2.Name = "lblavgHeartRate2";
            this.lblavgHeartRate2.Size = new System.Drawing.Size(13, 15);
            this.lblavgHeartRate2.TabIndex = 23;
            this.lblavgHeartRate2.Text = "0";
            // 
            // lblmaxSpeed2
            // 
            this.lblmaxSpeed2.AutoSize = true;
            this.lblmaxSpeed2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxSpeed2.ForeColor = System.Drawing.Color.Black;
            this.lblmaxSpeed2.Location = new System.Drawing.Point(145, 124);
            this.lblmaxSpeed2.Name = "lblmaxSpeed2";
            this.lblmaxSpeed2.Size = new System.Drawing.Size(13, 15);
            this.lblmaxSpeed2.TabIndex = 22;
            this.lblmaxSpeed2.Text = "0";
            // 
            // lblavgSpeed2
            // 
            this.lblavgSpeed2.AutoSize = true;
            this.lblavgSpeed2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgSpeed2.ForeColor = System.Drawing.Color.Black;
            this.lblavgSpeed2.Location = new System.Drawing.Point(145, 86);
            this.lblavgSpeed2.Name = "lblavgSpeed2";
            this.lblavgSpeed2.Size = new System.Drawing.Size(13, 15);
            this.lblavgSpeed2.TabIndex = 21;
            this.lblavgSpeed2.Text = "0";
            // 
            // lbl_maxAltitude2
            // 
            this.lbl_maxAltitude2.AutoSize = true;
            this.lbl_maxAltitude2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxAltitude2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxAltitude2.Location = new System.Drawing.Point(295, 205);
            this.lbl_maxAltitude2.Name = "lbl_maxAltitude2";
            this.lbl_maxAltitude2.Size = new System.Drawing.Size(112, 15);
            this.lbl_maxAltitude2.TabIndex = 20;
            this.lbl_maxAltitude2.Text = "Maximum Altitude :";
            // 
            // lbl_avgAltitude2
            // 
            this.lbl_avgAltitude2.AutoSize = true;
            this.lbl_avgAltitude2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgAltitude2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgAltitude2.Location = new System.Drawing.Point(295, 167);
            this.lbl_avgAltitude2.Name = "lbl_avgAltitude2";
            this.lbl_avgAltitude2.Size = new System.Drawing.Size(101, 15);
            this.lbl_avgAltitude2.TabIndex = 19;
            this.lbl_avgAltitude2.Text = "Average Altitude :";
            // 
            // lbl_maxPower2
            // 
            this.lbl_maxPower2.AutoSize = true;
            this.lbl_maxPower2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxPower2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxPower2.Location = new System.Drawing.Point(295, 128);
            this.lbl_maxPower2.Name = "lbl_maxPower2";
            this.lbl_maxPower2.Size = new System.Drawing.Size(103, 15);
            this.lbl_maxPower2.TabIndex = 18;
            this.lbl_maxPower2.Text = "Maximum Power :";
            // 
            // lbl_avgPower2
            // 
            this.lbl_avgPower2.AutoSize = true;
            this.lbl_avgPower2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgPower2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgPower2.Location = new System.Drawing.Point(295, 90);
            this.lbl_avgPower2.Name = "lbl_avgPower2";
            this.lbl_avgPower2.Size = new System.Drawing.Size(92, 15);
            this.lbl_avgPower2.TabIndex = 17;
            this.lbl_avgPower2.Text = "Average Power :";
            // 
            // lbl_minHeartRate2
            // 
            this.lbl_minHeartRate2.AutoSize = true;
            this.lbl_minHeartRate2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_minHeartRate2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_minHeartRate2.Location = new System.Drawing.Point(295, 51);
            this.lbl_minHeartRate2.Name = "lbl_minHeartRate2";
            this.lbl_minHeartRate2.Size = new System.Drawing.Size(124, 15);
            this.lbl_minHeartRate2.TabIndex = 16;
            this.lbl_minHeartRate2.Text = "Minimum Heart Rate :";
            // 
            // lbl_maxHeartRate2
            // 
            this.lbl_maxHeartRate2.AutoSize = true;
            this.lbl_maxHeartRate2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxHeartRate2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxHeartRate2.Location = new System.Drawing.Point(6, 204);
            this.lbl_maxHeartRate2.Name = "lbl_maxHeartRate2";
            this.lbl_maxHeartRate2.Size = new System.Drawing.Size(125, 15);
            this.lbl_maxHeartRate2.TabIndex = 15;
            this.lbl_maxHeartRate2.Text = "Maximum Heart Rate :";
            // 
            // lbl_avgHeartRate2
            // 
            this.lbl_avgHeartRate2.AutoSize = true;
            this.lbl_avgHeartRate2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgHeartRate2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgHeartRate2.Location = new System.Drawing.Point(6, 163);
            this.lbl_avgHeartRate2.Name = "lbl_avgHeartRate2";
            this.lbl_avgHeartRate2.Size = new System.Drawing.Size(114, 15);
            this.lbl_avgHeartRate2.TabIndex = 14;
            this.lbl_avgHeartRate2.Text = "Average Heart Rate :";
            // 
            // lbl_maxSpeed2
            // 
            this.lbl_maxSpeed2.AutoSize = true;
            this.lbl_maxSpeed2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxSpeed2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxSpeed2.Location = new System.Drawing.Point(6, 125);
            this.lbl_maxSpeed2.Name = "lbl_maxSpeed2";
            this.lbl_maxSpeed2.Size = new System.Drawing.Size(102, 15);
            this.lbl_maxSpeed2.TabIndex = 13;
            this.lbl_maxSpeed2.Text = "Maximum Speed :";
            // 
            // lbl_avgSpeed2
            // 
            this.lbl_avgSpeed2.AutoSize = true;
            this.lbl_avgSpeed2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgSpeed2.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgSpeed2.Location = new System.Drawing.Point(6, 87);
            this.lbl_avgSpeed2.Name = "lbl_avgSpeed2";
            this.lbl_avgSpeed2.Size = new System.Drawing.Size(91, 15);
            this.lbl_avgSpeed2.TabIndex = 12;
            this.lbl_avgSpeed2.Text = "Average Speed :";
            // 
            // File2HeartRate
            // 
            this.File2HeartRate.HeaderText = "Heart Rate";
            this.File2HeartRate.Name = "File2HeartRate";
            this.File2HeartRate.ReadOnly = true;
            // 
            // File2Speed
            // 
            this.File2Speed.HeaderText = "Speed";
            this.File2Speed.Name = "File2Speed";
            this.File2Speed.ReadOnly = true;
            // 
            // File2Cadence
            // 
            this.File2Cadence.HeaderText = "Cadence";
            this.File2Cadence.Name = "File2Cadence";
            this.File2Cadence.ReadOnly = true;
            // 
            // File2Altitude
            // 
            this.File2Altitude.HeaderText = "Altitude";
            this.File2Altitude.Name = "File2Altitude";
            this.File2Altitude.ReadOnly = true;
            // 
            // File2Power
            // 
            this.File2Power.HeaderText = "Power";
            this.File2Power.Name = "File2Power";
            this.File2Power.ReadOnly = true;
            // 
            // File2PowerIndex
            // 
            this.File2PowerIndex.HeaderText = "Power Index";
            this.File2PowerIndex.Name = "File2PowerIndex";
            this.File2PowerIndex.ReadOnly = true;
            // 
            // File2TimeStamp
            // 
            this.File2TimeStamp.HeaderText = "Time Stamp";
            this.File2TimeStamp.Name = "File2TimeStamp";
            this.File2TimeStamp.ReadOnly = true;
            // 
            // FileComparison
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 628);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Name = "FileComparison";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FileComparison";
            this.Load += new System.EventHandler(this.FileComparison_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox_file1Summary.ResumeLayout(false);
            this.groupBox_file1Summary.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox_file2Summary.ResumeLayout(false);
            this.groupBox_file2Summary.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeStamp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.GroupBox groupBox_file1Summary;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblmaxAltitudePM;
        private System.Windows.Forms.Label lblavgAltitudePM;
        private System.Windows.Forms.Label lblmaxPowerPM;
        private System.Windows.Forms.Label lblavgPowerPM;
        private System.Windows.Forms.Label lblminHeartRatePM;
        private System.Windows.Forms.Label lblmaxHeartRatePM;
        private System.Windows.Forms.Label lblavgHeartRatePM;
        private System.Windows.Forms.Label lblmaxSpeedPM;
        private System.Windows.Forms.Label lblavgSpeedPM;
        private System.Windows.Forms.Label lbltotalDistancePM;
        private System.Windows.Forms.Label lbltotalDistance;
        private System.Windows.Forms.Label lbl_totalDistance;
        private System.Windows.Forms.Label lblmaxAltitude;
        private System.Windows.Forms.Label lblavgAltitude;
        private System.Windows.Forms.Label lblmaxPower;
        private System.Windows.Forms.Label lblavgPower;
        private System.Windows.Forms.Label lblminHeartRate;
        private System.Windows.Forms.Label lblmaxHeartRate;
        private System.Windows.Forms.Label lblavgHeartRate;
        private System.Windows.Forms.Label lblmaxSpeed;
        private System.Windows.Forms.Label lblavgSpeed;
        private System.Windows.Forms.Label lbl_maxAltitude;
        private System.Windows.Forms.Label lbl_avgAltitude;
        private System.Windows.Forms.Label lbl_maxPower;
        private System.Windows.Forms.Label lbl_avgPower;
        private System.Windows.Forms.Label lbl_minHeartRate;
        private System.Windows.Forms.Label lbl_maxHeartRate;
        private System.Windows.Forms.Label lbl_avgHeartRate;
        private System.Windows.Forms.Label lbl_maxSpeed;
        private System.Windows.Forms.Label lbl_avgSpeed;
        private System.Windows.Forms.GroupBox groupBox_file2Summary;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblmaxAltitude2PM;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblavgAltitude2PM;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblmaxPower2PM;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblavgPower2PM;
        private System.Windows.Forms.Label lblminHeartRate2PM;
        private System.Windows.Forms.Label lblmaxHeartRate2PM;
        private System.Windows.Forms.Label lbltotalDistance2;
        private System.Windows.Forms.Label lblavgHeartRate2PM;
        private System.Windows.Forms.Label lbl_totalDistance2;
        private System.Windows.Forms.Label lblmaxSpeed2PM;
        private System.Windows.Forms.Label lblmaxAltitude2;
        private System.Windows.Forms.Label lblavgSpeed2PM;
        private System.Windows.Forms.Label lblavgAltitude2;
        private System.Windows.Forms.Label lbltotalDistance2PM;
        private System.Windows.Forms.Label lblmaxPower2;
        private System.Windows.Forms.Label lblavgPower2;
        private System.Windows.Forms.Label lblminHeartRate2;
        private System.Windows.Forms.Label lblmaxHeartRate2;
        private System.Windows.Forms.Label lblavgHeartRate2;
        private System.Windows.Forms.Label lblmaxSpeed2;
        private System.Windows.Forms.Label lblavgSpeed2;
        private System.Windows.Forms.Label lbl_maxAltitude2;
        private System.Windows.Forms.Label lbl_avgAltitude2;
        private System.Windows.Forms.Label lbl_maxPower2;
        private System.Windows.Forms.Label lbl_avgPower2;
        private System.Windows.Forms.Label lbl_minHeartRate2;
        private System.Windows.Forms.Label lbl_maxHeartRate2;
        private System.Windows.Forms.Label lbl_avgHeartRate2;
        private System.Windows.Forms.Label lbl_maxSpeed2;
        private System.Windows.Forms.Label lbl_avgSpeed2;
        private System.Windows.Forms.DataGridViewTextBoxColumn File2HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn File2Speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn File2Cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn File2Altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn File2Power;
        private System.Windows.Forms.DataGridViewTextBoxColumn File2PowerIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn File2TimeStamp;
    }
}