﻿namespace DataAnalysisSoftware
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.btnFileComparison = new System.Windows.Forms.Button();
            this.btnIntervalDetection = new System.Windows.Forms.Button();
            this.btnAdvancedMetrics = new System.Windows.Forms.Button();
            this.btnViewGraph = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.gbParameterInfo = new System.Windows.Forms.GroupBox();
            this.label55 = new System.Windows.Forms.Label();
            this.lblMonitor = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblSMode = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.lblDeviceVersion = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblMonitorType = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblsmpowerbalance = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblsmPower = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblsmAltd = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblsmCad = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblsmSpeed = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeStamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDataSummary = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioKM = new System.Windows.Forms.RadioButton();
            this.radioMiles = new System.Windows.Forms.RadioButton();
            this.lblTotalDistance = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMaxAltitude = new System.Windows.Forms.Label();
            this.lblAvgAltitude = new System.Windows.Forms.Label();
            this.lblMaxPower = new System.Windows.Forms.Label();
            this.lblAvgPower = new System.Windows.Forms.Label();
            this.lblMinHeartRate = new System.Windows.Forms.Label();
            this.lblMaxHeartRate = new System.Windows.Forms.Label();
            this.lblAvgHeartRate = new System.Windows.Forms.Label();
            this.lblMaxSpeed = new System.Windows.Forms.Label();
            this.lblAvgSpeed = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSelectableInformation = new System.Windows.Forms.Button();
            this.pnlHeader.SuspendLayout();
            this.gbParameterInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.pnlDataSummary.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHeader.Controls.Add(this.btnSelectableInformation);
            this.pnlHeader.Controls.Add(this.btnFileComparison);
            this.pnlHeader.Controls.Add(this.btnIntervalDetection);
            this.pnlHeader.Controls.Add(this.btnAdvancedMetrics);
            this.pnlHeader.Controls.Add(this.btnViewGraph);
            this.pnlHeader.Controls.Add(this.label34);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1274, 61);
            this.pnlHeader.TabIndex = 2;
            // 
            // btnFileComparison
            // 
            this.btnFileComparison.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileComparison.Location = new System.Drawing.Point(973, 8);
            this.btnFileComparison.Name = "btnFileComparison";
            this.btnFileComparison.Size = new System.Drawing.Size(135, 43);
            this.btnFileComparison.TabIndex = 6;
            this.btnFileComparison.Text = "File Comparison";
            this.btnFileComparison.UseVisualStyleBackColor = true;
            this.btnFileComparison.Click += new System.EventHandler(this.btnFileComparison_Click);
            // 
            // btnIntervalDetection
            // 
            this.btnIntervalDetection.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIntervalDetection.Location = new System.Drawing.Point(1114, 8);
            this.btnIntervalDetection.Name = "btnIntervalDetection";
            this.btnIntervalDetection.Size = new System.Drawing.Size(135, 43);
            this.btnIntervalDetection.TabIndex = 5;
            this.btnIntervalDetection.Text = "Interval Detection";
            this.btnIntervalDetection.UseVisualStyleBackColor = true;
            this.btnIntervalDetection.Click += new System.EventHandler(this.btnIntervalDetection_Click);
            // 
            // btnAdvancedMetrics
            // 
            this.btnAdvancedMetrics.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdvancedMetrics.Location = new System.Drawing.Point(660, 8);
            this.btnAdvancedMetrics.Name = "btnAdvancedMetrics";
            this.btnAdvancedMetrics.Size = new System.Drawing.Size(135, 43);
            this.btnAdvancedMetrics.TabIndex = 4;
            this.btnAdvancedMetrics.Text = "Advanced Metrics";
            this.btnAdvancedMetrics.UseVisualStyleBackColor = true;
            this.btnAdvancedMetrics.Click += new System.EventHandler(this.btnAdvancedMetrics_Click);
            // 
            // btnViewGraph
            // 
            this.btnViewGraph.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewGraph.Location = new System.Drawing.Point(519, 8);
            this.btnViewGraph.Name = "btnViewGraph";
            this.btnViewGraph.Size = new System.Drawing.Size(135, 43);
            this.btnViewGraph.TabIndex = 3;
            this.btnViewGraph.Text = "View Graph";
            this.btnViewGraph.UseVisualStyleBackColor = true;
            this.btnViewGraph.Click += new System.EventHandler(this.btnViewGraph_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Cambria", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(109, 16);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(259, 22);
            this.label34.TabIndex = 1;
            this.label34.Text = "POLAR HRM DATA SUMMARY";
            // 
            // gbParameterInfo
            // 
            this.gbParameterInfo.Controls.Add(this.label55);
            this.gbParameterInfo.Controls.Add(this.lblMonitor);
            this.gbParameterInfo.Controls.Add(this.label33);
            this.gbParameterInfo.Controls.Add(this.lblInterval);
            this.gbParameterInfo.Controls.Add(this.label24);
            this.gbParameterInfo.Controls.Add(this.lblStartTime);
            this.gbParameterInfo.Controls.Add(this.label25);
            this.gbParameterInfo.Controls.Add(this.lblDate);
            this.gbParameterInfo.Controls.Add(this.label26);
            this.gbParameterInfo.Controls.Add(this.lblSMode);
            this.gbParameterInfo.Controls.Add(this.label27);
            this.gbParameterInfo.Controls.Add(this.lblLength);
            this.gbParameterInfo.Controls.Add(this.lblDeviceVersion);
            this.gbParameterInfo.Controls.Add(this.label28);
            this.gbParameterInfo.Controls.Add(this.lblMonitorType);
            this.gbParameterInfo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbParameterInfo.Location = new System.Drawing.Point(0, 0);
            this.gbParameterInfo.Name = "gbParameterInfo";
            this.gbParameterInfo.Size = new System.Drawing.Size(307, 295);
            this.gbParameterInfo.TabIndex = 0;
            this.gbParameterInfo.TabStop = false;
            this.gbParameterInfo.Text = "GENERAL PARAMETERS";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(22, 247);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(66, 20);
            this.label55.TabIndex = 34;
            this.label55.Text = "Monitor:";
            // 
            // lblMonitor
            // 
            this.lblMonitor.AutoSize = true;
            this.lblMonitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonitor.Location = new System.Drawing.Point(174, 247);
            this.lblMonitor.Name = "lblMonitor";
            this.lblMonitor.Size = new System.Drawing.Size(18, 20);
            this.lblMonitor.TabIndex = 31;
            this.lblMonitor.Text = "0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(22, 215);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(63, 20);
            this.label33.TabIndex = 10;
            this.label33.Text = "Length:";
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInterval.Location = new System.Drawing.Point(174, 180);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(18, 20);
            this.lblInterval.TabIndex = 1;
            this.lblInterval.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(22, 180);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 20);
            this.label24.TabIndex = 1;
            this.label24.Text = "Interval:";
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartTime.Location = new System.Drawing.Point(174, 150);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(18, 20);
            this.lblStartTime.TabIndex = 2;
            this.lblStartTime.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(22, 150);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 20);
            this.label25.TabIndex = 2;
            this.label25.Text = "Start Time:";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(174, 121);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(18, 20);
            this.lblDate.TabIndex = 3;
            this.lblDate.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(22, 121);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(48, 20);
            this.label26.TabIndex = 3;
            this.label26.Text = "Date:";
            // 
            // lblSMode
            // 
            this.lblSMode.AutoSize = true;
            this.lblSMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSMode.Location = new System.Drawing.Point(174, 91);
            this.lblSMode.Name = "lblSMode";
            this.lblSMode.Size = new System.Drawing.Size(18, 20);
            this.lblSMode.TabIndex = 4;
            this.lblSMode.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(22, 91);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 20);
            this.label27.TabIndex = 4;
            this.label27.Text = "SMode:";
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLength.Location = new System.Drawing.Point(174, 215);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(18, 20);
            this.lblLength.TabIndex = 0;
            this.lblLength.Text = "0";
            // 
            // lblDeviceVersion
            // 
            this.lblDeviceVersion.AutoSize = true;
            this.lblDeviceVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeviceVersion.Location = new System.Drawing.Point(174, 60);
            this.lblDeviceVersion.Name = "lblDeviceVersion";
            this.lblDeviceVersion.Size = new System.Drawing.Size(18, 20);
            this.lblDeviceVersion.TabIndex = 5;
            this.lblDeviceVersion.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(22, 60);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(119, 20);
            this.label28.TabIndex = 5;
            this.label28.Text = "Device Version:";
            // 
            // lblMonitorType
            // 
            this.lblMonitorType.AutoSize = true;
            this.lblMonitorType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonitorType.Location = new System.Drawing.Point(44, 23);
            this.lblMonitorType.Name = "lblMonitorType";
            this.lblMonitorType.Size = new System.Drawing.Size(234, 20);
            this.lblMonitorType.TabIndex = 0;
            this.lblMonitorType.Text = "HEART RATE MONITOR TYPE";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblsmpowerbalance);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.lblsmPower);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblsmAltd);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lblsmCad);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblsmSpeed);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 301);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(304, 243);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SMODE";
            // 
            // lblsmpowerbalance
            // 
            this.lblsmpowerbalance.AutoSize = true;
            this.lblsmpowerbalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsmpowerbalance.Location = new System.Drawing.Point(165, 179);
            this.lblsmpowerbalance.Name = "lblsmpowerbalance";
            this.lblsmpowerbalance.Size = new System.Drawing.Size(18, 20);
            this.lblsmpowerbalance.TabIndex = 16;
            this.lblsmpowerbalance.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 179);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 20);
            this.label12.TabIndex = 17;
            this.label12.Text = "Power Balance";
            // 
            // lblsmPower
            // 
            this.lblsmPower.AutoSize = true;
            this.lblsmPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsmPower.Location = new System.Drawing.Point(165, 140);
            this.lblsmPower.Name = "lblsmPower";
            this.lblsmPower.Size = new System.Drawing.Size(18, 20);
            this.lblsmPower.TabIndex = 8;
            this.lblsmPower.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Power";
            // 
            // lblsmAltd
            // 
            this.lblsmAltd.AutoSize = true;
            this.lblsmAltd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsmAltd.Location = new System.Drawing.Point(164, 104);
            this.lblsmAltd.Name = "lblsmAltd";
            this.lblsmAltd.Size = new System.Drawing.Size(18, 20);
            this.lblsmAltd.TabIndex = 10;
            this.lblsmAltd.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Altitude";
            // 
            // lblsmCad
            // 
            this.lblsmCad.AutoSize = true;
            this.lblsmCad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsmCad.Location = new System.Drawing.Point(164, 69);
            this.lblsmCad.Name = "lblsmCad";
            this.lblsmCad.Size = new System.Drawing.Size(18, 20);
            this.lblsmCad.TabIndex = 12;
            this.lblsmCad.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(19, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "Cadence";
            // 
            // lblsmSpeed
            // 
            this.lblsmSpeed.AutoSize = true;
            this.lblsmSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsmSpeed.Location = new System.Drawing.Point(164, 39);
            this.lblsmSpeed.Name = "lblsmSpeed";
            this.lblsmSpeed.Size = new System.Drawing.Size(18, 20);
            this.lblsmSpeed.TabIndex = 14;
            this.lblsmSpeed.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(19, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 20);
            this.label10.TabIndex = 15;
            this.label10.Text = "Speed";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HeartRate,
            this.Speed,
            this.Cadence,
            this.Altitude,
            this.Power,
            this.PowerIndex,
            this.TimeStamp});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(683, 547);
            this.dataGridView1.TabIndex = 5;
            // 
            // HeartRate
            // 
            this.HeartRate.HeaderText = "Heart Rate";
            this.HeartRate.Name = "HeartRate";
            this.HeartRate.ReadOnly = true;
            // 
            // Speed
            // 
            this.Speed.HeaderText = "Speed";
            this.Speed.Name = "Speed";
            this.Speed.ReadOnly = true;
            // 
            // Cadence
            // 
            this.Cadence.HeaderText = "Cadence";
            this.Cadence.Name = "Cadence";
            this.Cadence.ReadOnly = true;
            // 
            // Altitude
            // 
            this.Altitude.HeaderText = "Altitude";
            this.Altitude.Name = "Altitude";
            this.Altitude.ReadOnly = true;
            // 
            // Power
            // 
            this.Power.HeaderText = "Power";
            this.Power.Name = "Power";
            this.Power.ReadOnly = true;
            // 
            // PowerIndex
            // 
            this.PowerIndex.HeaderText = "Power Index";
            this.PowerIndex.Name = "PowerIndex";
            this.PowerIndex.ReadOnly = true;
            // 
            // TimeStamp
            // 
            this.TimeStamp.HeaderText = "Time Stamp";
            this.TimeStamp.Name = "TimeStamp";
            this.TimeStamp.ReadOnly = true;
            // 
            // pnlDataSummary
            // 
            this.pnlDataSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDataSummary.Controls.Add(this.panel2);
            this.pnlDataSummary.Controls.Add(this.panel1);
            this.pnlDataSummary.Controls.Add(this.groupBox1);
            this.pnlDataSummary.Controls.Add(this.gbParameterInfo);
            this.pnlDataSummary.Location = new System.Drawing.Point(0, 85);
            this.pnlDataSummary.Name = "pnlDataSummary";
            this.pnlDataSummary.Size = new System.Drawing.Size(1274, 549);
            this.pnlDataSummary.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.lblTotalDistance);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lblMaxAltitude);
            this.panel2.Controls.Add(this.lblAvgAltitude);
            this.panel2.Controls.Add(this.lblMaxPower);
            this.panel2.Controls.Add(this.lblAvgPower);
            this.panel2.Controls.Add(this.lblMinHeartRate);
            this.panel2.Controls.Add(this.lblMaxHeartRate);
            this.panel2.Controls.Add(this.lblAvgHeartRate);
            this.panel2.Controls.Add(this.lblMaxSpeed);
            this.panel2.Controls.Add(this.lblAvgSpeed);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1002, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(270, 547);
            this.panel2.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(11, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(125, 23);
            this.label17.TabIndex = 81;
            this.label17.Text = "Summary Data";
            this.label17.UseWaitCursor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.radioKM);
            this.panel3.Controls.Add(this.radioMiles);
            this.panel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(18, 40);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(227, 43);
            this.panel3.TabIndex = 80;
            // 
            // radioKM
            // 
            this.radioKM.AutoSize = true;
            this.radioKM.Checked = true;
            this.radioKM.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioKM.Location = new System.Drawing.Point(6, 9);
            this.radioKM.Name = "radioKM";
            this.radioKM.Size = new System.Drawing.Size(100, 23);
            this.radioKM.TabIndex = 1;
            this.radioKM.TabStop = true;
            this.radioKM.Text = "Kilometers";
            this.radioKM.UseVisualStyleBackColor = true;
            this.radioKM.CheckedChanged += new System.EventHandler(this.radioKM_CheckedChanged);
            // 
            // radioMiles
            // 
            this.radioMiles.AutoSize = true;
            this.radioMiles.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioMiles.Location = new System.Drawing.Point(140, 9);
            this.radioMiles.Name = "radioMiles";
            this.radioMiles.Size = new System.Drawing.Size(63, 23);
            this.radioMiles.TabIndex = 0;
            this.radioMiles.Text = "Miles";
            this.radioMiles.UseVisualStyleBackColor = true;
            this.radioMiles.CheckedChanged += new System.EventHandler(this.radioMiles_CheckedChanged);
            // 
            // lblTotalDistance
            // 
            this.lblTotalDistance.AutoSize = true;
            this.lblTotalDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDistance.Location = new System.Drawing.Point(178, 108);
            this.lblTotalDistance.Name = "lblTotalDistance";
            this.lblTotalDistance.Size = new System.Drawing.Size(32, 16);
            this.lblTotalDistance.TabIndex = 79;
            this.lblTotalDistance.Text = "0.00";
            this.lblTotalDistance.UseWaitCursor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 16);
            this.label1.TabIndex = 78;
            this.label1.Text = "Total Distance Covered";
            this.label1.UseWaitCursor = true;
            // 
            // lblMaxAltitude
            // 
            this.lblMaxAltitude.AutoSize = true;
            this.lblMaxAltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxAltitude.Location = new System.Drawing.Point(178, 410);
            this.lblMaxAltitude.Name = "lblMaxAltitude";
            this.lblMaxAltitude.Size = new System.Drawing.Size(32, 16);
            this.lblMaxAltitude.TabIndex = 77;
            this.lblMaxAltitude.Text = "0.00";
            this.lblMaxAltitude.UseWaitCursor = true;
            // 
            // lblAvgAltitude
            // 
            this.lblAvgAltitude.AutoSize = true;
            this.lblAvgAltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgAltitude.Location = new System.Drawing.Point(178, 379);
            this.lblAvgAltitude.Name = "lblAvgAltitude";
            this.lblAvgAltitude.Size = new System.Drawing.Size(32, 16);
            this.lblAvgAltitude.TabIndex = 76;
            this.lblAvgAltitude.Text = "0.00";
            this.lblAvgAltitude.UseWaitCursor = true;
            // 
            // lblMaxPower
            // 
            this.lblMaxPower.AutoSize = true;
            this.lblMaxPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxPower.Location = new System.Drawing.Point(178, 345);
            this.lblMaxPower.Name = "lblMaxPower";
            this.lblMaxPower.Size = new System.Drawing.Size(32, 16);
            this.lblMaxPower.TabIndex = 75;
            this.lblMaxPower.Text = "0.00";
            this.lblMaxPower.UseWaitCursor = true;
            // 
            // lblAvgPower
            // 
            this.lblAvgPower.AutoSize = true;
            this.lblAvgPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgPower.Location = new System.Drawing.Point(178, 311);
            this.lblAvgPower.Name = "lblAvgPower";
            this.lblAvgPower.Size = new System.Drawing.Size(32, 16);
            this.lblAvgPower.TabIndex = 74;
            this.lblAvgPower.Text = "0.00";
            this.lblAvgPower.UseWaitCursor = true;
            // 
            // lblMinHeartRate
            // 
            this.lblMinHeartRate.AutoSize = true;
            this.lblMinHeartRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinHeartRate.Location = new System.Drawing.Point(178, 277);
            this.lblMinHeartRate.Name = "lblMinHeartRate";
            this.lblMinHeartRate.Size = new System.Drawing.Size(32, 16);
            this.lblMinHeartRate.TabIndex = 73;
            this.lblMinHeartRate.Text = "0.00";
            this.lblMinHeartRate.UseWaitCursor = true;
            // 
            // lblMaxHeartRate
            // 
            this.lblMaxHeartRate.AutoSize = true;
            this.lblMaxHeartRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxHeartRate.Location = new System.Drawing.Point(178, 244);
            this.lblMaxHeartRate.Name = "lblMaxHeartRate";
            this.lblMaxHeartRate.Size = new System.Drawing.Size(32, 16);
            this.lblMaxHeartRate.TabIndex = 72;
            this.lblMaxHeartRate.Text = "0.00";
            this.lblMaxHeartRate.UseWaitCursor = true;
            // 
            // lblAvgHeartRate
            // 
            this.lblAvgHeartRate.AutoSize = true;
            this.lblAvgHeartRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgHeartRate.Location = new System.Drawing.Point(178, 208);
            this.lblAvgHeartRate.Name = "lblAvgHeartRate";
            this.lblAvgHeartRate.Size = new System.Drawing.Size(32, 16);
            this.lblAvgHeartRate.TabIndex = 71;
            this.lblAvgHeartRate.Text = "0.00";
            this.lblAvgHeartRate.UseWaitCursor = true;
            // 
            // lblMaxSpeed
            // 
            this.lblMaxSpeed.AutoSize = true;
            this.lblMaxSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxSpeed.Location = new System.Drawing.Point(178, 175);
            this.lblMaxSpeed.Name = "lblMaxSpeed";
            this.lblMaxSpeed.Size = new System.Drawing.Size(32, 16);
            this.lblMaxSpeed.TabIndex = 70;
            this.lblMaxSpeed.Text = "0.00";
            this.lblMaxSpeed.UseWaitCursor = true;
            // 
            // lblAvgSpeed
            // 
            this.lblAvgSpeed.AutoSize = true;
            this.lblAvgSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgSpeed.Location = new System.Drawing.Point(178, 142);
            this.lblAvgSpeed.Name = "lblAvgSpeed";
            this.lblAvgSpeed.Size = new System.Drawing.Size(32, 16);
            this.lblAvgSpeed.TabIndex = 69;
            this.lblAvgSpeed.Text = "0.00";
            this.lblAvgSpeed.UseWaitCursor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 410);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 16);
            this.label11.TabIndex = 68;
            this.label11.Text = "Maximum Altitude";
            this.label11.UseWaitCursor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 379);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 16);
            this.label3.TabIndex = 67;
            this.label3.Text = "Average Ailtitude";
            this.label3.UseWaitCursor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 345);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 16);
            this.label9.TabIndex = 66;
            this.label9.Text = "Maximum Power";
            this.label9.UseWaitCursor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 311);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 16);
            this.label5.TabIndex = 65;
            this.label5.Text = "Average Power";
            this.label5.UseWaitCursor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 277);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 16);
            this.label7.TabIndex = 64;
            this.label7.Text = "Minimum Heart Rate";
            this.label7.UseWaitCursor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(16, 244);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 16);
            this.label13.TabIndex = 63;
            this.label13.Text = "Maximum Heart Rate";
            this.label13.UseWaitCursor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(16, 208);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 16);
            this.label14.TabIndex = 62;
            this.label14.Text = "Average Heart Rate";
            this.label14.UseWaitCursor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(16, 175);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 16);
            this.label15.TabIndex = 61;
            this.label15.Text = "Maximum Speed";
            this.label15.UseWaitCursor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(16, 142);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 16);
            this.label16.TabIndex = 60;
            this.label16.Text = "Average Speed";
            this.label16.UseWaitCursor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(313, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(683, 547);
            this.panel1.TabIndex = 7;
            // 
            // btnSelectableInformation
            // 
            this.btnSelectableInformation.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectableInformation.Location = new System.Drawing.Point(801, 8);
            this.btnSelectableInformation.Name = "btnSelectableInformation";
            this.btnSelectableInformation.Size = new System.Drawing.Size(166, 43);
            this.btnSelectableInformation.TabIndex = 7;
            this.btnSelectableInformation.Text = "Selectable Information";
            this.btnSelectableInformation.UseVisualStyleBackColor = true;
            this.btnSelectableInformation.Click += new System.EventHandler(this.btnSelectableInformation_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(1274, 635);
            this.Controls.Add(this.pnlDataSummary);
            this.Controls.Add(this.pnlHeader);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.gbParameterInfo.ResumeLayout(false);
            this.gbParameterInfo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.pnlDataSummary.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btnViewGraph;
        private System.Windows.Forms.GroupBox gbParameterInfo;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label lblMonitor;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lblInterval;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblStartTime;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblSMode;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.Label lblDeviceVersion;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblMonitorType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblsmpowerbalance;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblsmPower;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblsmAltd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblsmCad;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblsmSpeed;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel pnlDataSummary;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTotalDistance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMaxAltitude;
        private System.Windows.Forms.Label lblAvgAltitude;
        private System.Windows.Forms.Label lblMaxPower;
        private System.Windows.Forms.Label lblAvgPower;
        private System.Windows.Forms.Label lblMinHeartRate;
        private System.Windows.Forms.Label lblMaxHeartRate;
        private System.Windows.Forms.Label lblAvgHeartRate;
        private System.Windows.Forms.Label lblMaxSpeed;
        private System.Windows.Forms.Label lblAvgSpeed;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioKM;
        private System.Windows.Forms.RadioButton radioMiles;
        private System.Windows.Forms.Button btnAdvancedMetrics;
        private System.Windows.Forms.Button btnIntervalDetection;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeStamp;
        private System.Windows.Forms.Button btnFileComparison;
        private System.Windows.Forms.Button btnSelectableInformation;
    }
}

