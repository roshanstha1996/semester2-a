﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace DataAnalysisSoftware
{
    /// <summary>
    /// class of selectable information
    /// </summary>
    public partial class SelectableInformation : Form
    {
        //variable declared
        int[] arrayHR;
        int[] arraySpeed;
        int[] arrayCadence;
        int[] arrayAltitude;
        int[] arrayPower;
        int[] arrayPowerBalance;
        string[] arrayTime;
        int totalItems;
        double avgSpeed;
        double avgPower;
        double totalSeconds;

        //static variable declaration
        //can be used on other class without creating object of base class
        public static int[] chunk1ArrayHR = new int[2000];
        public static int[] chunk2ArrayHR = new int[2000];
        public static int[] chunk3ArrayHR = new int[2000];
        public static int[] chunk4ArrayHR = new int[2000];
        public static int[] chunk1ArraySpeed = new int[2000];
        public static int[] chunk2ArraySpeed = new int[2000];
        public static int[] chunk3ArraySpeed = new int[2000];
        public static int[] chunk4ArraySpeed = new int[2000];
        public static int[] chunk1ArrayCadence = new int[2000];
        public static int[] chunk2ArrayCadence = new int[2000];
        public static int[] chunk3ArrayCadence = new int[2000];
        public static int[] chunk4ArrayCadence = new int[2000];
        public static int[] chunk1ArrayAltitude = new int[2000];
        public static int[] chunk2ArrayAltitude = new int[2000];
        public static int[] chunk3ArrayAltitude = new int[2000];
        public static int[] chunk4ArrayAltitude = new int[2000];
        public static int[] chunk1ArrayPower = new int[2000];
        public static int[] chunk2ArrayPower = new int[2000];
        public static int[] chunk3ArrayPower = new int[2000];
        public static int[] chunk4ArrayPower = new int[2000];


        /// <summary>
        /// selectable information class constructor with parameters
        /// </summary>
        /// <param name="arrayHR"></param>
        /// <param name="arraySpeed"></param>
        /// <param name="arrayCadence"></param>
        /// <param name="arrayAltitude"></param>
        /// <param name="arrayPower"></param>
        /// <param name="arrayPowerBalance"></param>
        /// <param name="arrayTime"></param>
        /// <param name="totalSeconds"></param>
        public SelectableInformation(int[] arrayHR, int[] arraySpeed, int[] arrayCadence,
            int[] arrayAltitude, int[] arrayPower, int[] arrayPowerBalance, string[] arrayTime, double totalSeconds)
        {
            this.arrayHR = arrayHR;
            this.arraySpeed = arraySpeed;
            this.arrayCadence = arrayCadence;
            this.arrayAltitude = arrayAltitude;
            this.arrayPower = arrayPower;
            this.arrayPowerBalance = arrayPowerBalance;
            this.arrayTime = arrayTime;
            this.totalSeconds = totalSeconds;
            InitializeComponent();
            loadDataGridView();
        }

        /// <summary>
        /// loading data into grid view
        /// </summary>
        public void loadDataGridView()
        {
            int data = 0;
            int totalData = arrayHR.Length;
            int row = 0;
            int heartRate = 0;
            int speed = 1;
            int cadence = 2;
            int altitude = 3;
            int power = 4;
            int powerBalance = 5;
            int time = 6;
            while (data < totalData)
            {
                dataGridView.Rows.Add();
                for (int i = 0; i < 6; i++)
                {
                    dataGridView[heartRate, row].Value = arrayHR[row];
                    dataGridView[speed, row].Value = arraySpeed[row];
                    dataGridView[cadence, row].Value = arrayCadence[row];
                    dataGridView[altitude, row].Value = arrayAltitude[row];
                    dataGridView[power, row].Value = arrayPower[row];
                    dataGridView[powerBalance, row].Value = arrayPowerBalance[row];
                    dataGridView[time, row].Value = arrayTime[row];
                }
                data++;
                row++;
            }
        }

        /// <summary>
        /// event for form load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectableInformation_Load(object sender, EventArgs e)
        {
            dataGridView.CurrentCell.Selected = false;
        }

        /// <summary>
        /// event for row selection on data grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            lblpowerBalance.Text = "53";
            if (dataGridView.SelectedRows.Count > 1)
            {
                btnChunkSelectedData.Enabled = true;
            }
            else
            {
                btnChunkSelectedData.Enabled = false;
            }
            summarydata_load();
            //methods calling
            advancedMetricsCalculation();
            drawGraph();
        }

        /// <summary>
        /// method to load summary data
        /// </summary>
        public void summarydata_load()
        {
            List<int> hr = new List<int>();
            List<int> speed = new List<int>();
            List<int> cadence = new List<int>();
            List<int> altitude = new List<int>();
            List<int> power = new List<int>();
            foreach (DataGridViewRow row in dataGridView.SelectedRows)
            {
                //adding data of every column to its repective list  
                hr.Add(int.Parse(row.Cells["HeartRate"].Value.ToString()));
                speed.Add(int.Parse(row.Cells["Speed"].Value.ToString()));
                cadence.Add(int.Parse(row.Cells["Cadence"].Value.ToString()));
                altitude.Add(int.Parse(row.Cells["Altitude"].Value.ToString()));
                power.Add(int.Parse(row.Cells["Power"].Value.ToString()));
            }

            //now converting list to array
            arrayHR = hr.ToArray();
            arraySpeed = speed.ToArray();
            arrayCadence = cadence.ToArray();
            arrayAltitude = altitude.ToArray();
            arrayPower = power.ToArray();

            //methods called to calculate summary data
            calcSpeed();
            calcHR();
            calcPower();
            calcAltitude();
            calcTotalDistance();
        }

        /// <summary>
        ///method to calculate speed data
        /// </summary>
        public void calcSpeed()
        {
            totalItems = arraySpeed.Length;
            int totalSpeed = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total speed
                totalSpeed = totalSpeed + arraySpeed[i];
            }
            //average speed
            avgSpeed = Math.Round((double)totalSpeed / totalItems, 2);
            lblavgSpeed.Text = avgSpeed.ToString() + " " + "km/h";
            //maximum speed
            int maxSpeed = arraySpeed.Max();
            lblmaxSpeed.Text = maxSpeed.ToString() + " " + "km/h";
        }

        /// <summary>
        /// method to calculate heart rate data
        /// </summary>
        public void calcHR()
        {
            totalItems = arrayHR.Length;
            int totalHeartRate = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total heart rate
                totalHeartRate = totalHeartRate + arrayHR[i];
            }
            //average heart rate
            int avgHR = totalHeartRate / totalItems;
            lblavgHeartRate.Text = avgHR.ToString() + " " + "bpm";
            //maximum heart rate
            int maxHR = arrayHR.Max();
            lblmaxHeartRate.Text = maxHR.ToString() + " " + "bpm";
            //minimum heart rate
            int minHR = arrayHR.Min();
            lblminHeartRate.Text = minHR.ToString() + " " + "bpm";
        }

        /// <summary>
        /// method to calculate power data
        /// </summary>
        public void calcPower()
        {
            totalItems = arrayPower.Length;
            int totalPower = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total power
                totalPower = totalPower + arrayPower[i];
            }
            //average power
            avgPower = totalPower / totalItems;
            lblavgPower.Text = avgPower.ToString() + " " + "watts";
            //maximum power
            int maxPower = arrayPower.Max();
            lblmaxPower.Text = maxPower.ToString() + " " + "watts";
        }

        /// <summary>
        /// method to calculate  altitude data
        /// </summary>
        public void calcAltitude()
        {
            totalItems = arrayAltitude.Length;
            int totalAltitude = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total altitude
                totalAltitude = totalAltitude + arrayAltitude[i];
            }
            //average altitude
            double avgAltitude = Math.Round((double)totalAltitude / totalItems, 2);
            lblavgAltitude.Text = avgAltitude.ToString() + " " + "m/ft";
            //maximum altitude
            int maxAltitude = arrayAltitude.Max();
            lblmaxAltitude.Text = maxAltitude.ToString() + " " + "m/ft";
        }


        /// <summary>
        /// method to calculate distance covered
        /// </summary>
        public void calcTotalDistance()
        {
            //calculating distance covered
            double totalDistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalSeconds), 2);
            lbltotalDistance.Text = totalDistance.ToString() + " " + "km/h";
        }

        /// <summary>
        /// methods to calculate advanced metrics
        /// </summary>
        public void advancedMetricsCalculation()
        {
            //calculate functional threshold power
            double funcThreshPower = avgPower * 0.95;

            //calculating normalized power
            double func = Math.Pow(avgPower, 4.0);
            double min = func * 66.0;
            double time = min / 60.0;
            double normalisedPower = Math.Round(Math.Pow(time, (1.0 / 4)), 2);
            lblnormalisedPower.Text = normalisedPower.ToString() + " " + "watts";

            //calculate intensity factor
            double intensFactor = Math.Round(normalisedPower / funcThreshPower, 2);
            lblintensityFactor.Text = intensFactor.ToString();

            //calculate training stress score
            double trainstressScore = Math.Round(((totalSeconds * normalisedPower * intensFactor) /
                (funcThreshPower * 3600)) * 100, 2);
            lbltrainingStressScore.Text = trainstressScore.ToString();
        }

        /// <summary>
        /// method to draw graph
        /// </summary>
        public void drawGraph()
        {
            //method for showing the segment with thier individual graph
            GraphPane graphpane = zedGraphControl.GraphPane; 
            //graph and axis titles
            graphpane.Title = "Cycle Data Graph";
            graphpane.XAxis.Title = "Time Interval (seconds)";
            graphpane.YAxis.Title = "Value";
            
            //pointpairlist to determine X and Y cordinates
            PointPairList hrPPL = new PointPairList();
            PointPairList speedPPL = new PointPairList();
            PointPairList cadencePPL = new PointPairList();
            PointPairList altitudePPL = new PointPairList();
            PointPairList powerPPL = new PointPairList();
            int totalItems = arrayHR.Length;
            // x-axis and y-axis min, max value
            graphpane.YAxis.Min = 0;
            graphpane.YAxis.Max = 2000;
            graphpane.XAxis.Min = 0;
            graphpane.XAxis.Max = totalItems;

            int data = totalItems - 1;
            for (int i = 0; i < totalItems; i++)
            {
                hrPPL.Add(i, arrayHR[i]);
                speedPPL.Add(i, arraySpeed[data]);
                cadencePPL.Add(i, arrayCadence[data]);
                altitudePPL.Add(i, arrayAltitude[data]);
                powerPPL.Add(i, arrayPower[data]);
                data--;
            }
            
            //creating line and adding to graph panel
            LineItem heartRateCurve = graphpane.AddCurve("Heart Rate", hrPPL, Color.Red, SymbolType.None);
            LineItem speedCurve = graphpane.AddCurve("Speed", speedPPL, Color.Blue, SymbolType.None);
            LineItem cadenceCurve = graphpane.AddCurve("Cadence", cadencePPL, Color.Black, SymbolType.None);
            LineItem altitudeCurve = graphpane.AddCurve("Altitude", altitudePPL, Color.Brown, SymbolType.None);
            LineItem powerCurve = graphpane.AddCurve("Power", powerPPL, Color.Green, SymbolType.None);

            //for auto scaling operation
            zedGraphControl.AxisChange();
            zedGraphControl.Invalidate();
        }

        /// <summary>
        /// event for chunk selected data button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChunkSelectedData_Click(object sender, EventArgs e)
        {
            int chunkPortion;
            int[] remainingArrayHR;
            int[] remainingArraySpeed;
            int[] remainingArrayCadence;
            int[] remainingArrayAltitude;
            int[] remainingArrayPower;

            //to clear all the array elements
            Array.Clear(chunk1ArrayHR, 0, chunk1ArrayHR.Length);
            Array.Clear(chunk2ArrayHR, 0, chunk2ArrayHR.Length);
            Array.Clear(chunk3ArrayHR, 0, chunk3ArrayHR.Length);
            Array.Clear(chunk4ArrayHR, 0, chunk4ArrayHR.Length);
            Array.Clear(chunk1ArraySpeed, 0, chunk1ArraySpeed.Length);
            Array.Clear(chunk2ArraySpeed, 0, chunk2ArraySpeed.Length);
            Array.Clear(chunk3ArraySpeed, 0, chunk3ArraySpeed.Length);
            Array.Clear(chunk4ArraySpeed, 0, chunk4ArraySpeed.Length);
            Array.Clear(chunk1ArrayCadence, 0, chunk1ArrayCadence.Length);
            Array.Clear(chunk2ArrayCadence, 0, chunk2ArrayCadence.Length);
            Array.Clear(chunk3ArrayCadence, 0, chunk3ArrayCadence.Length);
            Array.Clear(chunk4ArrayCadence, 0, chunk4ArrayCadence.Length);
            Array.Clear(chunk1ArrayAltitude, 0, chunk1ArrayAltitude.Length);
            Array.Clear(chunk2ArrayAltitude, 0, chunk2ArrayAltitude.Length);
            Array.Clear(chunk3ArrayAltitude, 0, chunk3ArrayAltitude.Length);
            Array.Clear(chunk4ArrayAltitude, 0, chunk4ArrayAltitude.Length);
            Array.Clear(chunk1ArrayPower, 0, chunk1ArrayPower.Length);
            Array.Clear(chunk2ArrayPower, 0, chunk2ArrayPower.Length);
            Array.Clear(chunk3ArrayPower, 0, chunk3ArrayPower.Length);
            Array.Clear(chunk4ArrayPower, 0, chunk4ArrayPower.Length);

            int selectedrows = dataGridView.SelectedRows.Count;

            //to load new array elements with condition
            if (selectedrows==2)
            {
                chunk1ArrayHR = arrayHR.Take(arrayHR.Length / 2).ToArray();
                chunk1ArraySpeed = arraySpeed.Take(arraySpeed.Length / 2).ToArray();
                chunk1ArrayCadence = arrayCadence.Take(arrayCadence.Length / 2).ToArray();
                chunk1ArrayAltitude = arrayAltitude.Take(arrayAltitude.Length / 2).ToArray();
                chunk1ArrayPower = arrayPower.Take(arrayPower.Length / 2).ToArray();
                chunk2ArrayHR = arrayHR.Skip(arrayHR.Length / 2).ToArray();
                chunk2ArraySpeed = arraySpeed.Skip(arraySpeed.Length / 2).ToArray();
                chunk2ArrayCadence = arrayCadence.Skip(arrayCadence.Length / 2).ToArray();
                chunk2ArrayAltitude = arrayAltitude.Skip(arrayAltitude.Length / 2).ToArray();
                chunk2ArrayPower = arrayPower.Skip(arrayPower.Length / 2).ToArray();

                SelectedChunkData chunkData = new SelectedChunkData(selectedrows, totalSeconds);
                chunkData.ShowDialog();
            }
            else if(selectedrows==3)
            {
                ChunkData chunkData = new ChunkData(selectedrows);
                chunkData.ShowDialog();
                chunkPortion = ChunkData.chunkPortion;
                if (chunkPortion == 2)
                {
                    chunk1ArrayHR = arrayHR.Take(arrayHR.Length / 2).ToArray();
                    chunk1ArraySpeed = arraySpeed.Take(arraySpeed.Length / 2).ToArray();
                    chunk1ArrayCadence = arrayCadence.Take(arrayCadence.Length / 2).ToArray();
                    chunk1ArrayAltitude = arrayAltitude.Take(arrayAltitude.Length / 2).ToArray();
                    chunk1ArrayPower = arrayPower.Take(arrayPower.Length / 2).ToArray();
                    chunk2ArrayHR = arrayHR.Skip(arrayHR.Length / 2).ToArray();
                    chunk2ArraySpeed = arraySpeed.Skip(arraySpeed.Length / 2).ToArray();
                    chunk2ArrayCadence = arrayCadence.Skip(arrayCadence.Length / 2).ToArray();
                    chunk2ArrayAltitude = arrayAltitude.Skip(arrayAltitude.Length / 2).ToArray();
                    chunk2ArrayPower = arrayPower.Skip(arrayPower.Length / 2).ToArray();

                    SelectedChunkData selectedChunkData = new SelectedChunkData(chunkPortion, totalSeconds);
                    selectedChunkData.ShowDialog();
                }
                else
                {
                    chunk1ArrayHR = arrayHR.Take(arrayHR.Length / 3).ToArray();
                    chunk1ArraySpeed = arraySpeed.Take(arraySpeed.Length / 3).ToArray();
                    chunk1ArrayCadence = arrayCadence.Take(arrayCadence.Length / 3).ToArray();
                    chunk1ArrayAltitude = arrayAltitude.Take(arrayAltitude.Length / 3).ToArray();
                    chunk1ArrayPower = arrayPower.Take(arrayPower.Length / 3).ToArray();
                    remainingArrayHR = arrayHR.Skip(arrayHR.Length / 3).ToArray();
                    remainingArraySpeed = arraySpeed.Skip(arraySpeed.Length / 3).ToArray();
                    remainingArrayCadence = arrayCadence.Skip(arrayCadence.Length / 3).ToArray();
                    remainingArrayAltitude = arrayAltitude.Skip(arrayAltitude.Length / 3).ToArray();
                    remainingArrayPower = arrayPower.Skip(arrayPower.Length / 3).ToArray();
                    chunk2ArrayHR = remainingArrayHR.Take(remainingArrayHR.Length / 2).ToArray();
                    chunk2ArraySpeed = remainingArraySpeed.Take(remainingArraySpeed.Length / 2).ToArray();
                    chunk2ArrayCadence = remainingArrayCadence.Take(remainingArrayCadence.Length / 2).ToArray();
                    chunk2ArrayAltitude = remainingArrayAltitude.Take(remainingArrayAltitude.Length / 2).ToArray();
                    chunk2ArrayPower = remainingArrayPower.Take(remainingArrayPower.Length / 2).ToArray();
                    chunk3ArrayHR = remainingArrayHR.Skip(remainingArrayHR.Length / 2).ToArray();
                    chunk3ArraySpeed = remainingArraySpeed.Skip(remainingArraySpeed.Length / 2).ToArray();
                    chunk3ArrayCadence = remainingArrayCadence.Skip(remainingArrayCadence.Length / 2).ToArray();
                    chunk3ArrayAltitude = remainingArrayAltitude.Skip(remainingArrayAltitude.Length / 2).ToArray();
                    chunk3ArrayPower = remainingArrayPower.Skip(remainingArrayPower.Length / 2).ToArray();

                    SelectedChunkData selectedChunkData = new SelectedChunkData(chunkPortion, totalSeconds);
                    selectedChunkData.ShowDialog();
                }
            }
            else if (selectedrows>3)
            {
                ChunkData chunkData = new ChunkData(selectedrows);
                chunkData.ShowDialog();
                chunkPortion = ChunkData.chunkPortion;
                if (chunkPortion == 2)
                {
                    chunk1ArrayHR = arrayHR.Take(arrayHR.Length / 2).ToArray();
                    chunk1ArraySpeed = arraySpeed.Take(arraySpeed.Length / 2).ToArray();
                    chunk1ArrayCadence = arrayCadence.Take(arrayCadence.Length / 2).ToArray();
                    chunk2ArrayAltitude = arrayAltitude.Take(arrayAltitude.Length / 2).ToArray();
                    chunk1ArrayPower = arrayPower.Take(arrayPower.Length / 2).ToArray();
                    chunk2ArrayHR = arrayHR.Skip(arrayHR.Length / 2).ToArray();
                    chunk2ArraySpeed = arraySpeed.Skip(arraySpeed.Length / 2).ToArray();
                    chunk2ArrayCadence = arrayCadence.Skip(arrayCadence.Length / 2).ToArray();
                    chunk2ArrayAltitude = arrayAltitude.Skip(arrayAltitude.Length / 2).ToArray();
                    chunk2ArrayPower = arrayPower.Skip(arrayPower.Length / 2).ToArray();

                    SelectedChunkData selectedChunkData = new SelectedChunkData(chunkPortion, totalSeconds);
                    selectedChunkData.ShowDialog();
                }
                else if (chunkPortion == 3)
                {
                    chunk1ArrayHR = arrayHR.Take(arrayHR.Length / 3).ToArray();
                    chunk1ArraySpeed = arraySpeed.Take(arraySpeed.Length / 3).ToArray();
                    chunk1ArrayCadence = arrayCadence.Take(arrayCadence.Length / 3).ToArray();
                    chunk1ArrayAltitude = arrayAltitude.Take(arrayAltitude.Length / 3).ToArray();
                    chunk1ArrayPower = arrayPower.Take(arrayPower.Length / 3).ToArray();
                    remainingArrayHR = arrayHR.Skip(arrayHR.Length / 3).ToArray();
                    remainingArraySpeed = arraySpeed.Skip(arraySpeed.Length / 3).ToArray();
                    remainingArrayCadence = arrayCadence.Skip(arrayCadence.Length / 3).ToArray();
                    remainingArrayAltitude = arrayAltitude.Skip(arrayAltitude.Length / 3).ToArray();
                    remainingArrayPower = arrayPower.Skip(arrayPower.Length / 3).ToArray();
                    chunk2ArrayHR = remainingArrayHR.Take(remainingArrayHR.Length / 2).ToArray();
                    chunk2ArraySpeed = remainingArraySpeed.Take(remainingArraySpeed.Length / 2).ToArray();
                    chunk2ArrayCadence = remainingArrayCadence.Take(remainingArrayCadence.Length / 2).ToArray();
                    chunk2ArrayAltitude = remainingArrayAltitude.Take(remainingArrayAltitude.Length / 2).ToArray();
                    chunk2ArrayPower = remainingArrayPower.Take(remainingArrayPower.Length / 2).ToArray();
                    chunk3ArrayHR = remainingArrayHR.Skip(remainingArrayHR.Length / 2).ToArray();
                    chunk3ArraySpeed = remainingArraySpeed.Skip(remainingArraySpeed.Length / 2).ToArray();
                    chunk3ArrayCadence = remainingArrayCadence.Skip(remainingArrayCadence.Length / 2).ToArray();
                    chunk3ArrayAltitude = remainingArrayAltitude.Skip(remainingArrayAltitude.Length / 2).ToArray();
                    chunk3ArrayPower = remainingArrayPower.Skip(remainingArrayPower.Length / 2).ToArray();

                    SelectedChunkData selectedChunkData = new SelectedChunkData(chunkPortion, totalSeconds);
                    selectedChunkData.ShowDialog();
                }
                else
                {
                    chunk1ArrayHR = arrayHR.Take(arrayHR.Length / 4).ToArray();
                    chunk1ArraySpeed = arraySpeed.Take(arraySpeed.Length / 4).ToArray();
                    chunk1ArrayCadence = arrayCadence.Take(arrayCadence.Length / 4).ToArray();
                    chunk1ArrayAltitude = arrayAltitude.Take(arrayAltitude.Length / 4).ToArray();
                    chunk1ArrayPower = arrayPower.Take(arrayPower.Length / 4).ToArray();
                    remainingArrayHR = arrayHR.Skip(arrayHR.Length / 4).ToArray();
                    remainingArraySpeed = arraySpeed.Skip(arraySpeed.Length / 4).ToArray();
                    remainingArrayCadence = arrayCadence.Skip(arrayCadence.Length / 4).ToArray();
                    remainingArrayAltitude = arrayAltitude.Skip(arrayAltitude.Length / 4).ToArray();
                    remainingArrayPower = arrayPower.Skip(arrayPower.Length / 4).ToArray();
                    chunk2ArrayHR = remainingArrayHR.Take(remainingArrayHR.Length / 3).ToArray();
                    chunk2ArraySpeed = remainingArraySpeed.Take(remainingArraySpeed.Length / 3).ToArray();
                    chunk2ArrayCadence = remainingArrayCadence.Take(remainingArrayCadence.Length / 3).ToArray();
                    chunk2ArrayAltitude = remainingArrayAltitude.Take(remainingArrayAltitude.Length / 3).ToArray();
                    chunk3ArrayPower = remainingArrayPower.Take(remainingArrayPower.Length / 3).ToArray();
                    remainingArrayHR = remainingArrayHR.Skip(remainingArrayHR.Length / 3).ToArray();
                    remainingArraySpeed = remainingArraySpeed.Skip(remainingArraySpeed.Length / 3).ToArray();
                    remainingArrayCadence = remainingArrayCadence.Skip(remainingArrayCadence.Length / 3).ToArray();
                    remainingArrayAltitude = remainingArrayAltitude.Skip(remainingArrayAltitude.Length / 3).ToArray();
                    remainingArrayPower = remainingArrayPower.Skip(remainingArrayPower.Length / 3).ToArray();
                    chunk3ArrayHR = remainingArrayHR.Take(remainingArrayHR.Length / 2).ToArray();
                    chunk3ArraySpeed = remainingArraySpeed.Take(remainingArraySpeed.Length / 2).ToArray();
                    chunk3ArrayCadence = remainingArrayCadence.Take(remainingArrayCadence.Length / 2).ToArray();
                    chunk3ArrayAltitude = remainingArrayAltitude.Take(remainingArrayAltitude.Length / 2).ToArray();
                    chunk3ArrayPower = remainingArrayPower.Take(remainingArrayPower.Length / 2).ToArray();
                    chunk4ArrayHR = remainingArrayHR.Skip(remainingArrayHR.Length / 2).ToArray();
                    chunk4ArraySpeed = remainingArraySpeed.Skip(remainingArraySpeed.Length / 2).ToArray();
                    chunk4ArrayCadence = remainingArrayCadence.Skip(remainingArrayCadence.Length / 2).ToArray();
                    chunk4ArrayAltitude = remainingArrayAltitude.Skip(remainingArrayAltitude.Length / 2).ToArray();
                    chunk4ArrayPower = remainingArrayPower.Skip(remainingArrayPower.Length / 2).ToArray();

                    SelectedChunkData selectedChunkData = new SelectedChunkData(chunkPortion, totalSeconds);
                    selectedChunkData.ShowDialog();
                }
            }
        }
    }
}
