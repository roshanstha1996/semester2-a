﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;

namespace DataAnalysisSoftware
{
    public partial class Form1 : Form
    {
        
        //variable declaration
        //can be used everywhere in this class
        string file;
        string sMode;
        string line;
        string startTime = "00:00:00.000";
        string lengthValue = "00:00:00.000";
        string interval = "0";
        int[] ArrayHR;
        int[] ArraySpeed;
        int[] ArrayCadence;
        int[] ArrayAltitude;
        int[] ArrayPower;
        int totalItems;
        double AverageSpeed;
        double avgPower;
        double totalSeconds;

        Boolean sModeSpeed = true;

        
        /// <summary>
        /// calling methods for header, hrdata, summary and smode
        /// </summary>
        /// <param name="file"></param>
        public Form1(string file)
        {
            this.file = file;
            InitializeComponent();
            LoadHeaderInformation();
            hrdata();
            summarydata_load();
            smode_load();
            LoadDefaultColumnCellVal();
        }
        

        /// <summary>
        /// fetching the headers from the file
        /// </summary>
        public void LoadHeaderInformation()
        {
            StreamReader reader = new StreamReader(file);
            string line;
            while ((line=reader.ReadLine())!=null)  //reads each line of the file using stream reader
            {
                if (line.Contains("Version"))
                {
                    string version = line.Split('=').Last();
                    lblDeviceVersion.Text = version;
                }
                else if (line.Contains("Monitor"))
                {
                    string monitor = line.Split('=').Last();
                    lblMonitor.Text = monitor;
                }
                else if (line.Contains("SMode"))
                {
                    sMode = line.Split('=').Last();
                    lblSMode.Text = sMode;
                }
                else if (line.Contains("Date"))
                {
                    string date = line.Split('=').Last();
                    string y = date.Substring(0, 4);
                    string m = date.Substring(4, 2);
                    string d = date.Substring(6, 2);
                    string dateformat = y + "/" + m + "/" + d;
                    lblDate.Text = dateformat;
                }
                else if (line.Contains("StartTime"))
                {
                    startTime = line.Split('=').Last();
                    lblStartTime.Text = startTime;
                }
                else if (line.Contains("Length"))
                {
                    lengthValue = line.Split('=').Last();
                    lblLength.Text = lengthValue;
                }
                else if (line.Contains("Interval"))
                {
                    string interval = line.Split('=').Last();
                    lblInterval.Text = interval;
                }
            }
        }


        /// <summary>
        /// method for loading hrdata into the data grid view
        /// </summary>
        public void hrdata()
        {
            //converts string time to DateTime type
            DateTime typestartTime = DateTime.ParseExact(startTime,
                "HH:mm:ss.FFF", CultureInfo.InvariantCulture);
            int typeInterval = Convert.ToInt32(interval);

            string fileContent = File.ReadAllText(file);

            string toFind = "[HRData]";

            int index = fileContent.IndexOf(toFind);  //index of searched element
            //remaining file content after excluding searched element
            string searchFileContent = fileContent.Substring(index + 10, fileContent.Length - (index + 10));

            //string reader to read string content
            StringReader reader = new StringReader(searchFileContent);

            int row = 0;
            //reading each line of the string
            while ((line = reader.ReadLine()) != null)
            {
                //splits the line on specific words & displays fetched data
                string[] columnData = line.Split('\t');
                dataGridView1.Rows.Add();
                for (int i = 0; i < columnData.Length; i++)
                {
                    dataGridView1[i, row].Value = columnData[i];
                }
                row++;
            }
            int allrows = dataGridView1.RowCount;
            int rowIndex = 6;
            for (int columnIndex = 0; columnIndex < allrows; columnIndex++)
            {
                //adding time value in each column
                dataGridView1[rowIndex, columnIndex].Value = typestartTime.ToLongTimeString();
                typestartTime = typestartTime.AddSeconds(typeInterval);
            }
            LoadDefaultColumnCellVal();
        }
        

        /// <summary>
        /// method for loading smode into the form
        /// </summary>
        public void smode_load()
        {
            //default value for every text field
            lblsmSpeed.Text = "True";
            lblsmCad.Text = "True";
            lblsmAltd.Text = "True";
            lblsmPower.Text = "True";
            lblsmpowerbalance.Text = "True";
            char[] sModeChar = new char[sMode.Length];

            for (int i = 0; i < sMode.Length; i++)
            {
                //reading and storing each character of SMode value
                sModeChar[i] = sMode[i];
            }
            //columns to hide based on SMode format
            //speed
            if (sModeChar[0] == '0')
            {
                sModeSpeed = false;
                lblsmSpeed.Text = "false";
                //dataGridView1.Columns["Speed"].Visible = false;
                lblTotalDistance.Text = " ";
                lblAvgSpeed.Text = " ";
                lblMaxSpeed.Text = " ";
            }
            //cadence
            if (sModeChar[1] == '0')
            {
                lblsmCad.Text = "false";
                //dataGridView1.Columns["Cadence"].Visible = false;
            }
            //altitude
            if (sModeChar[2] == '0')
            {
                lblsmAltd.Text = "false";
                //dataGridView1.Columns["Altitude"].Visible = false;
            }
            //power in watts
            if (sModeChar[3] == '0')
            {
                lblsmPower.Text = "false";
                //dataGridView1.Columns["PowerInWatts"].Visible = false;
            }
            //power balance and pedalling index
            if (sModeChar[4] == '0')
            {
                lblsmpowerbalance.Text = "false";
                //dataGridView1.Columns["PowerBalanceAndPedallingIndex"].Visible = false;
            }
        }


        /// <summary>
        /// method to load default column cell value
        /// </summary>
        public void LoadDefaultColumnCellVal()
        {
            int totalRows = dataGridView1.RowCount;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((row.Cells["HeartRate"].Value) == null)
                {
                    row.Cells["HeartRate"].Value = 0;
                }
                if ((row.Cells["Speed"].Value) == null)
                {
                    row.Cells["Speed"].Value = 0;
                }
                if ((row.Cells["Cadence"].Value) == null)
                {
                    row.Cells["Cadence"].Value = 0;
                }
                if ((row.Cells["Altitude"].Value) == null)
                {
                    row.Cells["Altitude"].Value = 0;
                }
                if ((row.Cells["Power"].Value) == null)
                {
                    row.Cells["Power"].Value = 0;
                }
                if ((row.Cells["PowerIndex"].Value) == null)
                {
                    row.Cells["PowerIndex"].Value = 0;
                }
            }
        }

        /// <summary>
        /// method to load summary data
        /// </summary>
        public void summarydata_load()
        {
            List<int> hr = new List<int>();
            List<int> speed = new List<int>();
            List<int> cadence = new List<int>();
            List<int> altitude = new List<int>();
            List<int> power = new List<int>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //adding data of every column to its repective list  
                hr.Add(int.Parse(row.Cells["HeartRate"].Value.ToString()));
                speed.Add(int.Parse(row.Cells["Speed"].Value.ToString()));
                cadence.Add(int.Parse(row.Cells["Cadence"].Value.ToString()));
                altitude.Add(int.Parse(row.Cells["Altitude"].Value.ToString()));
                power.Add(int.Parse(row.Cells["Power"].Value.ToString()));
            }

            //now converting list to array
            ArrayHR = hr.ToArray();
            ArraySpeed = speed.ToArray();
            ArrayCadence = cadence.ToArray();
            ArrayAltitude = altitude.ToArray();
            ArrayPower = power.ToArray();
            
            //methods called to calculate summary data
            calcSpeed();
            calcHR();
            calcPower();
            calcAltitude();
            calcTotalDistance();
        }

        /// <summary>
        /// constructor of main form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
        }

       
        /// <summary>
        /// called when graph button is clicked
        /// it displays graph for the file opened with x axis being time and y axis being data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnViewGraph_Click(object sender, EventArgs e)
        {
            
            ViewGraph viewgraph = new ViewGraph(ArrayHR, ArraySpeed, ArrayCadence, ArrayAltitude, ArrayPower);
            viewgraph.ShowDialog();
        }

        /// <summary>
        ///method to calculate speed data
        /// </summary>
        public void calcSpeed()
        {
            totalItems = ArraySpeed.Length;
            int totalSpeed = 0;
            for(int i = 0; i < totalItems; i++)
            {
                //total speed
                totalSpeed = totalSpeed + ArraySpeed[i];
            }
            //average speed
            AverageSpeed = Math.Round((double) totalSpeed / totalItems, 2);
            lblAvgSpeed.Text = AverageSpeed.ToString() + " " + "km/h";
            //maximum speed
            int maxSpeed = ArraySpeed.Max();
            lblMaxSpeed.Text = maxSpeed.ToString() + " " + "km/h";
        }

        /// <summary>
        /// method to calculate heart rate data
        /// </summary>
        public void calcHR()
        {
            totalItems = ArrayHR.Length;
            int totalHeartRate = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total heart rate
                totalHeartRate = totalHeartRate + ArrayHR[i];
            }
            //average heart rate
            int avgHR = totalHeartRate / totalItems;
            lblAvgHeartRate.Text = avgHR.ToString() + " " + "bpm";
            //maximum heart rate
            int maxHR = ArrayHR.Max();
            lblMaxHeartRate.Text = maxHR.ToString() + " " + "bpm";
            //minimum heart rate
            int minHR = ArrayHR.Min();
            lblMinHeartRate.Text = minHR.ToString() + " " + "bpm";
        }

        /// <summary>
        /// method to calculate power data
        /// </summary>
        public void calcPower()
        {
            totalItems = ArrayPower.Length;
            int totalPower = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total power
                totalPower = totalPower + ArrayPower[i];
            }
            //average power
            avgPower = totalPower / totalItems;
            lblAvgPower.Text = avgPower.ToString() + " " + "watts";
            //maximum power
            int maxPower = ArrayPower.Max();
            lblMaxPower.Text = maxPower.ToString() + " " + "watts";
        }

        /// <summary>
        /// method to calculate  altitude data
        /// </summary>
        public void calcAltitude()
        {
            totalItems = ArrayAltitude.Length;
            int totalAltitude = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total altitude
                totalAltitude = totalAltitude + ArrayAltitude[i];
            }
            //average altitude
            double avgAltitude = Math.Round((double)totalAltitude / totalItems, 2);
            lblAvgAltitude.Text = avgAltitude.ToString() + " " + "m/ft";
            //maximum altitude
            int maxAltitude = ArrayAltitude.Max();
            lblMaxAltitude.Text = maxAltitude.ToString() + " " + "m/ft";
        }


        /// <summary>
        /// method to calculate distance covered
        /// </summary>
        public void calcTotalDistance()
        {
            string length = lengthValue;
            //total seconds from start time
            totalSeconds = TimeSpan.Parse(length).TotalSeconds;
            //calculating distance covered
            double totalDistance = Math.Round((double)(((AverageSpeed / 60) / 60) * totalSeconds), 2);
            lblTotalDistance.Text = totalDistance.ToString() + " " + "km/h";
        }


        /// <summary>
        /// when clicked this radio button, the data gets converted to km/h
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioKM_CheckedChanged(object sender, EventArgs e)
        {
            radioKM.Enabled = false;
            radioMiles.Enabled = true;
            int rowIndex = 1;
            totalItems = ArraySpeed.Length;
            for (int columnIndex = 0; columnIndex < totalItems; columnIndex++)
            {
                //speed in km/h
                dataGridView1[rowIndex, columnIndex].Value = ArraySpeed[columnIndex];
            }
            //speed and distance calculation as per SMode information
            if (sModeSpeed==true)
            {
                totalItems = ArraySpeed.Length;
                int totalSpeed = 0;
                for (int i = 0; i < totalItems; i++)
                {
                    totalSpeed = totalSpeed + ArraySpeed[i];
                }
                double avgSpeed = Math.Round((double)totalSpeed / totalItems, 2);
                lblAvgSpeed.Text = avgSpeed.ToString() + " " + "km/h";
                int maxSpeed = ArraySpeed.Max();
                lblMaxSpeed.Text = maxSpeed.ToString() + " " + "km/h";
                string timestart = startTime;
                double totalseconds = TimeSpan.Parse(startTime).TotalSeconds;
                double totaldistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalseconds), 2);
                lblTotalDistance.Text = totaldistance.ToString() + " " + "km/h";
            }
        }

        /// <summary>
        /// event for advanced metrics calculation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdvancedMetrics_Click(object sender, EventArgs e)
        {
            AdvancedMetricsCalculation amc = new AdvancedMetricsCalculation(avgPower, totalSeconds);
            amc.Show();
        }

        /// <summary>
        /// event for interval detection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnIntervalDetection_Click(object sender, EventArgs e)
        {
            IntervalDetection intdec = new IntervalDetection(ArrayHR, ArraySpeed, ArrayCadence,
                ArrayAltitude, ArrayPower, totalSeconds);
            intdec.Show();
        }

        /// <summary>
        /// when clicked this radio button, the data gets converted to m/h
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioMiles_CheckedChanged(object sender, EventArgs e)
        {
            radioMiles.Enabled = false;
            radioKM.Enabled = true;
            int rowIndex = 1;
            totalItems = ArraySpeed.Length;
            double[] kmToMiles = new double[totalItems];
            for (int columnIndex = 0; columnIndex < totalItems; columnIndex++)
            {
                //speed in mph
                kmToMiles[columnIndex] = Math.Round(ArraySpeed[columnIndex] * 0.621371, 2);
                dataGridView1[rowIndex, columnIndex].Value = kmToMiles[columnIndex];
            }
            
            //calculation of speed, distance 
            if (sModeSpeed==true)
            {
                totalItems = kmToMiles.Length;
                double totalSpeed = 0;
                for (int i = 0; i < totalItems; i++)
                {
                    totalSpeed = totalSpeed + kmToMiles[i];
                }
                double avgSpeed = Math.Round((double)totalSpeed / totalItems, 2);
                lblAvgSpeed.Text = avgSpeed.ToString() + " " + "mph";
                double maxSpeed = kmToMiles.Max();
                lblMaxSpeed.Text = maxSpeed.ToString() + " " + "mph";
                string timestart = startTime;
                double totalseconds = TimeSpan.Parse(startTime).TotalSeconds;
                double totaldistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalseconds), 2);
                lblTotalDistance.Text = totaldistance.ToString() + " " + "mph";
            }
        }

        /// <summary>
        /// event for file comparison
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFileComparison_Click(object sender, EventArgs e)
        {
            Browse2Files b2f = new Browse2Files();
            b2f.Show();
        }

        /// <summary>
        /// button event for selection information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectableInformation_Click(object sender, EventArgs e)
        {
            int[] arrayPowerBalance;
            string[] arrayTime;
            List<int> powerBalance = new List<int>();
            List<string> time = new List<string>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //adding power balance and time stamo data to corresponding column
                powerBalance.Add(int.Parse(row.Cells["PowerIndex"].Value.ToString()));
                time.Add(row.Cells["TimeStamp"].Value.ToString());
            }
            //converting list to array
            arrayPowerBalance = powerBalance.ToArray();
            arrayTime = time.ToArray();

            //calling selectable information class constructor
            SelectableInformation selectableInformation = new SelectableInformation(ArrayHR, ArraySpeed, ArrayCadence,
                ArrayAltitude, ArrayPower, arrayPowerBalance, arrayTime, totalSeconds);
            selectableInformation.Show();
        }
    }
}
