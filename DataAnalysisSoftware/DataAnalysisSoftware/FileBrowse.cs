﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAnalysisSoftware
{
    public partial class FileBrowse : Form
    {
        public FileBrowse()
        {
            InitializeComponent();
        }

        private void btn_browse_hrm_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "HRM file (*.hrm) | *.hrm";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string file = ofd.FileName;
                Form1 frm = new Form1(file); //creating and calling main form constructor 
                frm.ShowDialog();
            }
        }
    }
}
