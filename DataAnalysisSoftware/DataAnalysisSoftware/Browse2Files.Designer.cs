﻿namespace DataAnalysisSoftware
{
    partial class Browse2Files
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_browseFile1 = new System.Windows.Forms.Button();
            this.lbl_select = new System.Windows.Forms.Label();
            this.lbl_title = new System.Windows.Forms.Label();
            this.btn_browseFile2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtfile1Path = new System.Windows.Forms.TextBox();
            this.txtfile2Path = new System.Windows.Forms.TextBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_browseFile1
            // 
            this.btn_browseFile1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_browseFile1.ForeColor = System.Drawing.Color.Blue;
            this.btn_browseFile1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_browseFile1.Location = new System.Drawing.Point(600, 110);
            this.btn_browseFile1.Name = "btn_browseFile1";
            this.btn_browseFile1.Size = new System.Drawing.Size(119, 34);
            this.btn_browseFile1.TabIndex = 8;
            this.btn_browseFile1.Text = "Select";
            this.btn_browseFile1.UseVisualStyleBackColor = true;
            this.btn_browseFile1.Click += new System.EventHandler(this.btn_browseFile1_Click);
            // 
            // lbl_select
            // 
            this.lbl_select.AutoSize = true;
            this.lbl_select.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_select.ForeColor = System.Drawing.Color.White;
            this.lbl_select.Location = new System.Drawing.Point(31, 83);
            this.lbl_select.Name = "lbl_select";
            this.lbl_select.Size = new System.Drawing.Size(274, 24);
            this.lbl_select.TabIndex = 7;
            this.lbl_select.Text = "Browse for HRM File 1:";
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.ForeColor = System.Drawing.Color.White;
            this.lbl_title.Location = new System.Drawing.Point(12, 19);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(295, 33);
            this.lbl_title.TabIndex = 6;
            this.lbl_title.Text = "Polar Cycle Data Analysis";
            // 
            // btn_browseFile2
            // 
            this.btn_browseFile2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_browseFile2.ForeColor = System.Drawing.Color.Blue;
            this.btn_browseFile2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_browseFile2.Location = new System.Drawing.Point(600, 202);
            this.btn_browseFile2.Name = "btn_browseFile2";
            this.btn_browseFile2.Size = new System.Drawing.Size(119, 34);
            this.btn_browseFile2.TabIndex = 10;
            this.btn_browseFile2.Text = "Select";
            this.btn_browseFile2.UseVisualStyleBackColor = true;
            this.btn_browseFile2.Click += new System.EventHandler(this.btn_browseFile2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(33, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(274, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "Browse for HRM File 2:";
            // 
            // txtfile1Path
            // 
            this.txtfile1Path.Enabled = false;
            this.txtfile1Path.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtfile1Path.Location = new System.Drawing.Point(35, 124);
            this.txtfile1Path.Name = "txtfile1Path";
            this.txtfile1Path.ReadOnly = true;
            this.txtfile1Path.Size = new System.Drawing.Size(537, 20);
            this.txtfile1Path.TabIndex = 11;
            // 
            // txtfile2Path
            // 
            this.txtfile2Path.Enabled = false;
            this.txtfile2Path.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtfile2Path.Location = new System.Drawing.Point(35, 216);
            this.txtfile2Path.Name = "txtfile2Path";
            this.txtfile2Path.ReadOnly = true;
            this.txtfile2Path.Size = new System.Drawing.Size(537, 20);
            this.txtfile2Path.TabIndex = 12;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancel.ForeColor = System.Drawing.Color.DarkCyan;
            this.btn_cancel.Location = new System.Drawing.Point(390, 266);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(111, 47);
            this.btn_cancel.TabIndex = 14;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ok.ForeColor = System.Drawing.Color.SlateGray;
            this.btn_ok.Location = new System.Drawing.Point(154, 266);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(111, 47);
            this.btn_ok.TabIndex = 13;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // Browse2Files
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkCyan;
            this.ClientSize = new System.Drawing.Size(742, 351);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.txtfile2Path);
            this.Controls.Add(this.txtfile1Path);
            this.Controls.Add(this.btn_browseFile2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_browseFile1);
            this.Controls.Add(this.lbl_select);
            this.Controls.Add(this.lbl_title);
            this.Name = "Browse2Files";
            this.Text = "Browse2Files";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_browseFile1;
        private System.Windows.Forms.Label lbl_select;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Button btn_browseFile2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtfile1Path;
        private System.Windows.Forms.TextBox txtfile2Path;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_ok;
    }
}