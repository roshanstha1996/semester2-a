﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAnalysisSoftware
{
    /// <summary>
    /// chunk data class
    /// </summary>
    public partial class ChunkData : Form
    {
        //declared variable
        public static int chunkPortion = 0;

        /// <summary>
        /// chunk data constructor
        /// </summary>
        /// <param name="chunkPortion"></param>
        public ChunkData(int chunkPortion)
        {
            InitializeComponent();
            //adding number to combo box
            if (chunkPortion==3)
            {
                cmbchunkValue.Items.Add("2");
                cmbchunkValue.Items.Add("3");
            }
            else
            {
                cmbchunkValue.Items.Add("2");
                cmbchunkValue.Items.Add("3");
                cmbchunkValue.Items.Add("4");
            }
        }

        /// <summary>
        /// event for chunk button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_chunk_Click(object sender, EventArgs e)
        {
            string chunk = cmbchunkValue.Text;
            if (!string.IsNullOrEmpty(chunk))
            {
                chunkPortion = Convert.ToInt32(chunk);
                Dispose();
            }
            else
            {
                MessageBox.Show("You haven't entered the number for chunking");
            }
        }
    }
}
