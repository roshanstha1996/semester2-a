﻿namespace DataAnalysisSoftware
{
    partial class IntervalDetection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lbltotalDistance = new System.Windows.Forms.Label();
            this.lbl_totalDistance = new System.Windows.Forms.Label();
            this.lblmaxAltitude = new System.Windows.Forms.Label();
            this.lblavgAltitude = new System.Windows.Forms.Label();
            this.lblmaxPower = new System.Windows.Forms.Label();
            this.lblavgPower = new System.Windows.Forms.Label();
            this.lblminHeartRate = new System.Windows.Forms.Label();
            this.lblmaxHeartRate = new System.Windows.Forms.Label();
            this.lblavgHeartRate = new System.Windows.Forms.Label();
            this.lblmaxSpeed = new System.Windows.Forms.Label();
            this.lblavgSpeed = new System.Windows.Forms.Label();
            this.lbl_maxAltitude = new System.Windows.Forms.Label();
            this.lbl_avgAltitude = new System.Windows.Forms.Label();
            this.lbl_maxPower = new System.Windows.Forms.Label();
            this.lbl_avgPower = new System.Windows.Forms.Label();
            this.lbl_minHeartRate = new System.Windows.Forms.Label();
            this.lbl_maxHeartRate = new System.Windows.Forms.Label();
            this.lbl_avgHeartRate = new System.Windows.Forms.Label();
            this.lbl_maxSpeed = new System.Windows.Forms.Label();
            this.lbl_avgSpeed = new System.Windows.Forms.Label();
            this.txtInterval = new System.Windows.Forms.TextBox();
            this.btnPreviousInterval = new System.Windows.Forms.Button();
            this.lbl_interval = new System.Windows.Forms.Label();
            this.btnNextInterval = new System.Windows.Forms.Button();
            this.groupBox_summaryData = new System.Windows.Forms.GroupBox();
            this.Intervals = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox_summaryData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dataGridView1.Location = new System.Drawing.Point(654, 422);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(479, 247);
            this.dataGridView1.TabIndex = 21;
            // 
            // lbltotalDistance
            // 
            this.lbltotalDistance.AutoSize = true;
            this.lbltotalDistance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotalDistance.ForeColor = System.Drawing.Color.Black;
            this.lbltotalDistance.Location = new System.Drawing.Point(237, 46);
            this.lbltotalDistance.Name = "lbltotalDistance";
            this.lbltotalDistance.Size = new System.Drawing.Size(60, 21);
            this.lbltotalDistance.TabIndex = 31;
            this.lbltotalDistance.Text = "0 km/h";
            // 
            // lbl_totalDistance
            // 
            this.lbl_totalDistance.AutoSize = true;
            this.lbl_totalDistance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalDistance.ForeColor = System.Drawing.Color.Blue;
            this.lbl_totalDistance.Location = new System.Drawing.Point(6, 46);
            this.lbl_totalDistance.Name = "lbl_totalDistance";
            this.lbl_totalDistance.Size = new System.Drawing.Size(174, 21);
            this.lbl_totalDistance.TabIndex = 30;
            this.lbl_totalDistance.Text = "Total Distance Covered :";
            // 
            // lblmaxAltitude
            // 
            this.lblmaxAltitude.AutoSize = true;
            this.lblmaxAltitude.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblmaxAltitude.Location = new System.Drawing.Point(237, 379);
            this.lblmaxAltitude.Name = "lblmaxAltitude";
            this.lblmaxAltitude.Size = new System.Drawing.Size(53, 21);
            this.lblmaxAltitude.TabIndex = 29;
            this.lblmaxAltitude.Text = "0 m/ft";
            // 
            // lblavgAltitude
            // 
            this.lblavgAltitude.AutoSize = true;
            this.lblavgAltitude.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblavgAltitude.Location = new System.Drawing.Point(237, 341);
            this.lblavgAltitude.Name = "lblavgAltitude";
            this.lblavgAltitude.Size = new System.Drawing.Size(53, 21);
            this.lblavgAltitude.TabIndex = 28;
            this.lblavgAltitude.Text = "0 m/ft";
            // 
            // lblmaxPower
            // 
            this.lblmaxPower.AutoSize = true;
            this.lblmaxPower.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxPower.ForeColor = System.Drawing.Color.Black;
            this.lblmaxPower.Location = new System.Drawing.Point(237, 301);
            this.lblmaxPower.Name = "lblmaxPower";
            this.lblmaxPower.Size = new System.Drawing.Size(60, 21);
            this.lblmaxPower.TabIndex = 27;
            this.lblmaxPower.Text = "0 watts";
            // 
            // lblavgPower
            // 
            this.lblavgPower.AutoSize = true;
            this.lblavgPower.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgPower.ForeColor = System.Drawing.Color.Black;
            this.lblavgPower.Location = new System.Drawing.Point(237, 264);
            this.lblavgPower.Name = "lblavgPower";
            this.lblavgPower.Size = new System.Drawing.Size(60, 21);
            this.lblavgPower.TabIndex = 26;
            this.lblavgPower.Text = "0 watts";
            // 
            // lblminHeartRate
            // 
            this.lblminHeartRate.AutoSize = true;
            this.lblminHeartRate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblminHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblminHeartRate.Location = new System.Drawing.Point(237, 224);
            this.lblminHeartRate.Name = "lblminHeartRate";
            this.lblminHeartRate.Size = new System.Drawing.Size(55, 21);
            this.lblminHeartRate.TabIndex = 25;
            this.lblminHeartRate.Text = "0 bpm";
            // 
            // lblmaxHeartRate
            // 
            this.lblmaxHeartRate.AutoSize = true;
            this.lblmaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblmaxHeartRate.Location = new System.Drawing.Point(237, 189);
            this.lblmaxHeartRate.Name = "lblmaxHeartRate";
            this.lblmaxHeartRate.Size = new System.Drawing.Size(55, 21);
            this.lblmaxHeartRate.TabIndex = 24;
            this.lblmaxHeartRate.Text = "0 bpm";
            // 
            // lblavgHeartRate
            // 
            this.lblavgHeartRate.AutoSize = true;
            this.lblavgHeartRate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblavgHeartRate.Location = new System.Drawing.Point(237, 152);
            this.lblavgHeartRate.Name = "lblavgHeartRate";
            this.lblavgHeartRate.Size = new System.Drawing.Size(55, 21);
            this.lblavgHeartRate.TabIndex = 23;
            this.lblavgHeartRate.Text = "0 bpm";
            // 
            // lblmaxSpeed
            // 
            this.lblmaxSpeed.AutoSize = true;
            this.lblmaxSpeed.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblmaxSpeed.Location = new System.Drawing.Point(237, 114);
            this.lblmaxSpeed.Name = "lblmaxSpeed";
            this.lblmaxSpeed.Size = new System.Drawing.Size(60, 21);
            this.lblmaxSpeed.TabIndex = 22;
            this.lblmaxSpeed.Text = "0 km/h";
            // 
            // lblavgSpeed
            // 
            this.lblavgSpeed.AutoSize = true;
            this.lblavgSpeed.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblavgSpeed.Location = new System.Drawing.Point(237, 78);
            this.lblavgSpeed.Name = "lblavgSpeed";
            this.lblavgSpeed.Size = new System.Drawing.Size(60, 21);
            this.lblavgSpeed.TabIndex = 21;
            this.lblavgSpeed.Text = "0 km/h";
            // 
            // lbl_maxAltitude
            // 
            this.lbl_maxAltitude.AutoSize = true;
            this.lbl_maxAltitude.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxAltitude.Location = new System.Drawing.Point(6, 379);
            this.lbl_maxAltitude.Name = "lbl_maxAltitude";
            this.lbl_maxAltitude.Size = new System.Drawing.Size(145, 21);
            this.lbl_maxAltitude.TabIndex = 20;
            this.lbl_maxAltitude.Text = "Maximum Altitude :";
            // 
            // lbl_avgAltitude
            // 
            this.lbl_avgAltitude.AutoSize = true;
            this.lbl_avgAltitude.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgAltitude.Location = new System.Drawing.Point(6, 341);
            this.lbl_avgAltitude.Name = "lbl_avgAltitude";
            this.lbl_avgAltitude.Size = new System.Drawing.Size(132, 21);
            this.lbl_avgAltitude.TabIndex = 19;
            this.lbl_avgAltitude.Text = "Average Altitude :";
            // 
            // lbl_maxPower
            // 
            this.lbl_maxPower.AutoSize = true;
            this.lbl_maxPower.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxPower.Location = new System.Drawing.Point(6, 301);
            this.lbl_maxPower.Name = "lbl_maxPower";
            this.lbl_maxPower.Size = new System.Drawing.Size(134, 21);
            this.lbl_maxPower.TabIndex = 18;
            this.lbl_maxPower.Text = "Maximum Power :";
            // 
            // lbl_avgPower
            // 
            this.lbl_avgPower.AutoSize = true;
            this.lbl_avgPower.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgPower.Location = new System.Drawing.Point(6, 264);
            this.lbl_avgPower.Name = "lbl_avgPower";
            this.lbl_avgPower.Size = new System.Drawing.Size(121, 21);
            this.lbl_avgPower.TabIndex = 17;
            this.lbl_avgPower.Text = "Average Power :";
            // 
            // lbl_minHeartRate
            // 
            this.lbl_minHeartRate.AutoSize = true;
            this.lbl_minHeartRate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_minHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_minHeartRate.Location = new System.Drawing.Point(6, 224);
            this.lbl_minHeartRate.Name = "lbl_minHeartRate";
            this.lbl_minHeartRate.Size = new System.Drawing.Size(162, 21);
            this.lbl_minHeartRate.TabIndex = 16;
            this.lbl_minHeartRate.Text = "Minimum Heart Rate :";
            // 
            // lbl_maxHeartRate
            // 
            this.lbl_maxHeartRate.AutoSize = true;
            this.lbl_maxHeartRate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxHeartRate.Location = new System.Drawing.Point(6, 189);
            this.lbl_maxHeartRate.Name = "lbl_maxHeartRate";
            this.lbl_maxHeartRate.Size = new System.Drawing.Size(164, 21);
            this.lbl_maxHeartRate.TabIndex = 15;
            this.lbl_maxHeartRate.Text = "Maximum Heart Rate :";
            // 
            // lbl_avgHeartRate
            // 
            this.lbl_avgHeartRate.AutoSize = true;
            this.lbl_avgHeartRate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgHeartRate.Location = new System.Drawing.Point(6, 152);
            this.lbl_avgHeartRate.Name = "lbl_avgHeartRate";
            this.lbl_avgHeartRate.Size = new System.Drawing.Size(151, 21);
            this.lbl_avgHeartRate.TabIndex = 14;
            this.lbl_avgHeartRate.Text = "Average Heart Rate :";
            // 
            // lbl_maxSpeed
            // 
            this.lbl_maxSpeed.AutoSize = true;
            this.lbl_maxSpeed.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxSpeed.Location = new System.Drawing.Point(6, 114);
            this.lbl_maxSpeed.Name = "lbl_maxSpeed";
            this.lbl_maxSpeed.Size = new System.Drawing.Size(134, 21);
            this.lbl_maxSpeed.TabIndex = 13;
            this.lbl_maxSpeed.Text = "Maximum Speed :";
            // 
            // lbl_avgSpeed
            // 
            this.lbl_avgSpeed.AutoSize = true;
            this.lbl_avgSpeed.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgSpeed.Location = new System.Drawing.Point(6, 78);
            this.lbl_avgSpeed.Name = "lbl_avgSpeed";
            this.lbl_avgSpeed.Size = new System.Drawing.Size(121, 21);
            this.lbl_avgSpeed.TabIndex = 12;
            this.lbl_avgSpeed.Text = "Average Speed :";
            // 
            // txtInterval
            // 
            this.txtInterval.Enabled = false;
            this.txtInterval.Font = new System.Drawing.Font("Consolas", 20.25F);
            this.txtInterval.Location = new System.Drawing.Point(323, 575);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.ReadOnly = true;
            this.txtInterval.Size = new System.Drawing.Size(113, 39);
            this.txtInterval.TabIndex = 6;
            // 
            // btnPreviousInterval
            // 
            this.btnPreviousInterval.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreviousInterval.ForeColor = System.Drawing.Color.Black;
            this.btnPreviousInterval.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPreviousInterval.Location = new System.Drawing.Point(269, 575);
            this.btnPreviousInterval.Name = "btnPreviousInterval";
            this.btnPreviousInterval.Size = new System.Drawing.Size(48, 39);
            this.btnPreviousInterval.TabIndex = 7;
            this.btnPreviousInterval.Text = "<";
            this.btnPreviousInterval.UseVisualStyleBackColor = true;
            this.btnPreviousInterval.Click += new System.EventHandler(this.btnPreviousInterval_Click);
            // 
            // lbl_interval
            // 
            this.lbl_interval.AutoSize = true;
            this.lbl_interval.Font = new System.Drawing.Font("Segoe UI Semibold", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_interval.ForeColor = System.Drawing.Color.Lavender;
            this.lbl_interval.Location = new System.Drawing.Point(138, 575);
            this.lbl_interval.Name = "lbl_interval";
            this.lbl_interval.Size = new System.Drawing.Size(125, 37);
            this.lbl_interval.TabIndex = 6;
            this.lbl_interval.Text = "Interval :";
            // 
            // btnNextInterval
            // 
            this.btnNextInterval.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNextInterval.ForeColor = System.Drawing.Color.Black;
            this.btnNextInterval.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNextInterval.Location = new System.Drawing.Point(442, 575);
            this.btnNextInterval.Name = "btnNextInterval";
            this.btnNextInterval.Size = new System.Drawing.Size(46, 39);
            this.btnNextInterval.TabIndex = 4;
            this.btnNextInterval.Text = ">";
            this.btnNextInterval.UseVisualStyleBackColor = true;
            this.btnNextInterval.Click += new System.EventHandler(this.btnNextInterval_Click);
            // 
            // groupBox_summaryData
            // 
            this.groupBox_summaryData.BackColor = System.Drawing.Color.White;
            this.groupBox_summaryData.Controls.Add(this.lbltotalDistance);
            this.groupBox_summaryData.Controls.Add(this.lbl_totalDistance);
            this.groupBox_summaryData.Controls.Add(this.lblmaxAltitude);
            this.groupBox_summaryData.Controls.Add(this.lblavgAltitude);
            this.groupBox_summaryData.Controls.Add(this.lblmaxPower);
            this.groupBox_summaryData.Controls.Add(this.lblavgPower);
            this.groupBox_summaryData.Controls.Add(this.lblminHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lblmaxHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lblavgHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lblmaxSpeed);
            this.groupBox_summaryData.Controls.Add(this.lblavgSpeed);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxAltitude);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgAltitude);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxPower);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgPower);
            this.groupBox_summaryData.Controls.Add(this.lbl_minHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxSpeed);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgSpeed);
            this.groupBox_summaryData.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_summaryData.Location = new System.Drawing.Point(654, -1);
            this.groupBox_summaryData.Name = "groupBox_summaryData";
            this.groupBox_summaryData.Size = new System.Drawing.Size(479, 417);
            this.groupBox_summaryData.TabIndex = 20;
            this.groupBox_summaryData.TabStop = false;
            this.groupBox_summaryData.Text = "Summary Data";
            // 
            // Intervals
            // 
            this.Intervals.HeaderText = "Intervals";
            this.Intervals.Name = "Intervals";
            this.Intervals.ReadOnly = true;
            this.Intervals.Width = 108;
            // 
            // Power
            // 
            this.Power.HeaderText = "Power";
            this.Power.Name = "Power";
            this.Power.ReadOnly = true;
            this.Power.Width = 107;
            // 
            // Altitude
            // 
            this.Altitude.HeaderText = "Altitude";
            this.Altitude.Name = "Altitude";
            this.Altitude.ReadOnly = true;
            this.Altitude.Width = 108;
            // 
            // Cadence
            // 
            this.Cadence.HeaderText = "Cadence";
            this.Cadence.Name = "Cadence";
            this.Cadence.ReadOnly = true;
            this.Cadence.Width = 107;
            // 
            // Speed
            // 
            this.Speed.HeaderText = "Speed";
            this.Speed.Name = "Speed";
            this.Speed.ReadOnly = true;
            this.Speed.Width = 107;
            // 
            // HeartRate
            // 
            this.HeartRate.HeaderText = "Heart Rate";
            this.HeartRate.Name = "HeartRate";
            this.HeartRate.ReadOnly = true;
            this.HeartRate.Width = 107;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HeartRate,
            this.Speed,
            this.Cadence,
            this.Altitude,
            this.Power,
            this.Intervals});
            this.dataGridView.Location = new System.Drawing.Point(1, -1);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(647, 506);
            this.dataGridView.TabIndex = 22;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Intervals";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Power";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Heart Rate";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Speed";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Cadence";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Altitude";
            this.Column6.Name = "Column6";
            // 
            // IntervalDetection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(1134, 673);
            this.Controls.Add(this.txtInterval);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.btnPreviousInterval);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lbl_interval);
            this.Controls.Add(this.btnNextInterval);
            this.Controls.Add(this.groupBox_summaryData);
            this.Name = "IntervalDetection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IntervalDetection";
            this.Load += new System.EventHandler(this.IntervalDetection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox_summaryData.ResumeLayout(false);
            this.groupBox_summaryData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lbltotalDistance;
        private System.Windows.Forms.Label lbl_totalDistance;
        private System.Windows.Forms.Label lblmaxAltitude;
        private System.Windows.Forms.Label lblavgAltitude;
        private System.Windows.Forms.Label lblmaxPower;
        private System.Windows.Forms.Label lblavgPower;
        private System.Windows.Forms.Label lblminHeartRate;
        private System.Windows.Forms.Label lblmaxHeartRate;
        private System.Windows.Forms.Label lblavgHeartRate;
        private System.Windows.Forms.Label lblmaxSpeed;
        private System.Windows.Forms.Label lblavgSpeed;
        private System.Windows.Forms.Label lbl_maxAltitude;
        private System.Windows.Forms.Label lbl_avgAltitude;
        private System.Windows.Forms.Label lbl_maxPower;
        private System.Windows.Forms.Label lbl_avgPower;
        private System.Windows.Forms.Label lbl_minHeartRate;
        private System.Windows.Forms.Label lbl_maxHeartRate;
        private System.Windows.Forms.Label lbl_avgHeartRate;
        private System.Windows.Forms.Label lbl_maxSpeed;
        private System.Windows.Forms.Label lbl_avgSpeed;
        private System.Windows.Forms.TextBox txtInterval;
        private System.Windows.Forms.Button btnPreviousInterval;
        private System.Windows.Forms.Label lbl_interval;
        private System.Windows.Forms.Button btnNextInterval;
        private System.Windows.Forms.GroupBox groupBox_summaryData;
        private System.Windows.Forms.DataGridViewTextBoxColumn Intervals;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}