﻿namespace DataAnalysisSoftware
{
    partial class SelectedChunkData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_chunk1TotalDistance = new System.Windows.Forms.Label();
            this.grpchunk1Summ = new System.Windows.Forms.GroupBox();
            this.lblchunk1TotalDistance = new System.Windows.Forms.Label();
            this.lblchunk1MaxAltitude = new System.Windows.Forms.Label();
            this.lblchunk1AvgAltitude = new System.Windows.Forms.Label();
            this.lblchunk1MaxPower = new System.Windows.Forms.Label();
            this.lblchunk1AvgPower = new System.Windows.Forms.Label();
            this.lblchunk1MinHeartRate = new System.Windows.Forms.Label();
            this.lblchunk1MaxHeartRate = new System.Windows.Forms.Label();
            this.lblchunk1AvgHeartRate = new System.Windows.Forms.Label();
            this.lblchunk1MaxSpeed = new System.Windows.Forms.Label();
            this.lblchunk1AvgSpeed = new System.Windows.Forms.Label();
            this.lbl_chunk1MaxAltitude = new System.Windows.Forms.Label();
            this.lbl_chunk1AvgAltitude = new System.Windows.Forms.Label();
            this.lbl_chunk1MaxPower = new System.Windows.Forms.Label();
            this.lbl_chunk1AvgPower = new System.Windows.Forms.Label();
            this.lbl_chunk1MinHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk1MaxHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk1AvgHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk1MaxSpeed = new System.Windows.Forms.Label();
            this.lbl_chunk1AvgSpeed = new System.Windows.Forms.Label();
            this.lblchunk2TotalDistance = new System.Windows.Forms.Label();
            this.lbl_chunk2TotalDistance = new System.Windows.Forms.Label();
            this.lblchunk4AvgHeartRate = new System.Windows.Forms.Label();
            this.lblchunk4MaxSpeed = new System.Windows.Forms.Label();
            this.lblchunk4AvgSpeed = new System.Windows.Forms.Label();
            this.lbl_chunk4MaxAltitude = new System.Windows.Forms.Label();
            this.lbl_chunk4AvgAltitude = new System.Windows.Forms.Label();
            this.lbl_chunk4MaxPower = new System.Windows.Forms.Label();
            this.lbl_chunk4AvgPower = new System.Windows.Forms.Label();
            this.lbl_chunk4MinHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk4MaxHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk4AvgHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk4MaxSpeed = new System.Windows.Forms.Label();
            this.lbl_chunk4AvgSpeed = new System.Windows.Forms.Label();
            this.lblchunk2MaxAltitude = new System.Windows.Forms.Label();
            this.lblchunk2AvgAltitude = new System.Windows.Forms.Label();
            this.lblchunk2MaxPower = new System.Windows.Forms.Label();
            this.lblchunk2AvgPower = new System.Windows.Forms.Label();
            this.lblchunk2MinHeartRate = new System.Windows.Forms.Label();
            this.lblchunk2MaxHeartRate = new System.Windows.Forms.Label();
            this.lblchunk2AvgHeartRate = new System.Windows.Forms.Label();
            this.lblchunk2MaxSpeed = new System.Windows.Forms.Label();
            this.lblchunk2AvgSpeed = new System.Windows.Forms.Label();
            this.lbl_chunk2MaxAltitude = new System.Windows.Forms.Label();
            this.lbl_chunk2AvgAltitude = new System.Windows.Forms.Label();
            this.lbl_chunk2MaxPower = new System.Windows.Forms.Label();
            this.lbl_chunk2AvgPower = new System.Windows.Forms.Label();
            this.lbl_chunk2MinHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk2MaxHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk2AvgHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk2MaxSpeed = new System.Windows.Forms.Label();
            this.lbl_chunk2AvgSpeed = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.grpchunk3Summ = new System.Windows.Forms.GroupBox();
            this.lblchunk3TotalDistance = new System.Windows.Forms.Label();
            this.lbl_chunk3TotalDistance = new System.Windows.Forms.Label();
            this.lblchunk3MaxAltitude = new System.Windows.Forms.Label();
            this.lblchunk3AvgAltitude = new System.Windows.Forms.Label();
            this.lblchunk3MaxPower = new System.Windows.Forms.Label();
            this.lblchunk3AvgPower = new System.Windows.Forms.Label();
            this.lblchunk3MinHeartRate = new System.Windows.Forms.Label();
            this.lblchunk3MaxHeartRate = new System.Windows.Forms.Label();
            this.lblchunk3AvgHeartRate = new System.Windows.Forms.Label();
            this.lblchunk3MaxSpeed = new System.Windows.Forms.Label();
            this.lblchunk3AvgSpeed = new System.Windows.Forms.Label();
            this.lbl_chunk3MaxAltitude = new System.Windows.Forms.Label();
            this.lbl_chunk3AvgAltitude = new System.Windows.Forms.Label();
            this.lbl_chunk3MaxPower = new System.Windows.Forms.Label();
            this.lbl_chunk3AvgPower = new System.Windows.Forms.Label();
            this.lbl_chunk3MinHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk3MaxHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk3AvgHeartRate = new System.Windows.Forms.Label();
            this.lbl_chunk3MaxSpeed = new System.Windows.Forms.Label();
            this.lbl_chunk3AvgSpeed = new System.Windows.Forms.Label();
            this.grpchunk2Summ = new System.Windows.Forms.GroupBox();
            this.grpchunk4Summ = new System.Windows.Forms.GroupBox();
            this.lblchunk4TotalDistance = new System.Windows.Forms.Label();
            this.lbl_chunk4TotalDistance = new System.Windows.Forms.Label();
            this.lblchunk4MaxAltitude = new System.Windows.Forms.Label();
            this.lblchunk4AvgAltitude = new System.Windows.Forms.Label();
            this.lblchunk4MaxPower = new System.Windows.Forms.Label();
            this.lblchunk4AvgPower = new System.Windows.Forms.Label();
            this.lblchunk4MinHeartRate = new System.Windows.Forms.Label();
            this.lblchunk4MaxHeartRate = new System.Windows.Forms.Label();
            this.grpchunk1Summ.SuspendLayout();
            this.panel.SuspendLayout();
            this.grpchunk3Summ.SuspendLayout();
            this.grpchunk2Summ.SuspendLayout();
            this.grpchunk4Summ.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_chunk1TotalDistance
            // 
            this.lbl_chunk1TotalDistance.AutoSize = true;
            this.lbl_chunk1TotalDistance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1TotalDistance.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1TotalDistance.Location = new System.Drawing.Point(6, 52);
            this.lbl_chunk1TotalDistance.Name = "lbl_chunk1TotalDistance";
            this.lbl_chunk1TotalDistance.Size = new System.Drawing.Size(149, 17);
            this.lbl_chunk1TotalDistance.TabIndex = 30;
            this.lbl_chunk1TotalDistance.Text = "Total Distance Covered :";
            // 
            // grpchunk1Summ
            // 
            this.grpchunk1Summ.BackColor = System.Drawing.Color.White;
            this.grpchunk1Summ.Controls.Add(this.lblchunk1TotalDistance);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1TotalDistance);
            this.grpchunk1Summ.Controls.Add(this.lblchunk1MaxAltitude);
            this.grpchunk1Summ.Controls.Add(this.lblchunk1AvgAltitude);
            this.grpchunk1Summ.Controls.Add(this.lblchunk1MaxPower);
            this.grpchunk1Summ.Controls.Add(this.lblchunk1AvgPower);
            this.grpchunk1Summ.Controls.Add(this.lblchunk1MinHeartRate);
            this.grpchunk1Summ.Controls.Add(this.lblchunk1MaxHeartRate);
            this.grpchunk1Summ.Controls.Add(this.lblchunk1AvgHeartRate);
            this.grpchunk1Summ.Controls.Add(this.lblchunk1MaxSpeed);
            this.grpchunk1Summ.Controls.Add(this.lblchunk1AvgSpeed);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1MaxAltitude);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1AvgAltitude);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1MaxPower);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1AvgPower);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1MinHeartRate);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1MaxHeartRate);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1AvgHeartRate);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1MaxSpeed);
            this.grpchunk1Summ.Controls.Add(this.lbl_chunk1AvgSpeed);
            this.grpchunk1Summ.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpchunk1Summ.Location = new System.Drawing.Point(7, 14);
            this.grpchunk1Summ.Name = "grpchunk1Summ";
            this.grpchunk1Summ.Size = new System.Drawing.Size(277, 432);
            this.grpchunk1Summ.TabIndex = 6;
            this.grpchunk1Summ.TabStop = false;
            this.grpchunk1Summ.Text = "Chunk 1 Summary Data";
            // 
            // lblchunk1TotalDistance
            // 
            this.lblchunk1TotalDistance.AutoSize = true;
            this.lblchunk1TotalDistance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1TotalDistance.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1TotalDistance.Location = new System.Drawing.Point(167, 51);
            this.lblchunk1TotalDistance.Name = "lblchunk1TotalDistance";
            this.lblchunk1TotalDistance.Size = new System.Drawing.Size(48, 17);
            this.lblchunk1TotalDistance.TabIndex = 31;
            this.lblchunk1TotalDistance.Text = "0 km/h";
            // 
            // lblchunk1MaxAltitude
            // 
            this.lblchunk1MaxAltitude.AutoSize = true;
            this.lblchunk1MaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1MaxAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1MaxAltitude.Location = new System.Drawing.Point(167, 396);
            this.lblchunk1MaxAltitude.Name = "lblchunk1MaxAltitude";
            this.lblchunk1MaxAltitude.Size = new System.Drawing.Size(43, 17);
            this.lblchunk1MaxAltitude.TabIndex = 29;
            this.lblchunk1MaxAltitude.Text = "0 m/ft";
            // 
            // lblchunk1AvgAltitude
            // 
            this.lblchunk1AvgAltitude.AutoSize = true;
            this.lblchunk1AvgAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1AvgAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1AvgAltitude.Location = new System.Drawing.Point(167, 358);
            this.lblchunk1AvgAltitude.Name = "lblchunk1AvgAltitude";
            this.lblchunk1AvgAltitude.Size = new System.Drawing.Size(43, 17);
            this.lblchunk1AvgAltitude.TabIndex = 28;
            this.lblchunk1AvgAltitude.Text = "0 m/ft";
            // 
            // lblchunk1MaxPower
            // 
            this.lblchunk1MaxPower.AutoSize = true;
            this.lblchunk1MaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1MaxPower.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1MaxPower.Location = new System.Drawing.Point(167, 319);
            this.lblchunk1MaxPower.Name = "lblchunk1MaxPower";
            this.lblchunk1MaxPower.Size = new System.Drawing.Size(49, 17);
            this.lblchunk1MaxPower.TabIndex = 27;
            this.lblchunk1MaxPower.Text = "0 watts";
            // 
            // lblchunk1AvgPower
            // 
            this.lblchunk1AvgPower.AutoSize = true;
            this.lblchunk1AvgPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1AvgPower.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1AvgPower.Location = new System.Drawing.Point(167, 281);
            this.lblchunk1AvgPower.Name = "lblchunk1AvgPower";
            this.lblchunk1AvgPower.Size = new System.Drawing.Size(49, 17);
            this.lblchunk1AvgPower.TabIndex = 26;
            this.lblchunk1AvgPower.Text = "0 watts";
            // 
            // lblchunk1MinHeartRate
            // 
            this.lblchunk1MinHeartRate.AutoSize = true;
            this.lblchunk1MinHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1MinHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1MinHeartRate.Location = new System.Drawing.Point(167, 242);
            this.lblchunk1MinHeartRate.Name = "lblchunk1MinHeartRate";
            this.lblchunk1MinHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk1MinHeartRate.TabIndex = 25;
            this.lblchunk1MinHeartRate.Text = "0 bpm";
            // 
            // lblchunk1MaxHeartRate
            // 
            this.lblchunk1MaxHeartRate.AutoSize = true;
            this.lblchunk1MaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1MaxHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1MaxHeartRate.Location = new System.Drawing.Point(167, 203);
            this.lblchunk1MaxHeartRate.Name = "lblchunk1MaxHeartRate";
            this.lblchunk1MaxHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk1MaxHeartRate.TabIndex = 24;
            this.lblchunk1MaxHeartRate.Text = "0 bpm";
            // 
            // lblchunk1AvgHeartRate
            // 
            this.lblchunk1AvgHeartRate.AutoSize = true;
            this.lblchunk1AvgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1AvgHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1AvgHeartRate.Location = new System.Drawing.Point(167, 162);
            this.lblchunk1AvgHeartRate.Name = "lblchunk1AvgHeartRate";
            this.lblchunk1AvgHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk1AvgHeartRate.TabIndex = 23;
            this.lblchunk1AvgHeartRate.Text = "0 bpm";
            // 
            // lblchunk1MaxSpeed
            // 
            this.lblchunk1MaxSpeed.AutoSize = true;
            this.lblchunk1MaxSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1MaxSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1MaxSpeed.Location = new System.Drawing.Point(167, 124);
            this.lblchunk1MaxSpeed.Name = "lblchunk1MaxSpeed";
            this.lblchunk1MaxSpeed.Size = new System.Drawing.Size(48, 17);
            this.lblchunk1MaxSpeed.TabIndex = 22;
            this.lblchunk1MaxSpeed.Text = "0 km/h";
            // 
            // lblchunk1AvgSpeed
            // 
            this.lblchunk1AvgSpeed.AutoSize = true;
            this.lblchunk1AvgSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk1AvgSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblchunk1AvgSpeed.Location = new System.Drawing.Point(167, 86);
            this.lblchunk1AvgSpeed.Name = "lblchunk1AvgSpeed";
            this.lblchunk1AvgSpeed.Size = new System.Drawing.Size(48, 17);
            this.lblchunk1AvgSpeed.TabIndex = 21;
            this.lblchunk1AvgSpeed.Text = "0 km/h";
            // 
            // lbl_chunk1MaxAltitude
            // 
            this.lbl_chunk1MaxAltitude.AutoSize = true;
            this.lbl_chunk1MaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1MaxAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1MaxAltitude.Location = new System.Drawing.Point(6, 397);
            this.lbl_chunk1MaxAltitude.Name = "lbl_chunk1MaxAltitude";
            this.lbl_chunk1MaxAltitude.Size = new System.Drawing.Size(120, 17);
            this.lbl_chunk1MaxAltitude.TabIndex = 20;
            this.lbl_chunk1MaxAltitude.Text = "Maximum Altitude :";
            // 
            // lbl_chunk1AvgAltitude
            // 
            this.lbl_chunk1AvgAltitude.AutoSize = true;
            this.lbl_chunk1AvgAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1AvgAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1AvgAltitude.Location = new System.Drawing.Point(6, 359);
            this.lbl_chunk1AvgAltitude.Name = "lbl_chunk1AvgAltitude";
            this.lbl_chunk1AvgAltitude.Size = new System.Drawing.Size(111, 17);
            this.lbl_chunk1AvgAltitude.TabIndex = 19;
            this.lbl_chunk1AvgAltitude.Text = "Average Altitude :";
            // 
            // lbl_chunk1MaxPower
            // 
            this.lbl_chunk1MaxPower.AutoSize = true;
            this.lbl_chunk1MaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1MaxPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1MaxPower.Location = new System.Drawing.Point(6, 320);
            this.lbl_chunk1MaxPower.Name = "lbl_chunk1MaxPower";
            this.lbl_chunk1MaxPower.Size = new System.Drawing.Size(112, 17);
            this.lbl_chunk1MaxPower.TabIndex = 18;
            this.lbl_chunk1MaxPower.Text = "Maximum Power :";
            // 
            // lbl_chunk1AvgPower
            // 
            this.lbl_chunk1AvgPower.AutoSize = true;
            this.lbl_chunk1AvgPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1AvgPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1AvgPower.Location = new System.Drawing.Point(6, 282);
            this.lbl_chunk1AvgPower.Name = "lbl_chunk1AvgPower";
            this.lbl_chunk1AvgPower.Size = new System.Drawing.Size(103, 17);
            this.lbl_chunk1AvgPower.TabIndex = 17;
            this.lbl_chunk1AvgPower.Text = "Average Power :";
            // 
            // lbl_chunk1MinHeartRate
            // 
            this.lbl_chunk1MinHeartRate.AutoSize = true;
            this.lbl_chunk1MinHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1MinHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1MinHeartRate.Location = new System.Drawing.Point(6, 243);
            this.lbl_chunk1MinHeartRate.Name = "lbl_chunk1MinHeartRate";
            this.lbl_chunk1MinHeartRate.Size = new System.Drawing.Size(135, 17);
            this.lbl_chunk1MinHeartRate.TabIndex = 16;
            this.lbl_chunk1MinHeartRate.Text = "Minimum Heart Rate :";
            // 
            // lbl_chunk1MaxHeartRate
            // 
            this.lbl_chunk1MaxHeartRate.AutoSize = true;
            this.lbl_chunk1MaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1MaxHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1MaxHeartRate.Location = new System.Drawing.Point(6, 204);
            this.lbl_chunk1MaxHeartRate.Name = "lbl_chunk1MaxHeartRate";
            this.lbl_chunk1MaxHeartRate.Size = new System.Drawing.Size(138, 17);
            this.lbl_chunk1MaxHeartRate.TabIndex = 15;
            this.lbl_chunk1MaxHeartRate.Text = "Maximum Heart Rate :";
            // 
            // lbl_chunk1AvgHeartRate
            // 
            this.lbl_chunk1AvgHeartRate.AutoSize = true;
            this.lbl_chunk1AvgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1AvgHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1AvgHeartRate.Location = new System.Drawing.Point(6, 163);
            this.lbl_chunk1AvgHeartRate.Name = "lbl_chunk1AvgHeartRate";
            this.lbl_chunk1AvgHeartRate.Size = new System.Drawing.Size(129, 17);
            this.lbl_chunk1AvgHeartRate.TabIndex = 14;
            this.lbl_chunk1AvgHeartRate.Text = "Average Heart Rate :";
            // 
            // lbl_chunk1MaxSpeed
            // 
            this.lbl_chunk1MaxSpeed.AutoSize = true;
            this.lbl_chunk1MaxSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1MaxSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1MaxSpeed.Location = new System.Drawing.Point(6, 125);
            this.lbl_chunk1MaxSpeed.Name = "lbl_chunk1MaxSpeed";
            this.lbl_chunk1MaxSpeed.Size = new System.Drawing.Size(113, 17);
            this.lbl_chunk1MaxSpeed.TabIndex = 13;
            this.lbl_chunk1MaxSpeed.Text = "Maximum Speed :";
            // 
            // lbl_chunk1AvgSpeed
            // 
            this.lbl_chunk1AvgSpeed.AutoSize = true;
            this.lbl_chunk1AvgSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk1AvgSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk1AvgSpeed.Location = new System.Drawing.Point(6, 87);
            this.lbl_chunk1AvgSpeed.Name = "lbl_chunk1AvgSpeed";
            this.lbl_chunk1AvgSpeed.Size = new System.Drawing.Size(104, 17);
            this.lbl_chunk1AvgSpeed.TabIndex = 12;
            this.lbl_chunk1AvgSpeed.Text = "Average Speed :";
            // 
            // lblchunk2TotalDistance
            // 
            this.lblchunk2TotalDistance.AutoSize = true;
            this.lblchunk2TotalDistance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2TotalDistance.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2TotalDistance.Location = new System.Drawing.Point(168, 51);
            this.lblchunk2TotalDistance.Name = "lblchunk2TotalDistance";
            this.lblchunk2TotalDistance.Size = new System.Drawing.Size(48, 17);
            this.lblchunk2TotalDistance.TabIndex = 31;
            this.lblchunk2TotalDistance.Text = "0 km/h";
            // 
            // lbl_chunk2TotalDistance
            // 
            this.lbl_chunk2TotalDistance.AutoSize = true;
            this.lbl_chunk2TotalDistance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2TotalDistance.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2TotalDistance.Location = new System.Drawing.Point(6, 52);
            this.lbl_chunk2TotalDistance.Name = "lbl_chunk2TotalDistance";
            this.lbl_chunk2TotalDistance.Size = new System.Drawing.Size(149, 17);
            this.lbl_chunk2TotalDistance.TabIndex = 30;
            this.lbl_chunk2TotalDistance.Text = "Total Distance Covered :";
            // 
            // lblchunk4AvgHeartRate
            // 
            this.lblchunk4AvgHeartRate.AutoSize = true;
            this.lblchunk4AvgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4AvgHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4AvgHeartRate.Location = new System.Drawing.Point(187, 163);
            this.lblchunk4AvgHeartRate.Name = "lblchunk4AvgHeartRate";
            this.lblchunk4AvgHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk4AvgHeartRate.TabIndex = 23;
            this.lblchunk4AvgHeartRate.Text = "0 bpm";
            // 
            // lblchunk4MaxSpeed
            // 
            this.lblchunk4MaxSpeed.AutoSize = true;
            this.lblchunk4MaxSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4MaxSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4MaxSpeed.Location = new System.Drawing.Point(187, 125);
            this.lblchunk4MaxSpeed.Name = "lblchunk4MaxSpeed";
            this.lblchunk4MaxSpeed.Size = new System.Drawing.Size(48, 17);
            this.lblchunk4MaxSpeed.TabIndex = 22;
            this.lblchunk4MaxSpeed.Text = "0 km/h";
            // 
            // lblchunk4AvgSpeed
            // 
            this.lblchunk4AvgSpeed.AutoSize = true;
            this.lblchunk4AvgSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4AvgSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4AvgSpeed.Location = new System.Drawing.Point(187, 87);
            this.lblchunk4AvgSpeed.Name = "lblchunk4AvgSpeed";
            this.lblchunk4AvgSpeed.Size = new System.Drawing.Size(48, 17);
            this.lblchunk4AvgSpeed.TabIndex = 21;
            this.lblchunk4AvgSpeed.Text = "0 km/h";
            // 
            // lbl_chunk4MaxAltitude
            // 
            this.lbl_chunk4MaxAltitude.AutoSize = true;
            this.lbl_chunk4MaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4MaxAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4MaxAltitude.Location = new System.Drawing.Point(6, 397);
            this.lbl_chunk4MaxAltitude.Name = "lbl_chunk4MaxAltitude";
            this.lbl_chunk4MaxAltitude.Size = new System.Drawing.Size(120, 17);
            this.lbl_chunk4MaxAltitude.TabIndex = 20;
            this.lbl_chunk4MaxAltitude.Text = "Maximum Altitude :";
            // 
            // lbl_chunk4AvgAltitude
            // 
            this.lbl_chunk4AvgAltitude.AutoSize = true;
            this.lbl_chunk4AvgAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4AvgAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4AvgAltitude.Location = new System.Drawing.Point(6, 359);
            this.lbl_chunk4AvgAltitude.Name = "lbl_chunk4AvgAltitude";
            this.lbl_chunk4AvgAltitude.Size = new System.Drawing.Size(111, 17);
            this.lbl_chunk4AvgAltitude.TabIndex = 19;
            this.lbl_chunk4AvgAltitude.Text = "Average Altitude :";
            // 
            // lbl_chunk4MaxPower
            // 
            this.lbl_chunk4MaxPower.AutoSize = true;
            this.lbl_chunk4MaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4MaxPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4MaxPower.Location = new System.Drawing.Point(6, 320);
            this.lbl_chunk4MaxPower.Name = "lbl_chunk4MaxPower";
            this.lbl_chunk4MaxPower.Size = new System.Drawing.Size(112, 17);
            this.lbl_chunk4MaxPower.TabIndex = 18;
            this.lbl_chunk4MaxPower.Text = "Maximum Power :";
            // 
            // lbl_chunk4AvgPower
            // 
            this.lbl_chunk4AvgPower.AutoSize = true;
            this.lbl_chunk4AvgPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4AvgPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4AvgPower.Location = new System.Drawing.Point(6, 282);
            this.lbl_chunk4AvgPower.Name = "lbl_chunk4AvgPower";
            this.lbl_chunk4AvgPower.Size = new System.Drawing.Size(103, 17);
            this.lbl_chunk4AvgPower.TabIndex = 17;
            this.lbl_chunk4AvgPower.Text = "Average Power :";
            // 
            // lbl_chunk4MinHeartRate
            // 
            this.lbl_chunk4MinHeartRate.AutoSize = true;
            this.lbl_chunk4MinHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4MinHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4MinHeartRate.Location = new System.Drawing.Point(6, 243);
            this.lbl_chunk4MinHeartRate.Name = "lbl_chunk4MinHeartRate";
            this.lbl_chunk4MinHeartRate.Size = new System.Drawing.Size(135, 17);
            this.lbl_chunk4MinHeartRate.TabIndex = 16;
            this.lbl_chunk4MinHeartRate.Text = "Minimum Heart Rate :";
            // 
            // lbl_chunk4MaxHeartRate
            // 
            this.lbl_chunk4MaxHeartRate.AutoSize = true;
            this.lbl_chunk4MaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4MaxHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4MaxHeartRate.Location = new System.Drawing.Point(6, 204);
            this.lbl_chunk4MaxHeartRate.Name = "lbl_chunk4MaxHeartRate";
            this.lbl_chunk4MaxHeartRate.Size = new System.Drawing.Size(138, 17);
            this.lbl_chunk4MaxHeartRate.TabIndex = 15;
            this.lbl_chunk4MaxHeartRate.Text = "Maximum Heart Rate :";
            // 
            // lbl_chunk4AvgHeartRate
            // 
            this.lbl_chunk4AvgHeartRate.AutoSize = true;
            this.lbl_chunk4AvgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4AvgHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4AvgHeartRate.Location = new System.Drawing.Point(6, 163);
            this.lbl_chunk4AvgHeartRate.Name = "lbl_chunk4AvgHeartRate";
            this.lbl_chunk4AvgHeartRate.Size = new System.Drawing.Size(129, 17);
            this.lbl_chunk4AvgHeartRate.TabIndex = 14;
            this.lbl_chunk4AvgHeartRate.Text = "Average Heart Rate :";
            // 
            // lbl_chunk4MaxSpeed
            // 
            this.lbl_chunk4MaxSpeed.AutoSize = true;
            this.lbl_chunk4MaxSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4MaxSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4MaxSpeed.Location = new System.Drawing.Point(6, 125);
            this.lbl_chunk4MaxSpeed.Name = "lbl_chunk4MaxSpeed";
            this.lbl_chunk4MaxSpeed.Size = new System.Drawing.Size(113, 17);
            this.lbl_chunk4MaxSpeed.TabIndex = 13;
            this.lbl_chunk4MaxSpeed.Text = "Maximum Speed :";
            // 
            // lbl_chunk4AvgSpeed
            // 
            this.lbl_chunk4AvgSpeed.AutoSize = true;
            this.lbl_chunk4AvgSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4AvgSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4AvgSpeed.Location = new System.Drawing.Point(6, 87);
            this.lbl_chunk4AvgSpeed.Name = "lbl_chunk4AvgSpeed";
            this.lbl_chunk4AvgSpeed.Size = new System.Drawing.Size(104, 17);
            this.lbl_chunk4AvgSpeed.TabIndex = 12;
            this.lbl_chunk4AvgSpeed.Text = "Average Speed :";
            // 
            // lblchunk2MaxAltitude
            // 
            this.lblchunk2MaxAltitude.AutoSize = true;
            this.lblchunk2MaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2MaxAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2MaxAltitude.Location = new System.Drawing.Point(168, 397);
            this.lblchunk2MaxAltitude.Name = "lblchunk2MaxAltitude";
            this.lblchunk2MaxAltitude.Size = new System.Drawing.Size(43, 17);
            this.lblchunk2MaxAltitude.TabIndex = 29;
            this.lblchunk2MaxAltitude.Text = "0 m/ft";
            // 
            // lblchunk2AvgAltitude
            // 
            this.lblchunk2AvgAltitude.AutoSize = true;
            this.lblchunk2AvgAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2AvgAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2AvgAltitude.Location = new System.Drawing.Point(168, 359);
            this.lblchunk2AvgAltitude.Name = "lblchunk2AvgAltitude";
            this.lblchunk2AvgAltitude.Size = new System.Drawing.Size(43, 17);
            this.lblchunk2AvgAltitude.TabIndex = 28;
            this.lblchunk2AvgAltitude.Text = "0 m/ft";
            // 
            // lblchunk2MaxPower
            // 
            this.lblchunk2MaxPower.AutoSize = true;
            this.lblchunk2MaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2MaxPower.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2MaxPower.Location = new System.Drawing.Point(168, 320);
            this.lblchunk2MaxPower.Name = "lblchunk2MaxPower";
            this.lblchunk2MaxPower.Size = new System.Drawing.Size(49, 17);
            this.lblchunk2MaxPower.TabIndex = 27;
            this.lblchunk2MaxPower.Text = "0 watts";
            // 
            // lblchunk2AvgPower
            // 
            this.lblchunk2AvgPower.AutoSize = true;
            this.lblchunk2AvgPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2AvgPower.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2AvgPower.Location = new System.Drawing.Point(168, 282);
            this.lblchunk2AvgPower.Name = "lblchunk2AvgPower";
            this.lblchunk2AvgPower.Size = new System.Drawing.Size(49, 17);
            this.lblchunk2AvgPower.TabIndex = 26;
            this.lblchunk2AvgPower.Text = "0 watts";
            // 
            // lblchunk2MinHeartRate
            // 
            this.lblchunk2MinHeartRate.AutoSize = true;
            this.lblchunk2MinHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2MinHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2MinHeartRate.Location = new System.Drawing.Point(168, 243);
            this.lblchunk2MinHeartRate.Name = "lblchunk2MinHeartRate";
            this.lblchunk2MinHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk2MinHeartRate.TabIndex = 25;
            this.lblchunk2MinHeartRate.Text = "0 bpm";
            // 
            // lblchunk2MaxHeartRate
            // 
            this.lblchunk2MaxHeartRate.AutoSize = true;
            this.lblchunk2MaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2MaxHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2MaxHeartRate.Location = new System.Drawing.Point(168, 203);
            this.lblchunk2MaxHeartRate.Name = "lblchunk2MaxHeartRate";
            this.lblchunk2MaxHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk2MaxHeartRate.TabIndex = 24;
            this.lblchunk2MaxHeartRate.Text = "0 bpm";
            // 
            // lblchunk2AvgHeartRate
            // 
            this.lblchunk2AvgHeartRate.AutoSize = true;
            this.lblchunk2AvgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2AvgHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2AvgHeartRate.Location = new System.Drawing.Point(168, 162);
            this.lblchunk2AvgHeartRate.Name = "lblchunk2AvgHeartRate";
            this.lblchunk2AvgHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk2AvgHeartRate.TabIndex = 23;
            this.lblchunk2AvgHeartRate.Text = "0 bpm";
            // 
            // lblchunk2MaxSpeed
            // 
            this.lblchunk2MaxSpeed.AutoSize = true;
            this.lblchunk2MaxSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2MaxSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2MaxSpeed.Location = new System.Drawing.Point(168, 124);
            this.lblchunk2MaxSpeed.Name = "lblchunk2MaxSpeed";
            this.lblchunk2MaxSpeed.Size = new System.Drawing.Size(48, 17);
            this.lblchunk2MaxSpeed.TabIndex = 22;
            this.lblchunk2MaxSpeed.Text = "0 km/h";
            // 
            // lblchunk2AvgSpeed
            // 
            this.lblchunk2AvgSpeed.AutoSize = true;
            this.lblchunk2AvgSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk2AvgSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblchunk2AvgSpeed.Location = new System.Drawing.Point(168, 86);
            this.lblchunk2AvgSpeed.Name = "lblchunk2AvgSpeed";
            this.lblchunk2AvgSpeed.Size = new System.Drawing.Size(48, 17);
            this.lblchunk2AvgSpeed.TabIndex = 21;
            this.lblchunk2AvgSpeed.Text = "0 km/h";
            // 
            // lbl_chunk2MaxAltitude
            // 
            this.lbl_chunk2MaxAltitude.AutoSize = true;
            this.lbl_chunk2MaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2MaxAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2MaxAltitude.Location = new System.Drawing.Point(6, 398);
            this.lbl_chunk2MaxAltitude.Name = "lbl_chunk2MaxAltitude";
            this.lbl_chunk2MaxAltitude.Size = new System.Drawing.Size(120, 17);
            this.lbl_chunk2MaxAltitude.TabIndex = 20;
            this.lbl_chunk2MaxAltitude.Text = "Maximum Altitude :";
            // 
            // lbl_chunk2AvgAltitude
            // 
            this.lbl_chunk2AvgAltitude.AutoSize = true;
            this.lbl_chunk2AvgAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2AvgAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2AvgAltitude.Location = new System.Drawing.Point(6, 360);
            this.lbl_chunk2AvgAltitude.Name = "lbl_chunk2AvgAltitude";
            this.lbl_chunk2AvgAltitude.Size = new System.Drawing.Size(111, 17);
            this.lbl_chunk2AvgAltitude.TabIndex = 19;
            this.lbl_chunk2AvgAltitude.Text = "Average Altitude :";
            // 
            // lbl_chunk2MaxPower
            // 
            this.lbl_chunk2MaxPower.AutoSize = true;
            this.lbl_chunk2MaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2MaxPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2MaxPower.Location = new System.Drawing.Point(6, 321);
            this.lbl_chunk2MaxPower.Name = "lbl_chunk2MaxPower";
            this.lbl_chunk2MaxPower.Size = new System.Drawing.Size(112, 17);
            this.lbl_chunk2MaxPower.TabIndex = 18;
            this.lbl_chunk2MaxPower.Text = "Maximum Power :";
            // 
            // lbl_chunk2AvgPower
            // 
            this.lbl_chunk2AvgPower.AutoSize = true;
            this.lbl_chunk2AvgPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2AvgPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2AvgPower.Location = new System.Drawing.Point(6, 283);
            this.lbl_chunk2AvgPower.Name = "lbl_chunk2AvgPower";
            this.lbl_chunk2AvgPower.Size = new System.Drawing.Size(103, 17);
            this.lbl_chunk2AvgPower.TabIndex = 17;
            this.lbl_chunk2AvgPower.Text = "Average Power :";
            // 
            // lbl_chunk2MinHeartRate
            // 
            this.lbl_chunk2MinHeartRate.AutoSize = true;
            this.lbl_chunk2MinHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2MinHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2MinHeartRate.Location = new System.Drawing.Point(6, 244);
            this.lbl_chunk2MinHeartRate.Name = "lbl_chunk2MinHeartRate";
            this.lbl_chunk2MinHeartRate.Size = new System.Drawing.Size(135, 17);
            this.lbl_chunk2MinHeartRate.TabIndex = 16;
            this.lbl_chunk2MinHeartRate.Text = "Minimum Heart Rate :";
            // 
            // lbl_chunk2MaxHeartRate
            // 
            this.lbl_chunk2MaxHeartRate.AutoSize = true;
            this.lbl_chunk2MaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2MaxHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2MaxHeartRate.Location = new System.Drawing.Point(6, 204);
            this.lbl_chunk2MaxHeartRate.Name = "lbl_chunk2MaxHeartRate";
            this.lbl_chunk2MaxHeartRate.Size = new System.Drawing.Size(138, 17);
            this.lbl_chunk2MaxHeartRate.TabIndex = 15;
            this.lbl_chunk2MaxHeartRate.Text = "Maximum Heart Rate :";
            // 
            // lbl_chunk2AvgHeartRate
            // 
            this.lbl_chunk2AvgHeartRate.AutoSize = true;
            this.lbl_chunk2AvgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2AvgHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2AvgHeartRate.Location = new System.Drawing.Point(6, 163);
            this.lbl_chunk2AvgHeartRate.Name = "lbl_chunk2AvgHeartRate";
            this.lbl_chunk2AvgHeartRate.Size = new System.Drawing.Size(129, 17);
            this.lbl_chunk2AvgHeartRate.TabIndex = 14;
            this.lbl_chunk2AvgHeartRate.Text = "Average Heart Rate :";
            // 
            // lbl_chunk2MaxSpeed
            // 
            this.lbl_chunk2MaxSpeed.AutoSize = true;
            this.lbl_chunk2MaxSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2MaxSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2MaxSpeed.Location = new System.Drawing.Point(6, 125);
            this.lbl_chunk2MaxSpeed.Name = "lbl_chunk2MaxSpeed";
            this.lbl_chunk2MaxSpeed.Size = new System.Drawing.Size(113, 17);
            this.lbl_chunk2MaxSpeed.TabIndex = 13;
            this.lbl_chunk2MaxSpeed.Text = "Maximum Speed :";
            // 
            // lbl_chunk2AvgSpeed
            // 
            this.lbl_chunk2AvgSpeed.AutoSize = true;
            this.lbl_chunk2AvgSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk2AvgSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk2AvgSpeed.Location = new System.Drawing.Point(6, 87);
            this.lbl_chunk2AvgSpeed.Name = "lbl_chunk2AvgSpeed";
            this.lbl_chunk2AvgSpeed.Size = new System.Drawing.Size(104, 17);
            this.lbl_chunk2AvgSpeed.TabIndex = 12;
            this.lbl_chunk2AvgSpeed.Text = "Average Speed :";
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.LightCyan;
            this.panel.Controls.Add(this.grpchunk3Summ);
            this.panel.Controls.Add(this.grpchunk2Summ);
            this.panel.Controls.Add(this.grpchunk4Summ);
            this.panel.Controls.Add(this.grpchunk1Summ);
            this.panel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel.Location = new System.Drawing.Point(0, -1);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1145, 451);
            this.panel.TabIndex = 1;
            // 
            // grpchunk3Summ
            // 
            this.grpchunk3Summ.BackColor = System.Drawing.Color.White;
            this.grpchunk3Summ.Controls.Add(this.lblchunk3TotalDistance);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3TotalDistance);
            this.grpchunk3Summ.Controls.Add(this.lblchunk3MaxAltitude);
            this.grpchunk3Summ.Controls.Add(this.lblchunk3AvgAltitude);
            this.grpchunk3Summ.Controls.Add(this.lblchunk3MaxPower);
            this.grpchunk3Summ.Controls.Add(this.lblchunk3AvgPower);
            this.grpchunk3Summ.Controls.Add(this.lblchunk3MinHeartRate);
            this.grpchunk3Summ.Controls.Add(this.lblchunk3MaxHeartRate);
            this.grpchunk3Summ.Controls.Add(this.lblchunk3AvgHeartRate);
            this.grpchunk3Summ.Controls.Add(this.lblchunk3MaxSpeed);
            this.grpchunk3Summ.Controls.Add(this.lblchunk3AvgSpeed);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3MaxAltitude);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3AvgAltitude);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3MaxPower);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3AvgPower);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3MinHeartRate);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3MaxHeartRate);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3AvgHeartRate);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3MaxSpeed);
            this.grpchunk3Summ.Controls.Add(this.lbl_chunk3AvgSpeed);
            this.grpchunk3Summ.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpchunk3Summ.Location = new System.Drawing.Point(574, 14);
            this.grpchunk3Summ.Name = "grpchunk3Summ";
            this.grpchunk3Summ.Size = new System.Drawing.Size(277, 432);
            this.grpchunk3Summ.TabIndex = 33;
            this.grpchunk3Summ.TabStop = false;
            this.grpchunk3Summ.Text = "Chunk 3 Summary Data";
            // 
            // lblchunk3TotalDistance
            // 
            this.lblchunk3TotalDistance.AutoSize = true;
            this.lblchunk3TotalDistance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3TotalDistance.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3TotalDistance.Location = new System.Drawing.Point(164, 51);
            this.lblchunk3TotalDistance.Name = "lblchunk3TotalDistance";
            this.lblchunk3TotalDistance.Size = new System.Drawing.Size(48, 17);
            this.lblchunk3TotalDistance.TabIndex = 31;
            this.lblchunk3TotalDistance.Text = "0 km/h";
            // 
            // lbl_chunk3TotalDistance
            // 
            this.lbl_chunk3TotalDistance.AutoSize = true;
            this.lbl_chunk3TotalDistance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3TotalDistance.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3TotalDistance.Location = new System.Drawing.Point(6, 52);
            this.lbl_chunk3TotalDistance.Name = "lbl_chunk3TotalDistance";
            this.lbl_chunk3TotalDistance.Size = new System.Drawing.Size(149, 17);
            this.lbl_chunk3TotalDistance.TabIndex = 30;
            this.lbl_chunk3TotalDistance.Text = "Total Distance Covered :";
            // 
            // lblchunk3MaxAltitude
            // 
            this.lblchunk3MaxAltitude.AutoSize = true;
            this.lblchunk3MaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3MaxAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3MaxAltitude.Location = new System.Drawing.Point(168, 396);
            this.lblchunk3MaxAltitude.Name = "lblchunk3MaxAltitude";
            this.lblchunk3MaxAltitude.Size = new System.Drawing.Size(43, 17);
            this.lblchunk3MaxAltitude.TabIndex = 29;
            this.lblchunk3MaxAltitude.Text = "0 m/ft";
            // 
            // lblchunk3AvgAltitude
            // 
            this.lblchunk3AvgAltitude.AutoSize = true;
            this.lblchunk3AvgAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3AvgAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3AvgAltitude.Location = new System.Drawing.Point(168, 358);
            this.lblchunk3AvgAltitude.Name = "lblchunk3AvgAltitude";
            this.lblchunk3AvgAltitude.Size = new System.Drawing.Size(43, 17);
            this.lblchunk3AvgAltitude.TabIndex = 28;
            this.lblchunk3AvgAltitude.Text = "0 m/ft";
            // 
            // lblchunk3MaxPower
            // 
            this.lblchunk3MaxPower.AutoSize = true;
            this.lblchunk3MaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3MaxPower.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3MaxPower.Location = new System.Drawing.Point(168, 319);
            this.lblchunk3MaxPower.Name = "lblchunk3MaxPower";
            this.lblchunk3MaxPower.Size = new System.Drawing.Size(49, 17);
            this.lblchunk3MaxPower.TabIndex = 27;
            this.lblchunk3MaxPower.Text = "0 watts";
            // 
            // lblchunk3AvgPower
            // 
            this.lblchunk3AvgPower.AutoSize = true;
            this.lblchunk3AvgPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3AvgPower.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3AvgPower.Location = new System.Drawing.Point(168, 281);
            this.lblchunk3AvgPower.Name = "lblchunk3AvgPower";
            this.lblchunk3AvgPower.Size = new System.Drawing.Size(49, 17);
            this.lblchunk3AvgPower.TabIndex = 26;
            this.lblchunk3AvgPower.Text = "0 watts";
            // 
            // lblchunk3MinHeartRate
            // 
            this.lblchunk3MinHeartRate.AutoSize = true;
            this.lblchunk3MinHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3MinHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3MinHeartRate.Location = new System.Drawing.Point(168, 242);
            this.lblchunk3MinHeartRate.Name = "lblchunk3MinHeartRate";
            this.lblchunk3MinHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk3MinHeartRate.TabIndex = 25;
            this.lblchunk3MinHeartRate.Text = "0 bpm";
            // 
            // lblchunk3MaxHeartRate
            // 
            this.lblchunk3MaxHeartRate.AutoSize = true;
            this.lblchunk3MaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3MaxHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3MaxHeartRate.Location = new System.Drawing.Point(164, 203);
            this.lblchunk3MaxHeartRate.Name = "lblchunk3MaxHeartRate";
            this.lblchunk3MaxHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk3MaxHeartRate.TabIndex = 24;
            this.lblchunk3MaxHeartRate.Text = "0 bpm";
            // 
            // lblchunk3AvgHeartRate
            // 
            this.lblchunk3AvgHeartRate.AutoSize = true;
            this.lblchunk3AvgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3AvgHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3AvgHeartRate.Location = new System.Drawing.Point(164, 162);
            this.lblchunk3AvgHeartRate.Name = "lblchunk3AvgHeartRate";
            this.lblchunk3AvgHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk3AvgHeartRate.TabIndex = 23;
            this.lblchunk3AvgHeartRate.Text = "0 bpm";
            // 
            // lblchunk3MaxSpeed
            // 
            this.lblchunk3MaxSpeed.AutoSize = true;
            this.lblchunk3MaxSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3MaxSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3MaxSpeed.Location = new System.Drawing.Point(164, 124);
            this.lblchunk3MaxSpeed.Name = "lblchunk3MaxSpeed";
            this.lblchunk3MaxSpeed.Size = new System.Drawing.Size(48, 17);
            this.lblchunk3MaxSpeed.TabIndex = 22;
            this.lblchunk3MaxSpeed.Text = "0 km/h";
            // 
            // lblchunk3AvgSpeed
            // 
            this.lblchunk3AvgSpeed.AutoSize = true;
            this.lblchunk3AvgSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk3AvgSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblchunk3AvgSpeed.Location = new System.Drawing.Point(164, 86);
            this.lblchunk3AvgSpeed.Name = "lblchunk3AvgSpeed";
            this.lblchunk3AvgSpeed.Size = new System.Drawing.Size(48, 17);
            this.lblchunk3AvgSpeed.TabIndex = 21;
            this.lblchunk3AvgSpeed.Text = "0 km/h";
            // 
            // lbl_chunk3MaxAltitude
            // 
            this.lbl_chunk3MaxAltitude.AutoSize = true;
            this.lbl_chunk3MaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3MaxAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3MaxAltitude.Location = new System.Drawing.Point(10, 397);
            this.lbl_chunk3MaxAltitude.Name = "lbl_chunk3MaxAltitude";
            this.lbl_chunk3MaxAltitude.Size = new System.Drawing.Size(120, 17);
            this.lbl_chunk3MaxAltitude.TabIndex = 20;
            this.lbl_chunk3MaxAltitude.Text = "Maximum Altitude :";
            // 
            // lbl_chunk3AvgAltitude
            // 
            this.lbl_chunk3AvgAltitude.AutoSize = true;
            this.lbl_chunk3AvgAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3AvgAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3AvgAltitude.Location = new System.Drawing.Point(10, 359);
            this.lbl_chunk3AvgAltitude.Name = "lbl_chunk3AvgAltitude";
            this.lbl_chunk3AvgAltitude.Size = new System.Drawing.Size(111, 17);
            this.lbl_chunk3AvgAltitude.TabIndex = 19;
            this.lbl_chunk3AvgAltitude.Text = "Average Altitude :";
            // 
            // lbl_chunk3MaxPower
            // 
            this.lbl_chunk3MaxPower.AutoSize = true;
            this.lbl_chunk3MaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3MaxPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3MaxPower.Location = new System.Drawing.Point(10, 320);
            this.lbl_chunk3MaxPower.Name = "lbl_chunk3MaxPower";
            this.lbl_chunk3MaxPower.Size = new System.Drawing.Size(112, 17);
            this.lbl_chunk3MaxPower.TabIndex = 18;
            this.lbl_chunk3MaxPower.Text = "Maximum Power :";
            // 
            // lbl_chunk3AvgPower
            // 
            this.lbl_chunk3AvgPower.AutoSize = true;
            this.lbl_chunk3AvgPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3AvgPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3AvgPower.Location = new System.Drawing.Point(10, 282);
            this.lbl_chunk3AvgPower.Name = "lbl_chunk3AvgPower";
            this.lbl_chunk3AvgPower.Size = new System.Drawing.Size(103, 17);
            this.lbl_chunk3AvgPower.TabIndex = 17;
            this.lbl_chunk3AvgPower.Text = "Average Power :";
            // 
            // lbl_chunk3MinHeartRate
            // 
            this.lbl_chunk3MinHeartRate.AutoSize = true;
            this.lbl_chunk3MinHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3MinHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3MinHeartRate.Location = new System.Drawing.Point(10, 243);
            this.lbl_chunk3MinHeartRate.Name = "lbl_chunk3MinHeartRate";
            this.lbl_chunk3MinHeartRate.Size = new System.Drawing.Size(135, 17);
            this.lbl_chunk3MinHeartRate.TabIndex = 16;
            this.lbl_chunk3MinHeartRate.Text = "Minimum Heart Rate :";
            // 
            // lbl_chunk3MaxHeartRate
            // 
            this.lbl_chunk3MaxHeartRate.AutoSize = true;
            this.lbl_chunk3MaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3MaxHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3MaxHeartRate.Location = new System.Drawing.Point(6, 204);
            this.lbl_chunk3MaxHeartRate.Name = "lbl_chunk3MaxHeartRate";
            this.lbl_chunk3MaxHeartRate.Size = new System.Drawing.Size(138, 17);
            this.lbl_chunk3MaxHeartRate.TabIndex = 15;
            this.lbl_chunk3MaxHeartRate.Text = "Maximum Heart Rate :";
            // 
            // lbl_chunk3AvgHeartRate
            // 
            this.lbl_chunk3AvgHeartRate.AutoSize = true;
            this.lbl_chunk3AvgHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3AvgHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3AvgHeartRate.Location = new System.Drawing.Point(6, 163);
            this.lbl_chunk3AvgHeartRate.Name = "lbl_chunk3AvgHeartRate";
            this.lbl_chunk3AvgHeartRate.Size = new System.Drawing.Size(129, 17);
            this.lbl_chunk3AvgHeartRate.TabIndex = 14;
            this.lbl_chunk3AvgHeartRate.Text = "Average Heart Rate :";
            // 
            // lbl_chunk3MaxSpeed
            // 
            this.lbl_chunk3MaxSpeed.AutoSize = true;
            this.lbl_chunk3MaxSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3MaxSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3MaxSpeed.Location = new System.Drawing.Point(6, 125);
            this.lbl_chunk3MaxSpeed.Name = "lbl_chunk3MaxSpeed";
            this.lbl_chunk3MaxSpeed.Size = new System.Drawing.Size(113, 17);
            this.lbl_chunk3MaxSpeed.TabIndex = 13;
            this.lbl_chunk3MaxSpeed.Text = "Maximum Speed :";
            // 
            // lbl_chunk3AvgSpeed
            // 
            this.lbl_chunk3AvgSpeed.AutoSize = true;
            this.lbl_chunk3AvgSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk3AvgSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk3AvgSpeed.Location = new System.Drawing.Point(6, 87);
            this.lbl_chunk3AvgSpeed.Name = "lbl_chunk3AvgSpeed";
            this.lbl_chunk3AvgSpeed.Size = new System.Drawing.Size(104, 17);
            this.lbl_chunk3AvgSpeed.TabIndex = 12;
            this.lbl_chunk3AvgSpeed.Text = "Average Speed :";
            // 
            // grpchunk2Summ
            // 
            this.grpchunk2Summ.BackColor = System.Drawing.Color.White;
            this.grpchunk2Summ.Controls.Add(this.lblchunk2TotalDistance);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2TotalDistance);
            this.grpchunk2Summ.Controls.Add(this.lblchunk2MaxAltitude);
            this.grpchunk2Summ.Controls.Add(this.lblchunk2AvgAltitude);
            this.grpchunk2Summ.Controls.Add(this.lblchunk2MaxPower);
            this.grpchunk2Summ.Controls.Add(this.lblchunk2AvgPower);
            this.grpchunk2Summ.Controls.Add(this.lblchunk2MinHeartRate);
            this.grpchunk2Summ.Controls.Add(this.lblchunk2MaxHeartRate);
            this.grpchunk2Summ.Controls.Add(this.lblchunk2AvgHeartRate);
            this.grpchunk2Summ.Controls.Add(this.lblchunk2MaxSpeed);
            this.grpchunk2Summ.Controls.Add(this.lblchunk2AvgSpeed);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2MaxAltitude);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2AvgAltitude);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2MaxPower);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2AvgPower);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2MinHeartRate);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2MaxHeartRate);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2AvgHeartRate);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2MaxSpeed);
            this.grpchunk2Summ.Controls.Add(this.lbl_chunk2AvgSpeed);
            this.grpchunk2Summ.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpchunk2Summ.Location = new System.Drawing.Point(291, 14);
            this.grpchunk2Summ.Name = "grpchunk2Summ";
            this.grpchunk2Summ.Size = new System.Drawing.Size(277, 432);
            this.grpchunk2Summ.TabIndex = 32;
            this.grpchunk2Summ.TabStop = false;
            this.grpchunk2Summ.Text = "Chunk 2 Summary Data";
            // 
            // grpchunk4Summ
            // 
            this.grpchunk4Summ.BackColor = System.Drawing.Color.White;
            this.grpchunk4Summ.Controls.Add(this.lblchunk4TotalDistance);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4TotalDistance);
            this.grpchunk4Summ.Controls.Add(this.lblchunk4MaxAltitude);
            this.grpchunk4Summ.Controls.Add(this.lblchunk4AvgAltitude);
            this.grpchunk4Summ.Controls.Add(this.lblchunk4MaxPower);
            this.grpchunk4Summ.Controls.Add(this.lblchunk4AvgPower);
            this.grpchunk4Summ.Controls.Add(this.lblchunk4MinHeartRate);
            this.grpchunk4Summ.Controls.Add(this.lblchunk4MaxHeartRate);
            this.grpchunk4Summ.Controls.Add(this.lblchunk4AvgHeartRate);
            this.grpchunk4Summ.Controls.Add(this.lblchunk4MaxSpeed);
            this.grpchunk4Summ.Controls.Add(this.lblchunk4AvgSpeed);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4MaxAltitude);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4AvgAltitude);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4MaxPower);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4AvgPower);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4MinHeartRate);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4MaxHeartRate);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4AvgHeartRate);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4MaxSpeed);
            this.grpchunk4Summ.Controls.Add(this.lbl_chunk4AvgSpeed);
            this.grpchunk4Summ.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpchunk4Summ.Location = new System.Drawing.Point(858, 14);
            this.grpchunk4Summ.Name = "grpchunk4Summ";
            this.grpchunk4Summ.Size = new System.Drawing.Size(277, 432);
            this.grpchunk4Summ.TabIndex = 32;
            this.grpchunk4Summ.TabStop = false;
            this.grpchunk4Summ.Text = "Chunk 4 Summary Data";
            // 
            // lblchunk4TotalDistance
            // 
            this.lblchunk4TotalDistance.AutoSize = true;
            this.lblchunk4TotalDistance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4TotalDistance.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4TotalDistance.Location = new System.Drawing.Point(187, 52);
            this.lblchunk4TotalDistance.Name = "lblchunk4TotalDistance";
            this.lblchunk4TotalDistance.Size = new System.Drawing.Size(48, 17);
            this.lblchunk4TotalDistance.TabIndex = 31;
            this.lblchunk4TotalDistance.Text = "0 km/h";
            // 
            // lbl_chunk4TotalDistance
            // 
            this.lbl_chunk4TotalDistance.AutoSize = true;
            this.lbl_chunk4TotalDistance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chunk4TotalDistance.ForeColor = System.Drawing.Color.Blue;
            this.lbl_chunk4TotalDistance.Location = new System.Drawing.Point(6, 52);
            this.lbl_chunk4TotalDistance.Name = "lbl_chunk4TotalDistance";
            this.lbl_chunk4TotalDistance.Size = new System.Drawing.Size(149, 17);
            this.lbl_chunk4TotalDistance.TabIndex = 30;
            this.lbl_chunk4TotalDistance.Text = "Total Distance Covered :";
            // 
            // lblchunk4MaxAltitude
            // 
            this.lblchunk4MaxAltitude.AutoSize = true;
            this.lblchunk4MaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4MaxAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4MaxAltitude.Location = new System.Drawing.Point(187, 397);
            this.lblchunk4MaxAltitude.Name = "lblchunk4MaxAltitude";
            this.lblchunk4MaxAltitude.Size = new System.Drawing.Size(43, 17);
            this.lblchunk4MaxAltitude.TabIndex = 29;
            this.lblchunk4MaxAltitude.Text = "0 m/ft";
            // 
            // lblchunk4AvgAltitude
            // 
            this.lblchunk4AvgAltitude.AutoSize = true;
            this.lblchunk4AvgAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4AvgAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4AvgAltitude.Location = new System.Drawing.Point(187, 359);
            this.lblchunk4AvgAltitude.Name = "lblchunk4AvgAltitude";
            this.lblchunk4AvgAltitude.Size = new System.Drawing.Size(43, 17);
            this.lblchunk4AvgAltitude.TabIndex = 28;
            this.lblchunk4AvgAltitude.Text = "0 m/ft";
            // 
            // lblchunk4MaxPower
            // 
            this.lblchunk4MaxPower.AutoSize = true;
            this.lblchunk4MaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4MaxPower.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4MaxPower.Location = new System.Drawing.Point(187, 320);
            this.lblchunk4MaxPower.Name = "lblchunk4MaxPower";
            this.lblchunk4MaxPower.Size = new System.Drawing.Size(49, 17);
            this.lblchunk4MaxPower.TabIndex = 27;
            this.lblchunk4MaxPower.Text = "0 watts";
            // 
            // lblchunk4AvgPower
            // 
            this.lblchunk4AvgPower.AutoSize = true;
            this.lblchunk4AvgPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4AvgPower.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4AvgPower.Location = new System.Drawing.Point(187, 282);
            this.lblchunk4AvgPower.Name = "lblchunk4AvgPower";
            this.lblchunk4AvgPower.Size = new System.Drawing.Size(49, 17);
            this.lblchunk4AvgPower.TabIndex = 26;
            this.lblchunk4AvgPower.Text = "0 watts";
            // 
            // lblchunk4MinHeartRate
            // 
            this.lblchunk4MinHeartRate.AutoSize = true;
            this.lblchunk4MinHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4MinHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4MinHeartRate.Location = new System.Drawing.Point(187, 243);
            this.lblchunk4MinHeartRate.Name = "lblchunk4MinHeartRate";
            this.lblchunk4MinHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk4MinHeartRate.TabIndex = 25;
            this.lblchunk4MinHeartRate.Text = "0 bpm";
            // 
            // lblchunk4MaxHeartRate
            // 
            this.lblchunk4MaxHeartRate.AutoSize = true;
            this.lblchunk4MaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchunk4MaxHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblchunk4MaxHeartRate.Location = new System.Drawing.Point(187, 204);
            this.lblchunk4MaxHeartRate.Name = "lblchunk4MaxHeartRate";
            this.lblchunk4MaxHeartRate.Size = new System.Drawing.Size(46, 17);
            this.lblchunk4MaxHeartRate.TabIndex = 24;
            this.lblchunk4MaxHeartRate.Text = "0 bpm";
            // 
            // SelectedChunkData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 450);
            this.Controls.Add(this.panel);
            this.Name = "SelectedChunkData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SelectedChunkData";
            this.grpchunk1Summ.ResumeLayout(false);
            this.grpchunk1Summ.PerformLayout();
            this.panel.ResumeLayout(false);
            this.grpchunk3Summ.ResumeLayout(false);
            this.grpchunk3Summ.PerformLayout();
            this.grpchunk2Summ.ResumeLayout(false);
            this.grpchunk2Summ.PerformLayout();
            this.grpchunk4Summ.ResumeLayout(false);
            this.grpchunk4Summ.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_chunk1TotalDistance;
        private System.Windows.Forms.GroupBox grpchunk1Summ;
        private System.Windows.Forms.Label lblchunk1TotalDistance;
        private System.Windows.Forms.Label lblchunk1MaxAltitude;
        private System.Windows.Forms.Label lblchunk1AvgAltitude;
        private System.Windows.Forms.Label lblchunk1MaxPower;
        private System.Windows.Forms.Label lblchunk1AvgPower;
        private System.Windows.Forms.Label lblchunk1MinHeartRate;
        private System.Windows.Forms.Label lblchunk1MaxHeartRate;
        private System.Windows.Forms.Label lblchunk1AvgHeartRate;
        private System.Windows.Forms.Label lblchunk1MaxSpeed;
        private System.Windows.Forms.Label lblchunk1AvgSpeed;
        private System.Windows.Forms.Label lbl_chunk1MaxAltitude;
        private System.Windows.Forms.Label lbl_chunk1AvgAltitude;
        private System.Windows.Forms.Label lbl_chunk1MaxPower;
        private System.Windows.Forms.Label lbl_chunk1AvgPower;
        private System.Windows.Forms.Label lbl_chunk1MinHeartRate;
        private System.Windows.Forms.Label lbl_chunk1MaxHeartRate;
        private System.Windows.Forms.Label lbl_chunk1AvgHeartRate;
        private System.Windows.Forms.Label lbl_chunk1MaxSpeed;
        private System.Windows.Forms.Label lbl_chunk1AvgSpeed;
        private System.Windows.Forms.Label lblchunk2TotalDistance;
        private System.Windows.Forms.Label lbl_chunk2TotalDistance;
        private System.Windows.Forms.Label lblchunk4AvgHeartRate;
        private System.Windows.Forms.Label lblchunk4MaxSpeed;
        private System.Windows.Forms.Label lblchunk4AvgSpeed;
        private System.Windows.Forms.Label lbl_chunk4MaxAltitude;
        private System.Windows.Forms.Label lbl_chunk4AvgAltitude;
        private System.Windows.Forms.Label lbl_chunk4MaxPower;
        private System.Windows.Forms.Label lbl_chunk4AvgPower;
        private System.Windows.Forms.Label lbl_chunk4MinHeartRate;
        private System.Windows.Forms.Label lbl_chunk4MaxHeartRate;
        private System.Windows.Forms.Label lbl_chunk4AvgHeartRate;
        private System.Windows.Forms.Label lbl_chunk4MaxSpeed;
        private System.Windows.Forms.Label lbl_chunk4AvgSpeed;
        private System.Windows.Forms.Label lblchunk2MaxAltitude;
        private System.Windows.Forms.Label lblchunk2AvgAltitude;
        private System.Windows.Forms.Label lblchunk2MaxPower;
        private System.Windows.Forms.Label lblchunk2AvgPower;
        private System.Windows.Forms.Label lblchunk2MinHeartRate;
        private System.Windows.Forms.Label lblchunk2MaxHeartRate;
        private System.Windows.Forms.Label lblchunk2AvgHeartRate;
        private System.Windows.Forms.Label lblchunk2MaxSpeed;
        private System.Windows.Forms.Label lblchunk2AvgSpeed;
        private System.Windows.Forms.Label lbl_chunk2MaxAltitude;
        private System.Windows.Forms.Label lbl_chunk2AvgAltitude;
        private System.Windows.Forms.Label lbl_chunk2MaxPower;
        private System.Windows.Forms.Label lbl_chunk2AvgPower;
        private System.Windows.Forms.Label lbl_chunk2MinHeartRate;
        private System.Windows.Forms.Label lbl_chunk2MaxHeartRate;
        private System.Windows.Forms.Label lbl_chunk2AvgHeartRate;
        private System.Windows.Forms.Label lbl_chunk2MaxSpeed;
        private System.Windows.Forms.Label lbl_chunk2AvgSpeed;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.GroupBox grpchunk3Summ;
        private System.Windows.Forms.Label lblchunk3TotalDistance;
        private System.Windows.Forms.Label lbl_chunk3TotalDistance;
        private System.Windows.Forms.Label lblchunk3MaxAltitude;
        private System.Windows.Forms.Label lblchunk3AvgAltitude;
        private System.Windows.Forms.Label lblchunk3MaxPower;
        private System.Windows.Forms.Label lblchunk3AvgPower;
        private System.Windows.Forms.Label lblchunk3MinHeartRate;
        private System.Windows.Forms.Label lblchunk3MaxHeartRate;
        private System.Windows.Forms.Label lblchunk3AvgHeartRate;
        private System.Windows.Forms.Label lblchunk3MaxSpeed;
        private System.Windows.Forms.Label lblchunk3AvgSpeed;
        private System.Windows.Forms.Label lbl_chunk3MaxAltitude;
        private System.Windows.Forms.Label lbl_chunk3AvgAltitude;
        private System.Windows.Forms.Label lbl_chunk3MaxPower;
        private System.Windows.Forms.Label lbl_chunk3AvgPower;
        private System.Windows.Forms.Label lbl_chunk3MinHeartRate;
        private System.Windows.Forms.Label lbl_chunk3MaxHeartRate;
        private System.Windows.Forms.Label lbl_chunk3AvgHeartRate;
        private System.Windows.Forms.Label lbl_chunk3MaxSpeed;
        private System.Windows.Forms.Label lbl_chunk3AvgSpeed;
        private System.Windows.Forms.GroupBox grpchunk2Summ;
        private System.Windows.Forms.GroupBox grpchunk4Summ;
        private System.Windows.Forms.Label lblchunk4TotalDistance;
        private System.Windows.Forms.Label lbl_chunk4TotalDistance;
        private System.Windows.Forms.Label lblchunk4MaxAltitude;
        private System.Windows.Forms.Label lblchunk4AvgAltitude;
        private System.Windows.Forms.Label lblchunk4MaxPower;
        private System.Windows.Forms.Label lblchunk4AvgPower;
        private System.Windows.Forms.Label lblchunk4MinHeartRate;
        private System.Windows.Forms.Label lblchunk4MaxHeartRate;
    }
}