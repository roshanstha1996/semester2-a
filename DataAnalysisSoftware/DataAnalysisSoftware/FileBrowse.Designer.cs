﻿namespace DataAnalysisSoftware
{
    partial class FileBrowse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileBrowse));
            this.lbl_title = new System.Windows.Forms.Label();
            this.btn_browse_hrm = new System.Windows.Forms.Button();
            this.lbl_select = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.ForeColor = System.Drawing.Color.White;
            this.lbl_title.Location = new System.Drawing.Point(72, 26);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(295, 33);
            this.lbl_title.TabIndex = 2;
            this.lbl_title.Text = "Polar Cycle Data Analysis";
            // 
            // btn_browse_hrm
            // 
            this.btn_browse_hrm.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_browse_hrm.ForeColor = System.Drawing.Color.Blue;
            this.btn_browse_hrm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_browse_hrm.Location = new System.Drawing.Point(167, 136);
            this.btn_browse_hrm.Name = "btn_browse_hrm";
            this.btn_browse_hrm.Size = new System.Drawing.Size(119, 34);
            this.btn_browse_hrm.TabIndex = 5;
            this.btn_browse_hrm.Text = "Select";
            this.btn_browse_hrm.UseVisualStyleBackColor = true;
            this.btn_browse_hrm.Click += new System.EventHandler(this.btn_browse_hrm_Click);
            // 
            // lbl_select
            // 
            this.lbl_select.AutoSize = true;
            this.lbl_select.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_select.ForeColor = System.Drawing.Color.White;
            this.lbl_select.Location = new System.Drawing.Point(91, 90);
            this.lbl_select.Name = "lbl_select";
            this.lbl_select.Size = new System.Drawing.Size(250, 24);
            this.lbl_select.TabIndex = 4;
            this.lbl_select.Text = "Browse for HRM File:";
            // 
            // FileBrowse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Coral;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(453, 230);
            this.Controls.Add(this.btn_browse_hrm);
            this.Controls.Add(this.lbl_select);
            this.Controls.Add(this.lbl_title);
            this.Name = "FileBrowse";
            this.Text = "FileBrowse";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Button btn_browse_hrm;
        private System.Windows.Forms.Label lbl_select;
    }
}