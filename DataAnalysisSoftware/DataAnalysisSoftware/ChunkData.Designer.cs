﻿namespace DataAnalysisSoftware
{
    partial class ChunkData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_chunk = new System.Windows.Forms.Button();
            this.cmbchunkValue = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(509, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Choose the number to divide the selected row into chunks";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(53, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Input the number:";
            // 
            // btn_chunk
            // 
            this.btn_chunk.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_chunk.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_chunk.Location = new System.Drawing.Point(237, 148);
            this.btn_chunk.Name = "btn_chunk";
            this.btn_chunk.Size = new System.Drawing.Size(91, 51);
            this.btn_chunk.TabIndex = 8;
            this.btn_chunk.Text = "Chunk";
            this.btn_chunk.UseVisualStyleBackColor = true;
            this.btn_chunk.Click += new System.EventHandler(this.btn_chunk_Click);
            // 
            // cmbchunkValue
            // 
            this.cmbchunkValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbchunkValue.Font = new System.Drawing.Font("Consolas", 14.25F);
            this.cmbchunkValue.FormattingEnabled = true;
            this.cmbchunkValue.Location = new System.Drawing.Point(219, 83);
            this.cmbchunkValue.Name = "cmbchunkValue";
            this.cmbchunkValue.Size = new System.Drawing.Size(203, 30);
            this.cmbchunkValue.TabIndex = 6;
            // 
            // ChunkData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 216);
            this.Controls.Add(this.btn_chunk);
            this.Controls.Add(this.cmbchunkValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ChunkData";
            this.Text = "ChunkData";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_chunk;
        private System.Windows.Forms.ComboBox cmbchunkValue;
    }
}