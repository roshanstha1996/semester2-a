﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace DataAnalysisSoftware
{

    /// <summary>
    /// view graph class
    /// </summary>
    public partial class ViewGraph : Form
    {

        /// <summary>
        /// constructor with parameters
        /// </summary>
        /// <param name="arrayHR"></param>
        /// <param name="arraySpeed"></param>
        /// <param name="arrayCadence"></param>
        /// <param name="arrayAltitude"></param>
        /// <param name="arrayPower"></param>
        public ViewGraph(int[] arrayHR, int[] arraySpeed, int[] arrayCadence, int[] arrayAltitude, int[] arrayPower)
        {
            InitializeComponent();
            this.arrayHR = arrayHR;
            this.arraySpeed = arraySpeed;
            this.arrayCadence = arrayCadence;
            this.arrayAltitude = arrayAltitude;
            this.arrayPower = arrayPower;
            drawGraph();
        }

        private int[] arrayHR;
        private int[] arraySpeed;
        private int[] arrayCadence;
        private int[] arrayAltitude;
        private int[] arrayPower;

        /// <summary>
        /// method to draw graph
        /// </summary>
        public void drawGraph()
        {
            
            GraphPane graphpane = zedGraphControl1.GraphPane; //method for showing the segment with thier individual graph
            //graph and axis titles
            graphpane.Title = "Cycle Data Graph";
            graphpane.XAxis.Title = "Time Interval (seconds)";
            graphpane.YAxis.Title = "Value";
           
            //pointpairlist to determine X and Y cordinates
            PointPairList hrPPL = new PointPairList();
            PointPairList speedPPL = new PointPairList();
            PointPairList cadencePPL = new PointPairList();
            PointPairList altitudePPL = new PointPairList();
            PointPairList powerPPL = new PointPairList();
            int items = arrayHR.Length;
            //y-axiz min and max value
            graphpane.YAxis.Min = 0;
            graphpane.YAxis.Max = 1300;
            graphpane.XAxis.Max = items;

            for (int i = 0; i < items; i++)
            {
                hrPPL.Add(i, arrayHR[i]);
                speedPPL.Add(i, arraySpeed[i]);
                cadencePPL.Add(i, arrayCadence[i]);
                altitudePPL.Add(i, arrayAltitude[i]);
                powerPPL.Add(i, arrayPower[i]);
            }


            //creating line and adding to graph panel
            LineItem heartRateCurve = graphpane.AddCurve("Heart Rate", hrPPL, Color.Red, SymbolType.None);
            LineItem speedCurve = graphpane.AddCurve("Speed", speedPPL, Color.Blue, SymbolType.None);
            LineItem cadenceCurve = graphpane.AddCurve("Cadence", cadencePPL, Color.Black, SymbolType.None);
            LineItem altitudeCurve = graphpane.AddCurve("Altitude", altitudePPL, Color.Brown, SymbolType.None);
            LineItem powerCurve = graphpane.AddCurve("Power", powerPPL, Color.Green, SymbolType.None);

            //for auto scaling operation
            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
        }
    }
}
