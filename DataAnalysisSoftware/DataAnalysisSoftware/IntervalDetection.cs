﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAnalysisSoftware
{
    /// <summary>
    /// class of interval detection
    /// </summary>
    public partial class IntervalDetection : Form
    {
        //declared variables
        int[] arrayHR;
        int[] arraySpeed;
        int[] arrayCadence;
        int[] arrayAltitude;
        int[] arrayPower;

        int interval = 1;
        int index = 1;
        double avgPower;
        double totalSeconds;
        List<double[]> intervalList = new List<double[]>();

        /// <summary>
        /// interval detection constuctor with parameters
        /// </summary>
        /// <param name="arrayHR"></param>
        /// <param name="arraySpeed"></param>
        /// <param name="arrayCadence"></param>
        /// <param name="arrayAltitude"></param>
        /// <param name="arrayPower"></param>
        /// <param name="totalSeconds"></param>
        public IntervalDetection(int[] arrayHR, int[] arraySpeed, int[] arrayCadence, int[] arrayAltitude,
            int[] arrayPower, double totalSeconds)
        {
            this.arrayHR = arrayHR;
            this.arraySpeed = arraySpeed;
            this.arrayCadence = arrayCadence;
            this.arrayAltitude = arrayAltitude;
            this.arrayPower = arrayPower;
            this.totalSeconds = totalSeconds;
            InitializeComponent();
            loadGridViewData();
        }

        /// <summary>
        /// event on interval detection load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IntervalDetection_Load(object sender, EventArgs e)
        {
            //dataGridView1.Visible = false;
        }

        /// <summary>
        /// method to load data into gridview
        /// </summary>
        public void loadGridViewData()
        {
            avgPower = arrayPower.Average();
            double FTP = Math.Round(avgPower * 0.95);
            double thresholdpower = Math.Round((FTP * 1.05), 2);
            bool isPeak = false;
            //detecting interval based on threshold power
            for (int i = 0; i < arrayPower.Length; i++)
            {
                if (arrayPower[i] > thresholdpower)
                {
                    isPeak = true;
                }
                if (isPeak)
                {
                    //inserting required data into gridview
                    dataGridView.Rows.Add(arrayHR[i], arraySpeed[i], arrayCadence[i], arrayAltitude[i], arrayPower[i], interval);
                    double[] storeInterval = new double[6] { Convert.ToDouble(arrayPower[i]), interval, Convert.ToDouble(arrayHR[i]),
                        Convert.ToDouble(arraySpeed[i]), Convert.ToDouble(arrayCadence[i]), Convert.ToDouble(arrayAltitude[i]) };
                    intervalList.Add(storeInterval);
                    if (arrayPower[i] < thresholdpower)
                    {
                        interval = interval + 1;
                        isPeak = false;
                    }
                }
            }
            //calling method to load summary data
            loadSummaryData(interval);
        }

        /// <summary>
        /// method to load summary and data into new gridview for each interval
        /// </summary>
        /// <param name="interval"></param>
        public void loadSummaryData(int interval)
        {
            try
            {
                txtInterval.Text = index.ToString();
                dataGridView1.Rows.Clear();
                //initializing required lists
                List<double> power = new List<double>();
                List<double> heartrate = new List<double>();
                List<double> altitude = new List<double>();
                List<double> speed = new List<double>();
                List<double> cadence = new List<double>();
                power.Clear();
                //adding required row in new gridview
                for (int i = 0; i < intervalList.Count; i++)
                {
                    if (intervalList[i][1] == index)
                    {
                        dataGridView1.Rows.Add(intervalList[i][1], intervalList[i][0], intervalList[i][2], intervalList[i][3] / 10, intervalList[i][4], intervalList[i][5]);
                    }
                }
                //adding values of new gridview column to corresponding list
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    power.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value));
                    heartrate.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[2].Value));
                    speed.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value));
                    cadence.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[4].Value));
                    altitude.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[5].Value));
                }
                double distance = 0;
                for (int i = 0; i < speed.Count; i++)
                {
                    distance += Math.Round((speed[i]) / 3600, 2);
                }
                //displaying average data into required labels
                lblavgSpeed.Text = Math.Round(speed.Average()).ToString() + " " + "km/h";
                lblmaxSpeed.Text = speed.Max().ToString() + " " + "km/h";
                lblavgPower.Text = Math.Round(power.Average()).ToString() + " " + "watts";
                lblmaxPower.Text = Math.Round(power.Max()).ToString() + " " + "watts";
                lblavgHeartRate.Text = Math.Round(heartrate.Average()).ToString() + " " + "bpm";
                lblminHeartRate.Text = heartrate.Min().ToString() + " " + "bpm";
                lblmaxHeartRate.Text = heartrate.Max().ToString() + " " + "bpm";
                lblavgAltitude.Text = Math.Round(altitude.Average()).ToString() + " " + "m/ft";
                lblmaxAltitude.Text = altitude.Max().ToString() + " " + "m/ft";
                lbltotalDistance.Text = distance.ToString() + " " + "km/h";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// event for previous interval click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPreviousInterval_Click(object sender, EventArgs e)
        {
            index--;
            loadSummaryData(index);
        }

        /// <summary>
        /// event for next interval click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNextInterval_Click(object sender, EventArgs e)
        {
            index++;
            loadSummaryData(index);
        }
    }
}
