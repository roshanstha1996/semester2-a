﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;

namespace DataAnalysisSoftware
{
    /// <summary>
    /// class for file comparison
    /// </summary>
    public partial class FileComparison : Form
    {

        //variales declared
        string file1;
        string file2;
        string line;
        string startTime = "00:00:00.000";
        string lengthValue = "00:00:00.000";
        string interval = "0";
        int[] ArrayHR;
        int[] ArraySpeed;
        int[] ArrayCadence;
        int[] ArrayAltitude;
        int[] ArrayPower;
        int totalItems;
        double AverageSpeed;
        double avgPower;
        double totalSeconds;

        /// <summary>
        /// constructor with parameters
        /// </summary>
        /// <param name="file1"></param>
        /// <param name="file2"></param>
        public FileComparison(string file1, string file2)
        {
            this.file1 = file1;
            this.file2 = file2;
            InitializeComponent();

            //method called to load necessary info
            LoadFile1HeaderInformation();
            file1hrdata();
            LoadFile1DefaultColumnCellVal();
            file1_summarydata_load();
            LoadFile2HeaderInformation();
            file2hrdata();
            LoadFile2DefaultColumnCellVal();
            file2_summarydata_load();
            loadPM();
        }

        /// <summary>
        /// fetching the headers from the file1
        /// </summary>
        public void LoadFile1HeaderInformation()
        {
            //reads file object
            StreamReader reader = new StreamReader(file1);

            //reads each line of the file
            while ((line = reader.ReadLine()) != null)  //reads each line of the file using stream reader
            {
                if (line.Contains("StartTime"))
                {
                    startTime = line.Split('=').Last();
                }
                if (line.Contains("Length"))
                {
                    lengthValue = line.Split('=').Last();
                }
                if (line.Contains("Interval"))
                {
                    interval = line.Split('=').Last();
                }
            }
        }

        /// <summary>
        /// fetching the headers from the file2
        /// </summary>
        public void LoadFile2HeaderInformation()
        {
            StreamReader reader = new StreamReader(file2);
            while ((line = reader.ReadLine()) != null)  //reads each line of the file using stream reader
            {
                if (line.Contains("StartTime"))
                {
                    startTime = line.Split('=').Last();
                }
                if (line.Contains("Length"))
                {
                    lengthValue = line.Split('=').Last();
                }
                if (line.Contains("Interval"))
                {
                    interval = line.Split('=').Last();
                }
            }
        }

        /// <summary>
        /// method for loading hrdata of file1
        /// </summary>
        public void file1hrdata()
        {
            //converts string time to DateTime type
            DateTime typestartTime = DateTime.ParseExact(startTime,
                "HH:mm:ss.FFF", CultureInfo.InvariantCulture);
            int typeInterval = Convert.ToInt32(interval);
            //read all file content
            string fileContent = File.ReadAllText(file1);
            //finds certain elements
            string toFind = "[HRData]";
            //index of searched element
            int index = fileContent.IndexOf(toFind);  //index of searched element
            //remaining file content after excluding searched element
            string searchFileContent = fileContent.Substring(index + 10, 
                fileContent.Length - (index + 10));


            //string reader to read string content
            StringReader reader = new StringReader(searchFileContent);

            int row = 0;
            //reading each line of the string
            while ((line = reader.ReadLine()) != null)
            {
                //splits the line on specific words & displays fetched data
                string[] columnData = line.Split('\t');
                dataGridView1.Rows.Add();
                for (int i = 0; i < columnData.Length; i++)
                {
                    dataGridView1[i, row].Value = columnData[i];
                }
                row++;
            }
            int allrows = dataGridView1.RowCount;
            int rowIndex = 6;
            for (int columnIndex = 0; columnIndex < allrows; columnIndex++)
            {
                //adding time value in each column
                dataGridView1[rowIndex, columnIndex].Value = typestartTime.ToLongTimeString();
                typestartTime = typestartTime.AddSeconds(typeInterval);
            }
        }


        /// <summary>
        /// method for loading hrdata of file2
        /// </summary>
        public void file2hrdata()
        {
            //converts string time to DateTime type
            DateTime typestartTime = DateTime.ParseExact(startTime,
                "HH:mm:ss.FFF", CultureInfo.InvariantCulture);
            int typeInterval = Convert.ToInt32(interval);
            //read all file content
            string fileContent = File.ReadAllText(file2);
            //finds certain elements
            string toFind = "[HRData]";
            //index of searched element
            int index = fileContent.IndexOf(toFind);  //index of searched element
            //remaining file content after excluding searched element
            string searchFileContent = fileContent.Substring(index + 10,
                fileContent.Length - (index + 10));


            //string reader to read string content
            StringReader reader = new StringReader(searchFileContent);

            int row = 0;
            //reading each line of the string
            while ((line = reader.ReadLine()) != null)
            {
                //splits the line on specific words & displays fetched data
                string[] columnData = line.Split('\t');
                dataGridView2.Rows.Add();
                for (int i = 0; i < columnData.Length; i++)
                {
                    dataGridView2[i, row].Value = columnData[i];
                }
                row++;
            }
            int allrows = dataGridView2.RowCount;
            int rowIndex = 6;
            for (int columnIndex = 0; columnIndex < allrows; columnIndex++)
            {
                //adding time value in each column
                dataGridView2[rowIndex, columnIndex].Value = typestartTime.ToLongTimeString();
                typestartTime = typestartTime.AddSeconds(typeInterval);
            }
        }

        /// <summary>
        /// method to load default column cell value for file 2 cells
        /// </summary>
        public void LoadFile1DefaultColumnCellVal()
        {
            int totalRows = dataGridView1.RowCount;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((row.Cells["HeartRate"].Value) == null)
                {
                    row.Cells["HeartRate"].Value = 0;
                }
                if ((row.Cells["Speed"].Value) == null)
                {
                    row.Cells["Speed"].Value = 0;
                }
                if ((row.Cells["Cadence"].Value) == null)
                {
                    row.Cells["Cadence"].Value = 0;
                }
                if ((row.Cells["Altitude"].Value) == null)
                {
                    row.Cells["Altitude"].Value = 0;
                }
                if ((row.Cells["Power"].Value) == null)
                {
                    row.Cells["Power"].Value = 0;
                }
                if ((row.Cells["PowerIndex"].Value) == null)
                {
                    row.Cells["PowerIndex"].Value = 0;
                }
            }
        }

        /// <summary>
        /// method to load default column cell value for file 2 cells
        /// </summary>
        public void LoadFile2DefaultColumnCellVal()
        {
            int totalRows = dataGridView2.RowCount;
            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                if ((row.Cells["File2HeartRate"].Value) == null)
                {
                    row.Cells["File2HeartRate"].Value = 0;
                }
                if ((row.Cells["File2Speed"].Value) == null)
                {
                    row.Cells["File2Speed"].Value = 0;
                }
                if ((row.Cells["File2Cadence"].Value) == null)
                {
                    row.Cells["File2Cadence"].Value = 0;
                }
                if ((row.Cells["File2Altitude"].Value) == null)
                {
                    row.Cells["File2Altitude"].Value = 0;
                }
                if ((row.Cells["File2Power"].Value) == null)
                {
                    row.Cells["File2Power"].Value = 0;
                }
                if ((row.Cells["File2PowerIndex"].Value) == null)
                {
                    row.Cells["File2PowerIndex"].Value = 0;
                }
            }
        }

        /// <summary>
        /// method to load summary data for file 1
        /// </summary>
        public void file1_summarydata_load()
        {
            List<int> hr = new List<int>();
            List<int> speed = new List<int>();
            List<int> cadence = new List<int>();
            List<int> altitude = new List<int>();
            List<int> power = new List<int>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //adding data of every column to its repective list  
                hr.Add(int.Parse(row.Cells["HeartRate"].Value.ToString()));
                speed.Add(int.Parse(row.Cells["Speed"].Value.ToString()));
                cadence.Add(int.Parse(row.Cells["Cadence"].Value.ToString()));
                altitude.Add(int.Parse(row.Cells["Altitude"].Value.ToString()));
                power.Add(int.Parse(row.Cells["Power"].Value.ToString()));
            }

            //now converting list to array
            ArrayHR = hr.ToArray();
            ArraySpeed = speed.ToArray();
            ArrayCadence = cadence.ToArray();
            ArrayAltitude = altitude.ToArray();
            ArrayPower = power.ToArray();

            //methods called to calculate summary data
            calculateFile1Summary();
        }

        /// <summary>
        /// method to load summary data for file 2
        /// </summary>
        public void file2_summarydata_load()
        {
            List<int> hr = new List<int>();
            List<int> speed = new List<int>();
            List<int> cadence = new List<int>();
            List<int> altitude = new List<int>();
            List<int> power = new List<int>();
            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                //adding data of every column to its repective list  
                hr.Add(int.Parse(row.Cells["File2HeartRate"].Value.ToString()));
                speed.Add(int.Parse(row.Cells["File2Speed"].Value.ToString()));
                cadence.Add(int.Parse(row.Cells["File2Cadence"].Value.ToString()));
                altitude.Add(int.Parse(row.Cells["File2Altitude"].Value.ToString()));
                power.Add(int.Parse(row.Cells["File2Power"].Value.ToString()));
            }

            //now converting list to array
            ArrayHR = hr.ToArray();
            ArraySpeed = speed.ToArray();
            ArrayCadence = cadence.ToArray();
            ArrayAltitude = altitude.ToArray();
            ArrayPower = power.ToArray();

            //methods called to calculate summary data
            calculateFile2Summary();
        }

        /// <summary>
        /// method for calculatiing summary data for file 1
        /// </summary>
        public void calculateFile1Summary()
        {
            //calculating speed

            totalItems = ArraySpeed.Length;
            int totalSpeed = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total speed
                totalSpeed = totalSpeed + ArraySpeed[i];
            }
            //average speed
            AverageSpeed = Math.Round((double)totalSpeed / totalItems, 2);
            lblavgSpeed.Text = AverageSpeed.ToString();
            //maximum speed
            int maxSpeed = ArraySpeed.Max();
            lblmaxSpeed.Text = maxSpeed.ToString();

            //calculating heart rate

            totalItems = ArrayHR.Length;
            int totalHeartRate = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total heart rate
                totalHeartRate = totalHeartRate + ArrayHR[i];
            }
            //average heart rate
            int avgHeartRate = totalHeartRate / totalItems;
            lblavgHeartRate.Text = avgHeartRate.ToString();
            //maximum heart rate
            int maxHeartRate = ArrayHR.Max();
            lblmaxHeartRate.Text = maxHeartRate.ToString();
            //minimum heart rate
            int minHeartRate = ArrayHR.Min();
            lblminHeartRate.Text = minHeartRate.ToString();

            //calculating power

            totalItems = ArrayPower.Length;
            int totalPower = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total power
                totalPower = totalPower + ArrayPower[i];
            }
            //average power
            avgPower = totalPower / totalItems;
            lblavgPower.Text = avgPower.ToString();
            //maximum power
            int maxPower = ArrayPower.Max();
            lblmaxPower.Text = maxPower.ToString();

            //calculating altitude

            totalItems = ArrayAltitude.Length;
            int totalAltitude = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total altitude
                totalAltitude = totalAltitude + ArrayAltitude[i];
            }
            //average altitude
            double avgAltitude = Math.Round((double)totalAltitude / totalItems, 2);
            lblavgAltitude.Text = avgAltitude.ToString();
            //maximum altitude
            int maxAltitude = ArrayAltitude.Max();
            lblmaxAltitude.Text = maxAltitude.ToString();

            //calculating total distance covered

            string length = lengthValue;
            //total seconds from start time
            totalSeconds = TimeSpan.Parse(length).TotalSeconds;
            //calculating distance covered
            double totalDistance = Math.Round((double)(((AverageSpeed / 60) / 60) * totalSeconds), 2);
            lbltotalDistance.Text = totalDistance.ToString();
        }

        /// <summary>
        /// method for calculatiing summary data for file 2
        /// </summary>
        public void calculateFile2Summary()
        {
            //calculating speed

            totalItems = ArraySpeed.Length;
            int totalSpeed = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total speed
                totalSpeed = totalSpeed + ArraySpeed[i];
            }
            //average speed
            AverageSpeed = Math.Round((double)totalSpeed / totalItems, 2);
            lblavgSpeed2.Text = AverageSpeed.ToString();
            //maximum speed
            int maxSpeed = ArraySpeed.Max();
            lblmaxSpeed2.Text = maxSpeed.ToString();

            //calculating heart rate

            totalItems = ArrayHR.Length;
            int totalHeartRate = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total heart rate
                totalHeartRate = totalHeartRate + ArrayHR[i];
            }
            //average heart rate
            int avgHeartRate = totalHeartRate / totalItems;
            lblavgHeartRate2.Text = avgHeartRate.ToString();
            //maximum heart rate
            int maxHeartRate = ArrayHR.Max();
            lblmaxHeartRate2.Text = maxHeartRate.ToString();
            //minimum heart rate
            int minHeartRate = ArrayHR.Min();
            lblminHeartRate2.Text = minHeartRate.ToString();

            //calculating power

            totalItems = ArrayPower.Length;
            int totalPower = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total power
                totalPower = totalPower + ArrayPower[i];
            }
            //average power
            avgPower = totalPower / totalItems;
            lblavgPower2.Text = avgPower.ToString();
            //maximum power
            int maxPower = ArrayPower.Max();
            lblmaxPower2.Text = maxPower.ToString();

            //calculating altitude

            totalItems = ArrayAltitude.Length;
            int totalAltitude = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total altitude
                totalAltitude = totalAltitude + ArrayAltitude[i];
            }
            //average altitude
            double avgAltitude = Math.Round((double)totalAltitude / totalItems, 2);
            lblavgAltitude2.Text = avgAltitude.ToString();
            //maximum altitude
            int maxAltitude = ArrayAltitude.Max();
            lblmaxAltitude2.Text = maxAltitude.ToString();

            //calculating total distance covered

            string length = lengthValue;
            //total seconds from start time
            totalSeconds = TimeSpan.Parse(length).TotalSeconds;
            //calculating distance covered
            double totalDistance = Math.Round((double)(((AverageSpeed / 60) / 60) * totalSeconds), 2);
            lbltotalDistance2.Text = totalDistance.ToString();
        }

        /// <summary>
        /// form load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileComparison_Load(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Selected = false;
            dataGridView2.CurrentCell.Selected = false;
        }

        /// <summary>
        /// event for file 1 selected row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            file1_selected_summarydata_load();
            loadPM();
        }

        /// <summary>
        /// method to load summary data for selected row of file 1
        /// </summary>
        public void file1_selected_summarydata_load()
        {
            List<int> hr = new List<int>();
            List<int> speed = new List<int>();
            List<int> cadence = new List<int>();
            List<int> altitude = new List<int>();
            List<int> power = new List<int>();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                //adding data of every column to its repective list  
                hr.Add(int.Parse(row.Cells["HeartRate"].Value.ToString()));
                speed.Add(int.Parse(row.Cells["Speed"].Value.ToString()));
                cadence.Add(int.Parse(row.Cells["Cadence"].Value.ToString()));
                altitude.Add(int.Parse(row.Cells["Altitude"].Value.ToString()));
                power.Add(int.Parse(row.Cells["Power"].Value.ToString()));
            }

            //now converting list to array
            ArrayHR = hr.ToArray();
            ArraySpeed = speed.ToArray();
            ArrayCadence = cadence.ToArray();
            ArrayAltitude = altitude.ToArray();
            ArrayPower = power.ToArray();

            //methods called to calculate summary data
            calculateFile1Summary();
        }

        private void dataGridView2_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            file2_selected_summarydata_load();
            loadPM();
        }

        /// <summary>
        /// method to load summary data for selected row of file 2
        /// </summary>
        public void file2_selected_summarydata_load()
        {
            List<int> hr = new List<int>();
            List<int> speed = new List<int>();
            List<int> cadence = new List<int>();
            List<int> altitude = new List<int>();
            List<int> power = new List<int>();
            foreach (DataGridViewRow row in dataGridView2.SelectedRows)
            {
                //adding data of every column to its repective list  
                hr.Add(int.Parse(row.Cells["File2HeartRate"].Value.ToString()));
                speed.Add(int.Parse(row.Cells["File2Speed"].Value.ToString()));
                cadence.Add(int.Parse(row.Cells["File2Cadence"].Value.ToString()));
                altitude.Add(int.Parse(row.Cells["File2Altitude"].Value.ToString()));
                power.Add(int.Parse(row.Cells["File2Power"].Value.ToString()));
            }

            //now converting list to array
            ArrayHR = hr.ToArray();
            ArraySpeed = speed.ToArray();
            ArrayCadence = cadence.ToArray();
            ArrayAltitude = altitude.ToArray();
            ArrayPower = power.ToArray();

            //methods called to calculate summary data
            calculateFile2Summary();
        }


        /// <summary>
        /// maximum and minimum between files
        /// </summary>
        public void loadPM()
        {
            //variables to store label data of summary
            double totalDistance = Convert.ToDouble(lbltotalDistance.Text);
            double avgSpeed = Convert.ToDouble(lblavgSpeed.Text);
            double maxSpeed = Convert.ToDouble(lblmaxSpeed.Text);
            double avgHeartRate = Convert.ToDouble(lblavgHeartRate.Text);
            double maxHeartRate = Convert.ToDouble(lblmaxHeartRate.Text);
            double minHeartRate = Convert.ToDouble(lblminHeartRate.Text);
            double avgPower = Convert.ToDouble(lblavgPower.Text);
            double maxPower = Convert.ToDouble(lblmaxPower.Text);
            double avgAltitude = Convert.ToDouble(lblavgAltitude.Text);
            double maxAltitude = Convert.ToDouble(lblmaxAltitude.Text);
            double totalDistance2 = Convert.ToDouble(lbltotalDistance2.Text);
            double avgSpeed2 = Convert.ToDouble(lblavgSpeed2.Text);
            double maxSpeed2 = Convert.ToDouble(lblmaxSpeed2.Text);
            double avgHeartRate2 = Convert.ToDouble(lblavgHeartRate2.Text);
            double maxHeartRate2 = Convert.ToDouble(lblmaxHeartRate2.Text);
            double minHeartRate2 = Convert.ToDouble(lblminHeartRate2.Text);
            double avgPower2 = Convert.ToDouble(lblavgPower2.Text);
            double maxPower2 = Convert.ToDouble(lblmaxPower2.Text);
            double avgAltitude2 = Convert.ToDouble(lblavgAltitude2.Text);
            double maxAltitude2 = Convert.ToDouble(lblmaxAltitude2.Text);
            
            //if statement on certain condition meet
            if (totalDistance > totalDistance2)
            {
                lbltotalDistancePM.Text = "+";
                lbltotalDistance2PM.Text = "-";
            }
            else
            {
                lbltotalDistancePM.Text = "-";
                lbltotalDistance2PM.Text = "+";
            }
            if (avgSpeed > avgSpeed2)
            {
                lblavgSpeedPM.Text = "+";
                lblavgSpeed2PM.Text = "-";
            }
            else
            {
                lblavgSpeedPM.Text = "-";
                lblavgSpeed2PM.Text = "+";
            }
            if (maxSpeed > maxSpeed2)
            {
                lblmaxSpeedPM.Text = "+";
                lblmaxSpeed2PM.Text = "-";
            }
            else
            {
                lblmaxSpeedPM.Text = "-";
                lblmaxSpeed2PM.Text = "+";
            }
            if (avgHeartRate > avgHeartRate2)
            {
                lblavgHeartRatePM.Text = "+";
                lblavgHeartRate2PM.Text = "-";
            }
            else
            {
                lblavgHeartRatePM.Text = "-";
                lblavgHeartRate2PM.Text = "+";
            }
            if (maxHeartRate > maxHeartRate2)
            {
                lblmaxHeartRatePM.Text = "+";
                lblmaxHeartRate2PM.Text = "-";
            }
            else
            {
                lblmaxHeartRatePM.Text = "-";
                lblmaxHeartRate2PM.Text = "+";
            }
            if (minHeartRate > minHeartRate2)
            {
                lblminHeartRatePM.Text = "+";
                lblminHeartRate2PM.Text = "-";
            }
            else
            {
                lblminHeartRatePM.Text = "-";
                lblminHeartRate2PM.Text = "+";
            }
            if (avgPower > avgPower2)
            {
                lblavgPowerPM.Text = "+";
                lblavgPower2PM.Text = "-";
            }
            else
            {
                lblavgPowerPM.Text = "-";
                lblavgPower2PM.Text = "+";
            }
            if (maxPower > maxPower2)
            {
                lblmaxPowerPM.Text = "+";
                lblmaxPower2PM.Text = "-";
            }
            else
            {
                lblmaxPowerPM.Text = "-";
                lblmaxPower2PM.Text = "+";
            }
            if (avgAltitude > avgAltitude2)
            {
                lblavgAltitudePM.Text = "+";
                lblavgAltitude2PM.Text = "-";
            }
            else
            {
                lblavgAltitudePM.Text = "-";
                lblavgAltitude2PM.Text = "+";
            }
            if (maxAltitude > maxAltitude2)
            {
                lblmaxAltitudePM.Text = "+";
                lblmaxAltitude2PM.Text = "-";
            }
            else
            {
                lblmaxAltitudePM.Text = "-";
                lblmaxAltitude2PM.Text = "+";
            }
        }
    }
}
