﻿namespace DataAnalysisSoftware
{
    partial class SelectableInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel = new System.Windows.Forms.Panel();
            this.groupBox_advancedMetrics = new System.Windows.Forms.GroupBox();
            this.lblintensityFactor = new System.Windows.Forms.Label();
            this.lbl_intensityFactor = new System.Windows.Forms.Label();
            this.lbltrainingStressScore = new System.Windows.Forms.Label();
            this.lbl_trainingStressScore = new System.Windows.Forms.Label();
            this.lblnormalisedPower = new System.Windows.Forms.Label();
            this.lbl_normalisedPower = new System.Windows.Forms.Label();
            this.lblpowerBalance = new System.Windows.Forms.Label();
            this.lbl_powerBalance = new System.Windows.Forms.Label();
            this.groupBox_summaryData = new System.Windows.Forms.GroupBox();
            this.lbltotalDistance = new System.Windows.Forms.Label();
            this.lbl_totalDistance = new System.Windows.Forms.Label();
            this.lblmaxAltitude = new System.Windows.Forms.Label();
            this.lblavgAltitude = new System.Windows.Forms.Label();
            this.lblmaxPower = new System.Windows.Forms.Label();
            this.lblavgPower = new System.Windows.Forms.Label();
            this.lblminHeartRate = new System.Windows.Forms.Label();
            this.lblmaxHeartRate = new System.Windows.Forms.Label();
            this.lblavgHeartRate = new System.Windows.Forms.Label();
            this.lblmaxSpeed = new System.Windows.Forms.Label();
            this.lblavgSpeed = new System.Windows.Forms.Label();
            this.lbl_maxAltitude = new System.Windows.Forms.Label();
            this.lbl_avgAltitude = new System.Windows.Forms.Label();
            this.lbl_maxPower = new System.Windows.Forms.Label();
            this.lbl_avgPower = new System.Windows.Forms.Label();
            this.lbl_minHeartRate = new System.Windows.Forms.Label();
            this.lbl_maxHeartRate = new System.Windows.Forms.Label();
            this.lbl_avgHeartRate = new System.Windows.Forms.Label();
            this.lbl_maxSpeed = new System.Windows.Forms.Label();
            this.lbl_avgSpeed = new System.Windows.Forms.Label();
            this.zedGraphControl = new ZedGraph.ZedGraphControl();
            this.pan_createButton = new System.Windows.Forms.Panel();
            this.btnChunkSelectedData = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeStamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel.SuspendLayout();
            this.groupBox_advancedMetrics.SuspendLayout();
            this.groupBox_summaryData.SuspendLayout();
            this.pan_createButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.LightCyan;
            this.panel.Controls.Add(this.groupBox_advancedMetrics);
            this.panel.Controls.Add(this.groupBox_summaryData);
            this.panel.Location = new System.Drawing.Point(5, 401);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1188, 274);
            this.panel.TabIndex = 10;
            // 
            // groupBox_advancedMetrics
            // 
            this.groupBox_advancedMetrics.BackColor = System.Drawing.Color.White;
            this.groupBox_advancedMetrics.Controls.Add(this.lblintensityFactor);
            this.groupBox_advancedMetrics.Controls.Add(this.lbl_intensityFactor);
            this.groupBox_advancedMetrics.Controls.Add(this.lbltrainingStressScore);
            this.groupBox_advancedMetrics.Controls.Add(this.lbl_trainingStressScore);
            this.groupBox_advancedMetrics.Controls.Add(this.lblnormalisedPower);
            this.groupBox_advancedMetrics.Controls.Add(this.lbl_normalisedPower);
            this.groupBox_advancedMetrics.Controls.Add(this.lblpowerBalance);
            this.groupBox_advancedMetrics.Controls.Add(this.lbl_powerBalance);
            this.groupBox_advancedMetrics.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_advancedMetrics.Location = new System.Drawing.Point(719, 20);
            this.groupBox_advancedMetrics.Name = "groupBox_advancedMetrics";
            this.groupBox_advancedMetrics.Size = new System.Drawing.Size(458, 237);
            this.groupBox_advancedMetrics.TabIndex = 6;
            this.groupBox_advancedMetrics.TabStop = false;
            this.groupBox_advancedMetrics.Text = "Advanced Metrics";
            // 
            // lblintensityFactor
            // 
            this.lblintensityFactor.AutoSize = true;
            this.lblintensityFactor.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblintensityFactor.ForeColor = System.Drawing.Color.Black;
            this.lblintensityFactor.Location = new System.Drawing.Point(228, 206);
            this.lblintensityFactor.Name = "lblintensityFactor";
            this.lblintensityFactor.Size = new System.Drawing.Size(17, 20);
            this.lblintensityFactor.TabIndex = 9;
            this.lblintensityFactor.Text = "0";
            // 
            // lbl_intensityFactor
            // 
            this.lbl_intensityFactor.AutoSize = true;
            this.lbl_intensityFactor.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_intensityFactor.ForeColor = System.Drawing.Color.Blue;
            this.lbl_intensityFactor.Location = new System.Drawing.Point(6, 206);
            this.lbl_intensityFactor.Name = "lbl_intensityFactor";
            this.lbl_intensityFactor.Size = new System.Drawing.Size(115, 20);
            this.lbl_intensityFactor.TabIndex = 8;
            this.lbl_intensityFactor.Text = "Intensity Factor :";
            // 
            // lbltrainingStressScore
            // 
            this.lbltrainingStressScore.AutoSize = true;
            this.lbltrainingStressScore.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltrainingStressScore.ForeColor = System.Drawing.Color.Black;
            this.lbltrainingStressScore.Location = new System.Drawing.Point(228, 153);
            this.lbltrainingStressScore.Name = "lbltrainingStressScore";
            this.lbltrainingStressScore.Size = new System.Drawing.Size(17, 20);
            this.lbltrainingStressScore.TabIndex = 7;
            this.lbltrainingStressScore.Text = "0";
            // 
            // lbl_trainingStressScore
            // 
            this.lbl_trainingStressScore.AutoSize = true;
            this.lbl_trainingStressScore.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trainingStressScore.ForeColor = System.Drawing.Color.Blue;
            this.lbl_trainingStressScore.Location = new System.Drawing.Point(6, 153);
            this.lbl_trainingStressScore.Name = "lbl_trainingStressScore";
            this.lbl_trainingStressScore.Size = new System.Drawing.Size(152, 20);
            this.lbl_trainingStressScore.TabIndex = 6;
            this.lbl_trainingStressScore.Text = "Training Stress Score :";
            // 
            // lblnormalisedPower
            // 
            this.lblnormalisedPower.AutoSize = true;
            this.lblnormalisedPower.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnormalisedPower.ForeColor = System.Drawing.Color.Black;
            this.lblnormalisedPower.Location = new System.Drawing.Point(228, 102);
            this.lblnormalisedPower.Name = "lblnormalisedPower";
            this.lblnormalisedPower.Size = new System.Drawing.Size(56, 20);
            this.lblnormalisedPower.TabIndex = 5;
            this.lblnormalisedPower.Text = "0 watts";
            // 
            // lbl_normalisedPower
            // 
            this.lbl_normalisedPower.AutoSize = true;
            this.lbl_normalisedPower.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_normalisedPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_normalisedPower.Location = new System.Drawing.Point(6, 102);
            this.lbl_normalisedPower.Name = "lbl_normalisedPower";
            this.lbl_normalisedPower.Size = new System.Drawing.Size(137, 20);
            this.lbl_normalisedPower.TabIndex = 4;
            this.lbl_normalisedPower.Text = "Normalised Power :";
            // 
            // lblpowerBalance
            // 
            this.lblpowerBalance.AutoSize = true;
            this.lblpowerBalance.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpowerBalance.ForeColor = System.Drawing.Color.Black;
            this.lblpowerBalance.Location = new System.Drawing.Point(228, 50);
            this.lblpowerBalance.Name = "lblpowerBalance";
            this.lblpowerBalance.Size = new System.Drawing.Size(17, 20);
            this.lblpowerBalance.TabIndex = 3;
            this.lblpowerBalance.Text = "0";
            // 
            // lbl_powerBalance
            // 
            this.lbl_powerBalance.AutoSize = true;
            this.lbl_powerBalance.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_powerBalance.ForeColor = System.Drawing.Color.Blue;
            this.lbl_powerBalance.Location = new System.Drawing.Point(6, 50);
            this.lbl_powerBalance.Name = "lbl_powerBalance";
            this.lbl_powerBalance.Size = new System.Drawing.Size(112, 20);
            this.lbl_powerBalance.TabIndex = 2;
            this.lbl_powerBalance.Text = "Power Balance :";
            // 
            // groupBox_summaryData
            // 
            this.groupBox_summaryData.BackColor = System.Drawing.Color.White;
            this.groupBox_summaryData.Controls.Add(this.lbltotalDistance);
            this.groupBox_summaryData.Controls.Add(this.lbl_totalDistance);
            this.groupBox_summaryData.Controls.Add(this.lblmaxAltitude);
            this.groupBox_summaryData.Controls.Add(this.lblavgAltitude);
            this.groupBox_summaryData.Controls.Add(this.lblmaxPower);
            this.groupBox_summaryData.Controls.Add(this.lblavgPower);
            this.groupBox_summaryData.Controls.Add(this.lblminHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lblmaxHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lblavgHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lblmaxSpeed);
            this.groupBox_summaryData.Controls.Add(this.lblavgSpeed);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxAltitude);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgAltitude);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxPower);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgPower);
            this.groupBox_summaryData.Controls.Add(this.lbl_minHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxSpeed);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgSpeed);
            this.groupBox_summaryData.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_summaryData.Location = new System.Drawing.Point(11, 20);
            this.groupBox_summaryData.Name = "groupBox_summaryData";
            this.groupBox_summaryData.Size = new System.Drawing.Size(702, 237);
            this.groupBox_summaryData.TabIndex = 5;
            this.groupBox_summaryData.TabStop = false;
            this.groupBox_summaryData.Text = "Summary Data";
            // 
            // lbltotalDistance
            // 
            this.lbltotalDistance.AutoSize = true;
            this.lbltotalDistance.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotalDistance.ForeColor = System.Drawing.Color.Black;
            this.lbltotalDistance.Location = new System.Drawing.Point(237, 52);
            this.lbltotalDistance.Name = "lbltotalDistance";
            this.lbltotalDistance.Size = new System.Drawing.Size(55, 20);
            this.lbltotalDistance.TabIndex = 31;
            this.lbltotalDistance.Text = "0 km/h";
            // 
            // lbl_totalDistance
            // 
            this.lbl_totalDistance.AutoSize = true;
            this.lbl_totalDistance.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalDistance.ForeColor = System.Drawing.Color.Blue;
            this.lbl_totalDistance.Location = new System.Drawing.Point(6, 52);
            this.lbl_totalDistance.Name = "lbl_totalDistance";
            this.lbl_totalDistance.Size = new System.Drawing.Size(169, 20);
            this.lbl_totalDistance.TabIndex = 30;
            this.lbl_totalDistance.Text = "Total Distance Covered :";
            // 
            // lblmaxAltitude
            // 
            this.lblmaxAltitude.AutoSize = true;
            this.lblmaxAltitude.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblmaxAltitude.Location = new System.Drawing.Point(588, 206);
            this.lblmaxAltitude.Name = "lblmaxAltitude";
            this.lblmaxAltitude.Size = new System.Drawing.Size(50, 20);
            this.lblmaxAltitude.TabIndex = 29;
            this.lblmaxAltitude.Text = "0 m/ft";
            // 
            // lblavgAltitude
            // 
            this.lblavgAltitude.AutoSize = true;
            this.lblavgAltitude.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgAltitude.ForeColor = System.Drawing.Color.Black;
            this.lblavgAltitude.Location = new System.Drawing.Point(588, 168);
            this.lblavgAltitude.Name = "lblavgAltitude";
            this.lblavgAltitude.Size = new System.Drawing.Size(50, 20);
            this.lblavgAltitude.TabIndex = 28;
            this.lblavgAltitude.Text = "0 m/ft";
            // 
            // lblmaxPower
            // 
            this.lblmaxPower.AutoSize = true;
            this.lblmaxPower.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxPower.ForeColor = System.Drawing.Color.Black;
            this.lblmaxPower.Location = new System.Drawing.Point(588, 129);
            this.lblmaxPower.Name = "lblmaxPower";
            this.lblmaxPower.Size = new System.Drawing.Size(56, 20);
            this.lblmaxPower.TabIndex = 27;
            this.lblmaxPower.Text = "0 watts";
            // 
            // lblavgPower
            // 
            this.lblavgPower.AutoSize = true;
            this.lblavgPower.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgPower.ForeColor = System.Drawing.Color.Black;
            this.lblavgPower.Location = new System.Drawing.Point(588, 91);
            this.lblavgPower.Name = "lblavgPower";
            this.lblavgPower.Size = new System.Drawing.Size(56, 20);
            this.lblavgPower.TabIndex = 26;
            this.lblavgPower.Text = "0 watts";
            // 
            // lblminHeartRate
            // 
            this.lblminHeartRate.AutoSize = true;
            this.lblminHeartRate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblminHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblminHeartRate.Location = new System.Drawing.Point(588, 52);
            this.lblminHeartRate.Name = "lblminHeartRate";
            this.lblminHeartRate.Size = new System.Drawing.Size(52, 20);
            this.lblminHeartRate.TabIndex = 25;
            this.lblminHeartRate.Text = "0 bpm";
            // 
            // lblmaxHeartRate
            // 
            this.lblmaxHeartRate.AutoSize = true;
            this.lblmaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblmaxHeartRate.Location = new System.Drawing.Point(237, 204);
            this.lblmaxHeartRate.Name = "lblmaxHeartRate";
            this.lblmaxHeartRate.Size = new System.Drawing.Size(52, 20);
            this.lblmaxHeartRate.TabIndex = 24;
            this.lblmaxHeartRate.Text = "0 bpm";
            // 
            // lblavgHeartRate
            // 
            this.lblavgHeartRate.AutoSize = true;
            this.lblavgHeartRate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblavgHeartRate.Location = new System.Drawing.Point(237, 163);
            this.lblavgHeartRate.Name = "lblavgHeartRate";
            this.lblavgHeartRate.Size = new System.Drawing.Size(52, 20);
            this.lblavgHeartRate.TabIndex = 23;
            this.lblavgHeartRate.Text = "0 bpm";
            // 
            // lblmaxSpeed
            // 
            this.lblmaxSpeed.AutoSize = true;
            this.lblmaxSpeed.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblmaxSpeed.Location = new System.Drawing.Point(237, 125);
            this.lblmaxSpeed.Name = "lblmaxSpeed";
            this.lblmaxSpeed.Size = new System.Drawing.Size(55, 20);
            this.lblmaxSpeed.TabIndex = 22;
            this.lblmaxSpeed.Text = "0 km/h";
            // 
            // lblavgSpeed
            // 
            this.lblavgSpeed.AutoSize = true;
            this.lblavgSpeed.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblavgSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblavgSpeed.Location = new System.Drawing.Point(237, 87);
            this.lblavgSpeed.Name = "lblavgSpeed";
            this.lblavgSpeed.Size = new System.Drawing.Size(55, 20);
            this.lblavgSpeed.TabIndex = 21;
            this.lblavgSpeed.Text = "0 km/h";
            // 
            // lbl_maxAltitude
            // 
            this.lbl_maxAltitude.AutoSize = true;
            this.lbl_maxAltitude.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxAltitude.Location = new System.Drawing.Point(357, 206);
            this.lbl_maxAltitude.Name = "lbl_maxAltitude";
            this.lbl_maxAltitude.Size = new System.Drawing.Size(139, 20);
            this.lbl_maxAltitude.TabIndex = 20;
            this.lbl_maxAltitude.Text = "Maximum Altitude :";
            // 
            // lbl_avgAltitude
            // 
            this.lbl_avgAltitude.AutoSize = true;
            this.lbl_avgAltitude.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgAltitude.Location = new System.Drawing.Point(357, 168);
            this.lbl_avgAltitude.Name = "lbl_avgAltitude";
            this.lbl_avgAltitude.Size = new System.Drawing.Size(128, 20);
            this.lbl_avgAltitude.TabIndex = 19;
            this.lbl_avgAltitude.Text = "Average Altitude :";
            // 
            // lbl_maxPower
            // 
            this.lbl_maxPower.AutoSize = true;
            this.lbl_maxPower.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxPower.Location = new System.Drawing.Point(357, 129);
            this.lbl_maxPower.Name = "lbl_maxPower";
            this.lbl_maxPower.Size = new System.Drawing.Size(126, 20);
            this.lbl_maxPower.TabIndex = 18;
            this.lbl_maxPower.Text = "Maximum Power :";
            // 
            // lbl_avgPower
            // 
            this.lbl_avgPower.AutoSize = true;
            this.lbl_avgPower.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgPower.Location = new System.Drawing.Point(357, 91);
            this.lbl_avgPower.Name = "lbl_avgPower";
            this.lbl_avgPower.Size = new System.Drawing.Size(115, 20);
            this.lbl_avgPower.TabIndex = 17;
            this.lbl_avgPower.Text = "Average Power :";
            // 
            // lbl_minHeartRate
            // 
            this.lbl_minHeartRate.AutoSize = true;
            this.lbl_minHeartRate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_minHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_minHeartRate.Location = new System.Drawing.Point(357, 52);
            this.lbl_minHeartRate.Name = "lbl_minHeartRate";
            this.lbl_minHeartRate.Size = new System.Drawing.Size(154, 20);
            this.lbl_minHeartRate.TabIndex = 16;
            this.lbl_minHeartRate.Text = "Minimum Heart Rate :";
            // 
            // lbl_maxHeartRate
            // 
            this.lbl_maxHeartRate.AutoSize = true;
            this.lbl_maxHeartRate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxHeartRate.Location = new System.Drawing.Point(6, 204);
            this.lbl_maxHeartRate.Name = "lbl_maxHeartRate";
            this.lbl_maxHeartRate.Size = new System.Drawing.Size(157, 20);
            this.lbl_maxHeartRate.TabIndex = 15;
            this.lbl_maxHeartRate.Text = "Maximum Heart Rate :";
            // 
            // lbl_avgHeartRate
            // 
            this.lbl_avgHeartRate.AutoSize = true;
            this.lbl_avgHeartRate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgHeartRate.Location = new System.Drawing.Point(6, 163);
            this.lbl_avgHeartRate.Name = "lbl_avgHeartRate";
            this.lbl_avgHeartRate.Size = new System.Drawing.Size(146, 20);
            this.lbl_avgHeartRate.TabIndex = 14;
            this.lbl_avgHeartRate.Text = "Average Heart Rate :";
            // 
            // lbl_maxSpeed
            // 
            this.lbl_maxSpeed.AutoSize = true;
            this.lbl_maxSpeed.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxSpeed.Location = new System.Drawing.Point(6, 125);
            this.lbl_maxSpeed.Name = "lbl_maxSpeed";
            this.lbl_maxSpeed.Size = new System.Drawing.Size(128, 20);
            this.lbl_maxSpeed.TabIndex = 13;
            this.lbl_maxSpeed.Text = "Maximum Speed :";
            // 
            // lbl_avgSpeed
            // 
            this.lbl_avgSpeed.AutoSize = true;
            this.lbl_avgSpeed.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgSpeed.Location = new System.Drawing.Point(6, 87);
            this.lbl_avgSpeed.Name = "lbl_avgSpeed";
            this.lbl_avgSpeed.Size = new System.Drawing.Size(117, 20);
            this.lbl_avgSpeed.TabIndex = 12;
            this.lbl_avgSpeed.Text = "Average Speed :";
            // 
            // zedGraphControl
            // 
            this.zedGraphControl.IsShowPointValues = false;
            this.zedGraphControl.Location = new System.Drawing.Point(557, 1);
            this.zedGraphControl.Name = "zedGraphControl";
            this.zedGraphControl.PointValueFormat = "G";
            this.zedGraphControl.Size = new System.Drawing.Size(636, 394);
            this.zedGraphControl.TabIndex = 11;
            // 
            // pan_createButton
            // 
            this.pan_createButton.BackColor = System.Drawing.Color.Black;
            this.pan_createButton.Controls.Add(this.btnChunkSelectedData);
            this.pan_createButton.Location = new System.Drawing.Point(5, 1);
            this.pan_createButton.Name = "pan_createButton";
            this.pan_createButton.Size = new System.Drawing.Size(546, 56);
            this.pan_createButton.TabIndex = 12;
            // 
            // btnChunkSelectedData
            // 
            this.btnChunkSelectedData.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChunkSelectedData.Location = new System.Drawing.Point(322, 11);
            this.btnChunkSelectedData.Name = "btnChunkSelectedData";
            this.btnChunkSelectedData.Size = new System.Drawing.Size(200, 36);
            this.btnChunkSelectedData.TabIndex = 0;
            this.btnChunkSelectedData.Text = "Chunk Selected Data";
            this.btnChunkSelectedData.UseVisualStyleBackColor = true;
            this.btnChunkSelectedData.Click += new System.EventHandler(this.btnChunkSelectedData_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HeartRate,
            this.Speed,
            this.Cadence,
            this.Altitude,
            this.Power,
            this.PowerIndex,
            this.TimeStamp});
            this.dataGridView.Location = new System.Drawing.Point(5, 55);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(546, 340);
            this.dataGridView.TabIndex = 13;
            this.dataGridView.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseUp);
            // 
            // HeartRate
            // 
            this.HeartRate.HeaderText = "Heart Rate";
            this.HeartRate.Name = "HeartRate";
            this.HeartRate.ReadOnly = true;
            // 
            // Speed
            // 
            this.Speed.HeaderText = "Speed";
            this.Speed.Name = "Speed";
            this.Speed.ReadOnly = true;
            // 
            // Cadence
            // 
            this.Cadence.HeaderText = "Cadence";
            this.Cadence.Name = "Cadence";
            this.Cadence.ReadOnly = true;
            // 
            // Altitude
            // 
            this.Altitude.HeaderText = "Altitude";
            this.Altitude.Name = "Altitude";
            this.Altitude.ReadOnly = true;
            // 
            // Power
            // 
            this.Power.HeaderText = "Power";
            this.Power.Name = "Power";
            this.Power.ReadOnly = true;
            // 
            // PowerIndex
            // 
            this.PowerIndex.HeaderText = "Power Index";
            this.PowerIndex.Name = "PowerIndex";
            this.PowerIndex.ReadOnly = true;
            // 
            // TimeStamp
            // 
            this.TimeStamp.HeaderText = "Time Stamp";
            this.TimeStamp.Name = "TimeStamp";
            this.TimeStamp.ReadOnly = true;
            // 
            // SelectableInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 676);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.zedGraphControl);
            this.Controls.Add(this.pan_createButton);
            this.Name = "SelectableInformation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SelectableInformation";
            this.Load += new System.EventHandler(this.SelectableInformation_Load);
            this.panel.ResumeLayout(false);
            this.groupBox_advancedMetrics.ResumeLayout(false);
            this.groupBox_advancedMetrics.PerformLayout();
            this.groupBox_summaryData.ResumeLayout(false);
            this.groupBox_summaryData.PerformLayout();
            this.pan_createButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.GroupBox groupBox_advancedMetrics;
        private System.Windows.Forms.Label lblintensityFactor;
        private System.Windows.Forms.Label lbl_intensityFactor;
        private System.Windows.Forms.Label lbltrainingStressScore;
        private System.Windows.Forms.Label lbl_trainingStressScore;
        private System.Windows.Forms.Label lblnormalisedPower;
        private System.Windows.Forms.Label lbl_normalisedPower;
        private System.Windows.Forms.Label lblpowerBalance;
        private System.Windows.Forms.Label lbl_powerBalance;
        private System.Windows.Forms.GroupBox groupBox_summaryData;
        private System.Windows.Forms.Label lbltotalDistance;
        private System.Windows.Forms.Label lbl_totalDistance;
        private System.Windows.Forms.Label lblmaxAltitude;
        private System.Windows.Forms.Label lblavgAltitude;
        private System.Windows.Forms.Label lblmaxPower;
        private System.Windows.Forms.Label lblavgPower;
        private System.Windows.Forms.Label lblminHeartRate;
        private System.Windows.Forms.Label lblmaxHeartRate;
        private System.Windows.Forms.Label lblavgHeartRate;
        private System.Windows.Forms.Label lblmaxSpeed;
        private System.Windows.Forms.Label lblavgSpeed;
        private System.Windows.Forms.Label lbl_maxAltitude;
        private System.Windows.Forms.Label lbl_avgAltitude;
        private System.Windows.Forms.Label lbl_maxPower;
        private System.Windows.Forms.Label lbl_avgPower;
        private System.Windows.Forms.Label lbl_minHeartRate;
        private System.Windows.Forms.Label lbl_maxHeartRate;
        private System.Windows.Forms.Label lbl_avgHeartRate;
        private System.Windows.Forms.Label lbl_maxSpeed;
        private System.Windows.Forms.Label lbl_avgSpeed;
        private ZedGraph.ZedGraphControl zedGraphControl;
        private System.Windows.Forms.Panel pan_createButton;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeStamp;
        private System.Windows.Forms.Button btnChunkSelectedData;
    }
}