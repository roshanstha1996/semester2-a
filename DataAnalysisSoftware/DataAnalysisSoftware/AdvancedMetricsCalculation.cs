﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DataAnalysisSoftware
{
    /// <summary>
    /// advanced metrics calculation class
    /// </summary>
    public partial class AdvancedMetricsCalculation : Form
    {
        //variable declared
        double averagePower;
        double totalSeconds;

        /// <summary>
        /// constructor with parameters
        /// </summary>
        /// <param name="avgPower"></param>
        /// <param name="totalSeconds"></param>
        public AdvancedMetricsCalculation(double averagePower, double totalSeconds)
        {
            this.averagePower = averagePower;
            this.totalSeconds = totalSeconds;
            InitializeComponent();
            advancedMetricsCalculation();
        }

        /// <summary>
        /// methods to calculate advanced metrics
        /// </summary>
        public void advancedMetricsCalculation()
        {
            //calculate functional threshold power
            double funcThreshPower = averagePower * 0.95;

            //calculating normalized power
            double func = Math.Pow(averagePower, 4.0);
            double min = func * 66.0;
            double time = min / 60.0;
            double normalisedPower = Math.Round(Math.Pow(time, (1.0 / 4)), 2);
            lblNorPow.Text = normalisedPower.ToString() + " " + "watts";

            //calculate intensity factor
            double intensFactor = Math.Round(normalisedPower / funcThreshPower, 2);
            lblIntensiFac.Text = intensFactor.ToString();

            //calculate training stress score
            double trainstressScore = Math.Round(((totalSeconds * normalisedPower * intensFactor) /
                (funcThreshPower * 3600)) * 100, 2);
            lblTSS.Text = trainstressScore.ToString();
        }
    }
}
