﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAnalysisSoftware
{
    /// <summary>
    /// two file browse class
    /// </summary>
    public partial class Browse2Files : Form
    {
        //variable declared
        string file1, file2;

        /// <summary>
        /// constructor
        /// </summary>
        public Browse2Files()
        {
            InitializeComponent();
        }

        /// <summary>
        /// browse file 2 event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_browseFile2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "HRM FILE (*.hrm) | *.hrm";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                file2 = ofd.FileName;
                txtfile2Path.Text = file2;
            }
        }

        /// <summary>
        /// event for ok button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ok_Click(object sender, EventArgs e)
        {
            string file1 = txtfile1Path.Text;
            string file2 = txtfile2Path.Text;
            if(!string.IsNullOrEmpty(file1) && !string.IsNullOrEmpty(file2))
            {
                //created and called filecomparison class constructot and pass file parameters
                FileComparison filecomp = new FileComparison(file1, file2);
                filecomp.Show();
            }
            else
            {
                MessageBox.Show("Please select both files to compare!!");
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// browse file 1 event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_browseFile1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "HRM FILE (*.hrm) | *.hrm";
            if(ofd.ShowDialog()==DialogResult.OK)
            {
                file1 = ofd.FileName;
                txtfile1Path.Text = file1;
            }
        }


    }
}
